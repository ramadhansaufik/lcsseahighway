import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createSwitchNavigator } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';

import TabBarIcon from '../components/TabBarIcon';

import HomeScreen from '../screens/HomeScreen';
import {
  PurchaseOrder,
  Commodities,
  Penting,
  Kemasan,
  Satuan,
  Deliveries,
  Containers,
  Kapal,
  Tracking,
  Pengaduan,
  AktivasiUser,
  UserLCS,
  Regulator,
  Panduan,

  VesselTracking,

  StatusOrder,
  OrderPerBulan,

  OrderPerJenisPrioritas,
  MuatanTerbanyakPerJenisPrioritas,
  VoyagePerTrayek,
  TotalContainerPerPort,

  ReportUser,
  DaftarOperator,
  RealisasiMuatanPerOperator,

  LihatDisparitasHargaBarang,
  LihatKuotaTrayek,
  LihatMuatanPerWilayah,
  Realisasi,
  WaktuTempuh,
} from '../screens/HomeStack';

import LinksScreen from '../screens/LinksScreen';
import SettingsScreen from '../screens/SettingsScreen';

import ProfileScreen from '../screens/ProfileScreen';
import {
  EditProfileScreen
} from '../screens/ProfileStack'

const config = Platform.select({
  web: { headerMode: 'screen' },
  default: {},
});

const HomeStack = createStackNavigator(
  {
    Dashboard: HomeScreen,
    PurchaseOrder: PurchaseOrder,
    Commodities: Commodities,
    Penting: Penting,
    Kemasan: Kemasan,
    Satuan: Satuan,
    Deliveries: Deliveries,
    Containers: Containers,
    Kapal: Kapal,
    Tracking: Tracking,
    Pengaduan: Pengaduan,
    AktivasiUser: AktivasiUser,
    UserLCS: UserLCS,
    Regulator: Regulator,
    Panduan: Panduan,

    VesselTracking: VesselTracking,

    StatusOrder: StatusOrder,
    OrderPerBulan: OrderPerBulan,

    OrderPerJenisPrioritas: OrderPerJenisPrioritas,
    MuatanTerbanyakPerJenisPrioritas: MuatanTerbanyakPerJenisPrioritas,
    VoyagePerTrayek: VoyagePerTrayek,
    TotalContainerPerPort: TotalContainerPerPort,

    ReportUser: ReportUser,
    DaftarOperator: DaftarOperator,
    RealisasiMuatanPerOperator: RealisasiMuatanPerOperator,
    
    LihatDisparitasHargaBarang: LihatDisparitasHargaBarang,
    LihatKuotaTrayek: LihatKuotaTrayek,
    LihatMuatanPerWilayah: LihatMuatanPerWilayah,
    Realisasi: Realisasi,
    WaktuTempuh: WaktuTempuh,
  },
  config
);

HomeStack.navigationOptions = {
  tabBarLabel: 'Dashboard',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-information-circle${focused ? '' : '-outline'}`
          : 'md-home'
      }
    />
  ),
};

HomeStack.path = '';

const LinksStack = createStackNavigator(
  {
    Links: LinksScreen,
  },
  config
);

LinksStack.navigationOptions = {
  tabBarLabel: 'Links',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-link' : 'md-link'} />
  ),
};

LinksStack.path = '';

const SettingsStack = createStackNavigator(
  {
    Settings: SettingsScreen,
  },
  config
);

SettingsStack.navigationOptions = {
  tabBarLabel: 'Settings',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-options' : 'md-notifications'} />
  ),
};

SettingsStack.path = '';

const ProfileStack = createStackNavigator(
  {
    Profile: ProfileScreen,
    EditProfileScreen: EditProfileScreen,
  },
  config
);

ProfileStack.navigationOptions = {
  tabBarLabel: 'Profile',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-person' : 'md-person'} />
  ),
};

ProfileStack.path = '';

const tabNavigator = createBottomTabNavigator({
  HomeStack: HomeStack,
  LinksStack: LinksStack,
  SettingsStack: SettingsStack,
  ProfileStack: ProfileStack,
});

tabNavigator.path = '';

export default tabNavigator;