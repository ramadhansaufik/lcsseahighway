import {
    combineReducers
} from 'redux'
import {
    UNIT_GET_LOADING,
    UNIT_GET_ERROR,
    UNIT_GET_SUCCESS,

    UNIT_VIEW_ERROR,
    UNIT_VIEW_LOADING,
    UNIT_VIEW_SUCCESS,

    UNIT_ADD_ERROR,
    UNIT_ADD_LOADING,
    UNIT_ADD_SUCCESS,

    UNIT_DELETE_ERROR,
    UNIT_DELETE_LOADING,
    UNIT_DELETE_SUCCESS
} from '../actions/constant'

const unitGetError = (state = false, action) => {
    switch (action.type) {
        case UNIT_GET_ERROR:
            return action.unitGetError
        default:
            return state
    }
}

const unitGetLoading = (state = false, action) => {
    switch (action.type) {
        case UNIT_GET_LOADING:
            return action.unitGetLoading
        default:
            return state
    }
}

const unitGetSuccess = (state = false, action) => {
    switch (action.type) {
        case UNIT_GET_SUCCESS:
            return action.unitGetSuccess
        default:
            return state
    }
}

const units = (state = [], action) => {
    switch (action.type) {
        case UNIT_GET_SUCCESS:
            return action.units
        default:
            return state
    }
}

const unitViewLoading = (state = false, action) => {
    switch (action.type) {
        case UNIT_VIEW_LOADING:
            return action.unitViewLoading
        default:
            return state
    }
}

const unitViewError = (state = false, action) => {
    switch (action.type) {
        case UNIT_VIEW_ERROR:
            return action.unitViewError
        default:
            return state
    }
}

const unitViewSuccess = (state = false, action) => {
    switch (action.type) {
        case UNIT_VIEW_SUCCESS:
            return action.unitViewSuccess
        default:
            return state
    }
}

const unit = (state = [], action) => {
    switch (action.type) {
        case UNIT_VIEW_SUCCESS:
            return action.unit
        default:
            return state
    }
}

const unitAddError = (state = false, action) => {
    switch (action.type) {
        case UNIT_ADD_ERROR:
            return action.unitAddError
        default:
            return state
    }
}

const unitAddLoading = (state = false, action) => {
    switch (action.type) {
        case UNIT_ADD_LOADING:
            return action.unitAddLoading
        default:
            return state
    }
}

const unitAddSuccess = (state = false, action) => {
    switch (action.type) {
        case UNIT_ADD_SUCCESS:
            return action.unitAddSuccess
        default:
            return state
    }
}

const unitDeleteError = (state = false, action) => {
    switch (action.type) {
        case UNIT_DELETE_ERROR:
            return action.unitDeleteError
        default:
            return state
    }
}

const unitDeleteLoading = (state = false, action) => {
    switch (action.type) {
        case UNIT_DELETE_LOADING:
            return action.unitDeleteLoading
        default:
            return state
    }
}

const unitDeleteSuccess = (state = false, action) => {
    switch (action.type) {
        case UNIT_DELETE_SUCCESS:
            return action.unitDeleteSuccess
        default:
            return state
    }
}

const unitReducer = combineReducers({
    unitGetError,
    unitGetLoading,
    unitGetSuccess,
    units,

    unitViewLoading,
    unitViewError,
    unitViewSuccess,
    unit,

    unitAddError,
    unitAddLoading,
    unitAddSuccess,

    unitDeleteError,
    unitDeleteLoading,
    unitDeleteSuccess
})

export default unitReducer