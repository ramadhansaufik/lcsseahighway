import {
    combineReducers
} from 'redux'
import {
    JENISBARANG_GET_LOADING,
    JENISBARANG_GET_ERROR,
    JENISBARANG_GET_SUCCESS,
} from '../actions/constant'

const jenisbarangGetError = (state = false, action) => {
    switch (action.type) {
        case JENISBARANG_GET_ERROR:
            return action.jenisbarangGetError
        default:
            return state
    }
}

const jenisbarangGetLoading = (state = false, action) => {
    switch (action.type) {
        case JENISBARANG_GET_LOADING:
            return action.jenisbarangGetLoading
        default:
            return state
    }
}

const jenisbarangGetSuccess = (state = false, action) => {
    switch (action.type) {
        case JENISBARANG_GET_SUCCESS:
            return action.jenisbarangGetSuccess
        default:
            return state
    }
}

const jenisbarangs = (state = [], action) => {
    switch (action.type) {
        case JENISBARANG_GET_SUCCESS:
            return action.jenisbarangs
        default:
            return state
    }
}


const jenisbarangReducer = combineReducers({
    jenisbarangGetError,
    jenisbarangGetLoading,
    jenisbarangGetSuccess,
    jenisbarangs
})

export default jenisbarangReducer