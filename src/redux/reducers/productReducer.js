import {
    combineReducers
} from 'redux'
import {
    PRODUCT_GET_LOADING,
    PRODUCT_GET_ERROR,
    PRODUCT_GET_SUCCESS,

    PRODUCT_VIEW_ERROR,
    PRODUCT_VIEW_LOADING,
    PRODUCT_VIEW_SUCCESS,

    PRODUCT_ADD_ERROR,
    PRODUCT_ADD_LOADING,
    PRODUCT_ADD_SUCCESS,

    PRODUCT_DELETE_ERROR,
    PRODUCT_DELETE_LOADING,
    PRODUCT_DELETE_SUCCESS
} from '../actions/constant'

const productGetError = (state = false, action) => {
    switch (action.type) {
        case PRODUCT_GET_ERROR:
            return action.productGetError
        default:
            return state
    }
}

const productGetLoading = (state = false, action) => {
    switch (action.type) {
        case PRODUCT_GET_LOADING:
            return action.productGetLoading
        default:
            return state
    }
}

const productGetSuccess = (state = false, action) => {
    switch (action.type) {
        case PRODUCT_GET_SUCCESS:
            return action.productGetSuccess
        default:
            return state
    }
}

const products = (state = [], action) => {
    switch (action.type) {
        case PRODUCT_GET_SUCCESS:
            return action.products
        default:
            return state
    }
}

const productViewLoading = (state = false, action) => {
    switch (action.type) {
        case PRODUCT_VIEW_LOADING:
            return action.productViewLoading
        default:
            return state
    }
}

const productViewError = (state = false, action) => {
    switch (action.type) {
        case PRODUCT_VIEW_ERROR:
            return action.productViewError
        default:
            return state
    }
}

const productViewSuccess = (state = false, action) => {
    switch (action.type) {
        case PRODUCT_VIEW_SUCCESS:
            return action.productViewSuccess
        default:
            return state
    }
}

const product = (state = [], action) => {
    switch (action.type) {
        case PRODUCT_VIEW_SUCCESS:
            return action.product
        default:
            return state
    }
}

const productAddError = (state = false, action) => {
    switch (action.type) {
        case PRODUCT_ADD_ERROR:
            return action.productAddError
        default:
            return state
    }
}

const productAddLoading = (state = false, action) => {
    switch (action.type) {
        case PRODUCT_ADD_LOADING:
            return action.productAddLoading
        default:
            return state
    }
}

const productAddSuccess = (state = false, action) => {
    switch (action.type) {
        case PRODUCT_ADD_SUCCESS:
            return action.productAddSuccess
        default:
            return state
    }
}

const productDeleteError = (state = false, action) => {
    switch (action.type) {
        case PRODUCT_DELETE_ERROR:
            return action.productDeleteError
        default:
            return state
    }
}

const productDeleteLoading = (state = false, action) => {
    switch (action.type) {
        case PRODUCT_DELETE_LOADING:
            return action.productDeleteLoading
        default:
            return state
    }
}

const productDeleteSuccess = (state = false, action) => {
    switch (action.type) {
        case PRODUCT_DELETE_SUCCESS:
            return action.productDeleteSuccess
        default:
            return state
    }
}

const productReducer = combineReducers({
    productGetError,
    productGetLoading,
    productGetSuccess,
    products,

    productViewLoading,
    productViewError,
    productViewSuccess,
    product,

    productAddError,
    productAddLoading,
    productAddSuccess,

    productDeleteError,
    productDeleteLoading,
    productDeleteSuccess
})

export default productReducer