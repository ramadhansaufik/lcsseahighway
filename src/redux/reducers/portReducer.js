import {
    combineReducers
} from 'redux'
import {
    PORT_GET_LOADING,
    PORT_GET_ERROR,
    PORT_GET_SUCCESS,

    PORT_VIEW_ERROR,
    PORT_VIEW_LOADING,
    PORT_VIEW_SUCCESS,

    PORT_ADD_ERROR,
    PORT_ADD_LOADING,
    PORT_ADD_SUCCESS,

    PORT_DELETE_ERROR,
    PORT_DELETE_LOADING,
    PORT_DELETE_SUCCESS
} from '../actions/constant'

const portGetError = (state = false, action) => {
    switch (action.type) {
        case PORT_GET_ERROR:
            return action.portGetError
        default:
            return state
    }
}

const portGetLoading = (state = false, action) => {
    switch (action.type) {
        case PORT_GET_LOADING:
            return action.portGetLoading
        default:
            return state
    }
}

const portGetSuccess = (state = false, action) => {
    switch (action.type) {
        case PORT_GET_SUCCESS:
            return action.portGetSuccess
        default:
            return state
    }
}

const ports = (state = [], action) => {
    switch (action.type) {
        case PORT_GET_SUCCESS:
            return action.ports
        default:
            return state
    }
}

const portViewLoading = (state = false, action) => {
    switch (action.type) {
        case PORT_VIEW_LOADING:
            return action.portViewLoading
        default:
            return state
    }
}

const portViewError = (state = false, action) => {
    switch (action.type) {
        case PORT_VIEW_ERROR:
            return action.portViewError
        default:
            return state
    }
}

const portViewSuccess = (state = false, action) => {
    switch (action.type) {
        case PORT_VIEW_SUCCESS:
            return action.portViewSuccess
        default:
            return state
    }
}

const port = (state = [], action) => {
    switch (action.type) {
        case PORT_VIEW_SUCCESS:
            return action.port
        default:
            return state
    }
}

const portAddError = (state = false, action) => {
    switch (action.type) {
        case PORT_ADD_ERROR:
            return action.portAddError
        default:
            return state
    }
}

const portAddLoading = (state = false, action) => {
    switch (action.type) {
        case PORT_ADD_LOADING:
            return action.portAddLoading
        default:
            return state
    }
}

const portAddSuccess = (state = false, action) => {
    switch (action.type) {
        case PORT_ADD_SUCCESS:
            return action.portAddSuccess
        default:
            return state
    }
}

const portDeleteError = (state = false, action) => {
    switch (action.type) {
        case PORT_DELETE_ERROR:
            return action.portDeleteError
        default:
            return state
    }
}

const portDeleteLoading = (state = false, action) => {
    switch (action.type) {
        case PORT_DELETE_LOADING:
            return action.portDeleteLoading
        default:
            return state
    }
}

const portDeleteSuccess = (state = false, action) => {
    switch (action.type) {
        case PORT_DELETE_SUCCESS:
            return action.portDeleteSuccess
        default:
            return state
    }
}

const portReducer = combineReducers({
    portGetError,
    portGetLoading,
    portGetSuccess,
    ports,

    portViewLoading,
    portViewError,
    portViewSuccess,
    port,

    portAddError,
    portAddLoading,
    portAddSuccess,

    portDeleteError,
    portDeleteLoading,
    portDeleteSuccess
})

export default portReducer