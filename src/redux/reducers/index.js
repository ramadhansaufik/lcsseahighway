import { combineReducers
} from 'redux'
import authReducer from './authReducer'
import userReducer from './userReducer'
import commodityReducer from './commodityReducer'
import consigneeReducer from './consigneeReducer'
import shipperReducer from './shipperReducer'
import operatorReducer from './operatorReducer'
import deliveryReducer from './deliveryReducer'
import kotaReducer from './kotaReducer'
import provinsiReducer from './provinsiReducer'
import purchaseReducer from './purchaseReducer'
import supplierReducer from './supplierReducer'
import resellerReducer from './resellerReducer'
import trackReducer from './trackReducer'
import unitReducer from './unitReducer'
import detailpoReducer from './detailpoReducer'
import jenisbarangReducer from './jenisbarangReducer'
import routeReducer from './routeReducer'
import distributionReducer from './distributionReducer'
import portReducer from './portReducer'
import scheduleReducer from './scheduleReducer'
import bookingReducer from './bookingReducer'
import containerReducer from './containerReducer'
import detailcontainerReducer from './detailcontainerReducer'
import kemasanReducer from './kemasanReducer'
import typecontainerReducer from './typecontainerReducer'
import depotReducer from './depotReducer'
import jeniscommodityReducer from './jeniscommodityReducer'
import productReducer from './productReducer'
import vesselReducer from './vesselReducer'
import complaintReducer from './complaintReducer'
import msttrayekReducer from './msttrayekReducer'
import dashboardReducer from './dashboardReducer'
import trackingDataReducer from './trackingDataReducer'
import manifestReducer from './manifestReducer'
import packagingReducer from './packagingReducer'

const tolApp = combineReducers({
    auth: authReducer,
    users: userReducer,
    commodities: commodityReducer,
    consignees: consigneeReducer,
    shippers: shipperReducer,
    operators: operatorReducer,
    deliveries: deliveryReducer,
    kotas: kotaReducer,
    provinsis: provinsiReducer,
    purchases: purchaseReducer,
    suppliers: supplierReducer,
    resellers: resellerReducer,
    tracks: trackReducer,
    units: unitReducer,
    detailpos: detailpoReducer,
    jenisbarangs: jenisbarangReducer,
    routes: routeReducer,
    distributions: distributionReducer,
    ports: portReducer,
    schedules: scheduleReducer,
    bookings: bookingReducer,
    containers: containerReducer,
    detailcontainers: detailcontainerReducer,
    kemasans: kemasanReducer,
    typecontainers: typecontainerReducer,
    depots: depotReducer,
    jeniscommodities: jeniscommodityReducer,
    products: productReducer,
    vessels: vesselReducer,
    complaints: complaintReducer,
    msttrayeks: msttrayekReducer,
    dashboard: dashboardReducer,
    trackingData: trackingDataReducer,
    manifest: manifestReducer,
    packagings: packagingReducer,
})

export default tolApp