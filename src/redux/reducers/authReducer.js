import {
    combineReducers
} from 'redux'
import {AsyncStorage} from 'react-native'
import {
    AUTH_LOGIN_ERROR,
    AUTH_LOGIN_LOADING,
    AUTH_LOGIN_SUCCESS,

    AUTH_REGISTER_ERROR,
    AUTH_REGISTER_LOADING,
    AUTH_REGISTER_SUCCESS,

    AUTH_LOGOUT_SUCCESS,
    AUTH_LOGOUT_LOADING,

    AUTH_REGISTER_CHECK_ERROR,
    AUTH_REGISTER_CHECK_LOADING,
    AUTH_REGISTER_CHECK_SUCCESS,

    IS_AUTHENTICATED
} from '../actions/constant'

const authLoginError = (state = false, action) => {
    switch (action.type) {
        case AUTH_LOGIN_ERROR:
            return action.authLoginError
        default:
            return state
    }
}

const authLoginLoading = (state = false, action) => {
    switch (action.type) {
        case AUTH_LOGIN_LOADING:
            return action.authLoginLoading
        default:
            return state
    }
}

const authLoginSuccess = (state = false, action) => {
    switch (action.type) {
        case AUTH_LOGIN_SUCCESS:
            return action.authLoginSuccess
        default:
            return state
    }
}

const authRegisterError = (state = false, action) => {
    switch (action.type) {
        case AUTH_REGISTER_ERROR:
            return action.authRegisterError
        default:
            return state
    }
}

const authRegisterLoading = (state = false, action) => {
    switch (action.type) {
        case AUTH_REGISTER_LOADING:
            return action.authRegisterLoading
        default:
            return state
    }
}

const authRegisterSuccess = (state = false, action) => {
    switch (action.type) {
        case AUTH_REGISTER_SUCCESS:
            return action.authRegisterSuccess
        default:
            return state
    }
}

const account = (state = [], action) => {
    switch (action.type) {
        case AUTH_LOGIN_SUCCESS:
            return action.account
        case AUTH_REGISTER_SUCCESS:
            return action.account
        default:
            return state
    }
}

const isAuthenticated = (state = false, action) => {
    switch (action.type) {
        case IS_AUTHENTICATED:
            return action.isAuthenticated
        case AUTH_LOGIN_SUCCESS:
        case AUTH_LOGOUT_SUCCESS:
            return action.isAuthenticated
        default:
            return state
    }
}

const authLogoutLoading = (state = false, action) => {
    switch (action.type) {
        case AUTH_LOGOUT_LOADING:
            return action.authLogoutLoading
        default:
            return state
    }
}

const authLogoutSuccess = (state = false, action) => {
    switch (action.type) {
        case AUTH_LOGOUT_SUCCESS:
            return action.authLogoutSuccess
        default:
            return state
    }
}

const authRegisterCheckError = (state = false, action) => {
    switch (action.type) {
        case AUTH_REGISTER_CHECK_ERROR:
            return action.authRegisterCheckError
        default:
            return state
    }
}

const authRegisterCheckLoading = (state = false, action) => {
    switch (action.type) {
        case AUTH_REGISTER_CHECK_LOADING:
            return action.authRegisterCheckLoading
        default:
            return state
    }
}

const authRegisterCheckSuccess = (state = false, action) => {
    switch (action.type) {
        case AUTH_REGISTER_CHECK_SUCCESS:
            return action.authRegisterCheckSuccess
        default:
            return state
    }
}

const check_data = (state = [], action) => {
    switch (action.type) {
        case AUTH_REGISTER_CHECK_SUCCESS:
            return action.check_data
        default:
            return state
    }
}

const authReducer = combineReducers({
    authLoginError,
    authLoginLoading,
    authLoginSuccess,

    authRegisterError,
    authRegisterLoading,
    authRegisterSuccess,
    account,

    isAuthenticated,
    authLogoutLoading,
    authLogoutSuccess,

    authRegisterCheckError,
    authRegisterCheckLoading,
    authRegisterCheckSuccess,
    check_data,
})

export default authReducer