import {
    combineReducers
} from 'redux'
import {
    PURCHASE_GET_LOADING,
    PURCHASE_GET_ERROR,
    PURCHASE_GET_SUCCESS,

    PURCHASE_VIEW_ERROR,
    PURCHASE_VIEW_LOADING,
    PURCHASE_VIEW_SUCCESS,

    PURCHASE_ADD_ERROR,
    PURCHASE_ADD_LOADING,
    PURCHASE_ADD_SUCCESS,

    PURCHASE_DELETE_ERROR,
    PURCHASE_DELETE_LOADING,
    PURCHASE_DELETE_SUCCESS
} from '../actions/constant'

const purchaseGetError = (state = false, action) => {
    switch (action.type) {
        case PURCHASE_GET_ERROR:
            return action.purchaseGetError
        default:
            return state
    }
}

const purchaseGetLoading = (state = false, action) => {
    switch (action.type) {
        case PURCHASE_GET_LOADING:
            return action.purchaseGetLoading
        default:
            return state
    }
}

const purchaseGetSuccess = (state = false, action) => {
    switch (action.type) {
        case PURCHASE_GET_SUCCESS:
            return action.purchaseGetSuccess
        default:
            return state
    }
}

const purchases = (state = [], action) => {
    switch (action.type) {
        case PURCHASE_GET_SUCCESS:
            return action.purchases
        default:
            return state
    }
}

const purchaseViewLoading = (state = false, action) => {
    switch (action.type) {
        case PURCHASE_VIEW_LOADING:
            return action.purchaseViewLoading
        default:
            return state
    }
}

const purchaseViewError = (state = false, action) => {
    switch (action.type) {
        case PURCHASE_VIEW_ERROR:
            return action.purchaseViewError
        default:
            return state
    }
}

const purchaseViewSuccess = (state = false, action) => {
    switch (action.type) {
        case PURCHASE_VIEW_SUCCESS:
            return action.purchaseViewSuccess
        default:
            return state
    }
}

const purchase = (state = [], action) => {
    switch (action.type) {
        case PURCHASE_VIEW_SUCCESS:
            return action.purchase
        default:
            return state
    }
}

const purchaseAddError = (state = false, action) => {
    switch (action.type) {
        case PURCHASE_ADD_ERROR:
            return action.purchaseAddError
        default:
            return state
    }
}

const purchaseAddLoading = (state = false, action) => {
    switch (action.type) {
        case PURCHASE_ADD_LOADING:
            return action.purchaseAddLoading
        default:
            return state
    }
}

const purchaseAddSuccess = (state = false, action) => {
    switch (action.type) {
        case PURCHASE_ADD_SUCCESS:
            return action.purchaseAddSuccess
        default:
            return state
    }
}

const purchaseAdded = (state = [], action) => {
    switch (action.type) {
        case PURCHASE_ADD_SUCCESS:
            return action.purchase
        default:
            return state
    }
}

const purchaseDeleteError = (state = false, action) => {
    switch (action.type) {
        case PURCHASE_DELETE_ERROR:
            return action.purchaseDeleteError
        default:
            return state
    }
}

const purchaseDeleteLoading = (state = false, action) => {
    switch (action.type) {
        case PURCHASE_DELETE_LOADING:
            return action.purchaseDeleteLoading
        default:
            return state
    }
}

const purchaseDeleteSuccess = (state = false, action) => {
    switch (action.type) {
        case PURCHASE_DELETE_SUCCESS:
            return action.purchaseDeleteSuccess
        default:
            return state
    }
}

const purchaseReducer = combineReducers({
    purchaseGetError,
    purchaseGetLoading,
    purchaseGetSuccess,
    purchases,

    purchaseViewLoading,
    purchaseViewError,
    purchaseViewSuccess,
    purchase,

    purchaseAddError,
    purchaseAddLoading,
    purchaseAddSuccess,
    purchaseAdded,

    purchaseDeleteError,
    purchaseDeleteLoading,
    purchaseDeleteSuccess
})

export default purchaseReducer