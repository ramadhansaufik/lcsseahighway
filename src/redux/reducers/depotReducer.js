import {
    combineReducers
} from 'redux'
import {
    DEPOT_GET_LOADING,
    DEPOT_GET_ERROR,
    DEPOT_GET_SUCCESS,

    DEPOT_VIEW_ERROR,
    DEPOT_VIEW_LOADING,
    DEPOT_VIEW_SUCCESS,

    DEPOT_ADD_ERROR,
    DEPOT_ADD_LOADING,
    DEPOT_ADD_SUCCESS,

    DEPOT_DELETE_ERROR,
    DEPOT_DELETE_LOADING,
    DEPOT_DELETE_SUCCESS
} from '../actions/constant'

const depotGetError = (state = false, action) => {
    switch (action.type) {
        case DEPOT_GET_ERROR:
            return action.depotGetError
        default:
            return state
    }
}

const depotGetLoading = (state = false, action) => {
    switch (action.type) {
        case DEPOT_GET_LOADING:
            return action.depotGetLoading
        default:
            return state
    }
}

const depotGetSuccess = (state = false, action) => {
    switch (action.type) {
        case DEPOT_GET_SUCCESS:
            return action.depotGetSuccess
        default:
            return state
    }
}

const depots = (state = [], action) => {
    switch (action.type) {
        case DEPOT_GET_SUCCESS:
            return action.depots
        default:
            return state
    }
}

const depotViewLoading = (state = false, action) => {
    switch (action.type) {
        case DEPOT_VIEW_LOADING:
            return action.depotViewLoading
        default:
            return state
    }
}

const depotViewError = (state = false, action) => {
    switch (action.type) {
        case DEPOT_VIEW_ERROR:
            return action.depotViewError
        default:
            return state
    }
}

const depotViewSuccess = (state = false, action) => {
    switch (action.type) {
        case DEPOT_VIEW_SUCCESS:
            return action.depotViewSuccess
        default:
            return state
    }
}

const depot = (state = [], action) => {
    switch (action.type) {
        case DEPOT_VIEW_SUCCESS:
            return action.depot
        default:
            return state
    }
}

const depotAddError = (state = false, action) => {
    switch (action.type) {
        case DEPOT_ADD_ERROR:
            return action.depotAddError
        default:
            return state
    }
}

const depotAddLoading = (state = false, action) => {
    switch (action.type) {
        case DEPOT_ADD_LOADING:
            return action.depotAddLoading
        default:
            return state
    }
}

const depotAddSuccess = (state = false, action) => {
    switch (action.type) {
        case DEPOT_ADD_SUCCESS:
            return action.depotAddSuccess
        default:
            return state
    }
}

const depotDeleteError = (state = false, action) => {
    switch (action.type) {
        case DEPOT_DELETE_ERROR:
            return action.depotDeleteError
        default:
            return state
    }
}

const depotDeleteLoading = (state = false, action) => {
    switch (action.type) {
        case DEPOT_DELETE_LOADING:
            return action.depotDeleteLoading
        default:
            return state
    }
}

const depotDeleteSuccess = (state = false, action) => {
    switch (action.type) {
        case DEPOT_DELETE_SUCCESS:
            return action.depotDeleteSuccess
        default:
            return state
    }
}

const depotReducer = combineReducers({
    depotGetError,
    depotGetLoading,
    depotGetSuccess,
    depots,

    depotViewLoading,
    depotViewError,
    depotViewSuccess,
    depot,

    depotAddError,
    depotAddLoading,
    depotAddSuccess,

    depotDeleteError,
    depotDeleteLoading,
    depotDeleteSuccess
})

export default depotReducer