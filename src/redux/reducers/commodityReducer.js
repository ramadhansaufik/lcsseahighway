import {
    combineReducers
} from 'redux'
import {
    COMMODITY_GET_LOADING,
    COMMODITY_GET_ERROR,
    COMMODITY_GET_SUCCESS,

    COMMODITY_VIEW_ERROR,
    COMMODITY_VIEW_LOADING,
    COMMODITY_VIEW_SUCCESS,

    COMMODITY_ADD_ERROR,
    COMMODITY_ADD_LOADING,
    COMMODITY_ADD_INVALID,
    COMMODITY_ADD_SUCCESS,

    COMMODITY_DELETE_ERROR,
    COMMODITY_DELETE_LOADING,
    COMMODITY_DELETE_SUCCESS
} from '../actions/constant'

const commodityGetError = (state = false, action) => {
    switch (action.type) {
        case COMMODITY_GET_ERROR:
            return action.commodityGetError
        default:
            return state
    }
}

const commodityGetLoading = (state = false, action) => {
    switch (action.type) {
        case COMMODITY_GET_LOADING:
            return action.commodityGetLoading
        default:
            return state
    }
}

const commodityGetSuccess = (state = false, action) => {
    switch (action.type) {
        case COMMODITY_GET_SUCCESS:
            return action.commodityGetSuccess
        default:
            return state
    }
}

const commodities = (state = [], action) => {
    switch (action.type) {
        case COMMODITY_GET_SUCCESS:
            return action.commodities
        default:
            return state
    }
}

const commodityViewLoading = (state = false, action) => {
    switch (action.type) {
        case COMMODITY_VIEW_LOADING:
            return action.commodityViewLoading
        default:
            return state
    }
}

const commodityViewError = (state = false, action) => {
    switch (action.type) {
        case COMMODITY_VIEW_ERROR:
            return action.commodityViewError
        default:
            return state
    }
}

const commodityViewSuccess = (state = false, action) => {
    switch (action.type) {
        case COMMODITY_VIEW_SUCCESS:
            return action.commodityViewSuccess
        default:
            return state
    }
}

const commodity = (state = [], action) => {
    switch (action.type) {
        case COMMODITY_VIEW_SUCCESS:
            return action.commodity
        default:
            return state
    }
}

const commodityAddError = (state = false, action) => {
    switch (action.type) {
        case COMMODITY_ADD_ERROR:
            return action.commodityAddError
        default:
            return state
    }
}

const commodityAddLoading = (state = false, action) => {
    switch (action.type) {
        case COMMODITY_ADD_LOADING:
            return action.commodityAddLoading
        default:
            return state
    }
}

const commodityAddInvalid = (state = false, action) => {
    switch (action.type) {
        case COMMODITY_ADD_INVALID:
            return action.commodityAddInvalid
        default:
            return state
    }
}

const invalid = (state = [], action) => {
    switch (action.type) {
        case COMMODITY_ADD_INVALID:
            return action.invalid
        default:
            return state
    }
}

const commodityAddSuccess = (state = false, action) => {
    switch (action.type) {
        case COMMODITY_ADD_SUCCESS:
            return action.commodityAddSuccess
        default:
            return state
    }
}

const commodityDeleteError = (state = false, action) => {
    switch (action.type) {
        case COMMODITY_DELETE_ERROR:
            return action.commodityDeleteError
        default:
            return state
    }
}

const commodityDeleteLoading = (state = false, action) => {
    switch (action.type) {
        case COMMODITY_DELETE_LOADING:
            return action.commodityDeleteLoading
        default:
            return state
    }
}

const commodityDeleteSuccess = (state = false, action) => {
    switch (action.type) {
        case COMMODITY_DELETE_SUCCESS:
            return action.commodityDeleteSuccess
        default:
            return state
    }
}

const commodityReducer = combineReducers({
    commodityGetError,
    commodityGetLoading,
    commodityGetSuccess,
    commodities,

    commodityViewLoading,
    commodityViewError,
    commodityViewSuccess,
    commodity,

    commodityAddError,
    commodityAddLoading,
    commodityAddInvalid,
    commodityAddSuccess,
    invalid,

    commodityDeleteError,
    commodityDeleteLoading,
    commodityDeleteSuccess,
})

export default commodityReducer