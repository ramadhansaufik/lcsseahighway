import {
    combineReducers
} from 'redux'
import {
    MANIFEST_GET_LOADING,
    MANIFEST_GET_ERROR,
    MANIFEST_GET_SUCCESS,

    MANIFEST_VIEW_LOADING,
    MANIFEST_VIEW_ERROR,
    MANIFEST_VIEW_SUCCESS
} from '../actions/constant'

const manifestGetLoading = (state = false, action) => {
    switch (action.type) {
        case MANIFEST_GET_LOADING:
            return action.manifestGetLoading
        default:
            return state
    }
}

const manifestGetError = (state = false, action) => {
    switch (action.type) {
        case MANIFEST_GET_ERROR:
            return action.manifestGetError
        default:
            return state
    }
}

const manifestGetSuccess = (state = false, action) => {
    switch (action.type) {
        case MANIFEST_GET_SUCCESS:
            return action.manifestGetSuccess
        default:
            return state
    }
}

const manifests = (state = [], action) => {
    switch (action.type) {
        case MANIFEST_GET_SUCCESS:
            return action.manifests
        default:
            return state
    }
}

const manifestViewLoading = (state = false, action) => {
    switch (action.type) {
        case MANIFEST_VIEW_LOADING:
            return action.manifestViewLoading
        default:
            return state
    }
}

const manifestViewError = (state = false, action) => {
    switch (action.type) {
        case MANIFEST_VIEW_ERROR:
            return action.manifestViewError
        default:
            return state
    }
}

const manifestViewSuccess = (state = false, action) => {
    switch (action.type) {
        case MANIFEST_VIEW_SUCCESS:
            return action.manifestViewSuccess
        default:
            return state
    }
}

const manifest = (state = [], action) => {
    switch (action.type) {
        case MANIFEST_VIEW_SUCCESS:
            return action.manifest
        default:
            return state
    }
}

const manifestReducer = combineReducers({
    manifestGetLoading,
    manifestGetError,
    manifestGetSuccess,
    manifests,

    manifestViewLoading,
    manifestViewError,
    manifestViewSuccess,
    manifest
})

export default manifestReducer