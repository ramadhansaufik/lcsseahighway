import {
    combineReducers
} from 'redux'
import {
    TRACK_GET_LOADING,
    TRACK_GET_ERROR,
    TRACK_GET_SUCCESS,

    TRACK_VIEW_ERROR,
    TRACK_VIEW_LOADING,
    TRACK_VIEW_SUCCESS,

    TRACK_ADD_ERROR,
    TRACK_ADD_LOADING,
    TRACK_ADD_SUCCESS
} from '../actions/constant'

const trackGetError = (state = false, action) => {
    switch (action.type) {
        case TRACK_GET_ERROR:
            return action.trackGetError
        default:
            return state
    }
}

const trackGetLoading = (state = false, action) => {
    switch (action.type) {
        case TRACK_GET_LOADING:
            return action.trackGetLoading
        default:
            return state
    }
}

const trackGetSuccess = (state = false, action) => {
    switch (action.type) {
        case TRACK_GET_SUCCESS:
            return action.trackGetSuccess
        default:
            return state
    }
}

const tracks = (state = [], action) => {
    switch (action.type) {
        case TRACK_GET_SUCCESS:
            return action.tracks
        default:
            return state
    }
}

const trackViewLoading = (state = false, action) => {
    switch (action.type) {
        case TRACK_VIEW_LOADING:
            return action.trackViewLoading
        default:
            return state
    }
}

const trackViewError = (state = false, action) => {
    switch (action.type) {
        case TRACK_VIEW_ERROR:
            return action.trackViewError
        default:
            return state
    }
}

const trackViewSuccess = (state = false, action) => {
    switch (action.type) {
        case TRACK_VIEW_SUCCESS:
            return action.trackViewSuccess
        default:
            return state
    }
}

const track = (state = [], action) => {
    switch (action.type) {
        case TRACK_VIEW_SUCCESS:
            return action.track
        default:
            return state
    }
}

const trackAddError = (state = false, action) => {
    switch (action.type) {
        case TRACK_ADD_ERROR:
            return action.trackAddError
        default:
            return state
    }
}

const trackAddLoading = (state = false, action) => {
    switch (action.type) {
        case TRACK_ADD_LOADING:
            return action.trackAddLoading
        default:
            return state
    }
}

const trackAddSuccess = (state = false, action) => {
    switch (action.type) {
        case TRACK_ADD_SUCCESS:
            return action.trackAddSuccess
        default:
            return state
    }
}

const trackReducer = combineReducers({
    trackGetError,
    trackGetLoading,
    trackGetSuccess,
    tracks,

    trackViewLoading,
    trackViewError,
    trackViewSuccess,
    track,

    trackAddError,
    trackAddLoading,
    trackAddSuccess
})

export default trackReducer