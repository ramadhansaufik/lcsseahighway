import {
    combineReducers
} from 'redux'
import {
    OPERATOR_GET_LOADING,
    OPERATOR_GET_ERROR,
    OPERATOR_GET_SUCCESS,

    OPERATOR_VIEW_ERROR,
    OPERATOR_VIEW_LOADING,
    OPERATOR_VIEW_SUCCESS,

    OPERATOR_ADD_ERROR,
    OPERATOR_ADD_LOADING,
    OPERATOR_ADD_INVALID,
    OPERATOR_ADD_SUCCESS,

    OPERATOR_DELETE_ERROR,
    OPERATOR_DELETE_LOADING,
    OPERATOR_DELETE_SUCCESS
} from '../actions/constant'

const operatorGetError = (state = false, action) => {
    switch (action.type) {
        case OPERATOR_GET_ERROR:
            return action.operatorGetError
        default:
            return state
    }
}

const operatorGetLoading = (state = false, action) => {
    switch (action.type) {
        case OPERATOR_GET_LOADING:
            return action.operatorGetLoading
        default:
            return state
    }
}

const operatorGetSuccess = (state = false, action) => {
    switch (action.type) {
        case OPERATOR_GET_SUCCESS:
            return action.operatorGetSuccess
        default:
            return state
    }
}

const operators = (state = [], action) => {
    switch (action.type) {
        case OPERATOR_GET_SUCCESS:
            return action.operators
        default:
            return state
    }
}

const operatorViewLoading = (state = false, action) => {
    switch (action.type) {
        case OPERATOR_VIEW_LOADING:
            return action.operatorViewLoading
        default:
            return state
    }
}

const operatorViewError = (state = false, action) => {
    switch (action.type) {
        case OPERATOR_VIEW_ERROR:
            return action.operatorViewError
        default:
            return state
    }
}

const operatorViewSuccess = (state = false, action) => {
    switch (action.type) {
        case OPERATOR_VIEW_SUCCESS:
            return action.operatorViewSuccess
        default:
            return state
    }
}

const operator = (state = [], action) => {
    switch (action.type) {
        case OPERATOR_VIEW_SUCCESS:
            return action.operator
        default:
            return state
    }
}

const operatorAddError = (state = false, action) => {
    switch (action.type) {
        case OPERATOR_ADD_ERROR:
            return action.operatorAddError
        default:
            return state
    }
}

const operatorAddLoading = (state = false, action) => {
    switch (action.type) {
        case OPERATOR_ADD_LOADING:
            return action.operatorAddLoading
        default:
            return state
    }
}

const operatorAddInvalid = (state = false, action) => {
    switch (action.type) {
        case OPERATOR_ADD_INVALID:
            return action.operatorAddInvalid
        default:
            return state
    }
}

const invalid = (state = [], action) => {
    switch (action.type) {
        case OPERATOR_ADD_INVALID:
            return action.invalid
        default:
            return state
    }
}

const operatorAddSuccess = (state = false, action) => {
    switch (action.type) {
        case OPERATOR_ADD_SUCCESS:
            return action.operatorAddSuccess
        default:
            return state
    }
}

const operatorDeleteError = (state = false, action) => {
    switch (action.type) {
        case OPERATOR_DELETE_ERROR:
            return action.operatorDeleteError
        default:
            return state
    }
}

const operatorDeleteLoading = (state = false, action) => {
    switch (action.type) {
        case OPERATOR_DELETE_LOADING:
            return action.operatorDeleteLoading
        default:
            return state
    }
}

const operatorDeleteSuccess = (state = false, action) => {
    switch (action.type) {
        case OPERATOR_DELETE_SUCCESS:
            return action.operatorDeleteSuccess
        default:
            return state
    }
}

const operatorReducer = combineReducers({
    operatorGetError,
    operatorGetLoading,
    operatorGetSuccess,
    operators,

    operatorViewLoading,
    operatorViewError,
    operatorViewSuccess,
    operator,

    operatorAddError,
    operatorAddLoading,
    operatorAddInvalid,
    operatorAddSuccess,
    invalid,

    operatorDeleteError,
    operatorDeleteLoading,
    operatorDeleteSuccess
})

export default operatorReducer