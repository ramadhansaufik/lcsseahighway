import {
    combineReducers
} from 'redux'
import {
    USER_GET_LOADING,
    USER_GET_ERROR,
    USER_GET_SUCCESS,

    USER_VIEW_ERROR,
    USER_VIEW_LOADING,
    USER_VIEW_SUCCESS,

    USER_ADD_ERROR,
    USER_ADD_LOADING,
    USER_ADD_SUCCESS,

    USER_DELETE_ERROR,
    USER_DELETE_LOADING,
    USER_DELETE_SUCCESS
} from '../actions/constant'

const userGetError = (state = false, action) => {
    switch (action.type) {
        case USER_GET_ERROR:
            return action.userGetError
        default:
            return state
    }
}

const userGetLoading = (state = false, action) => {
    switch (action.type) {
        case USER_GET_LOADING:
            return action.userGetLoading
        default:
            return state
    }
}

const userGetSuccess = (state = false, action) => {
    switch (action.type) {
        case USER_GET_SUCCESS:
            return action.userGetSuccess
        default:
            return state
    }
}

const users = (state = [], action) => {
    switch (action.type) {
        case USER_GET_SUCCESS:
            return action.users
        default:
            return state
    }
}

const userViewLoading = (state = false, action) => {
    switch (action.type) {
        case USER_VIEW_LOADING:
            return action.userViewLoading
        default:
            return state
    }
}

const userViewError = (state = false, action) => {
    switch (action.type) {
        case USER_VIEW_ERROR:
            return action.userViewError
        default:
            return state
    }
}

const userViewSuccess = (state = false, action) => {
    switch (action.type) {
        case USER_VIEW_SUCCESS:
            return action.userViewSuccess
        default:
            return state
    }
}

const user = (state = {}, action) => {
    switch (action.type) {
        case USER_VIEW_SUCCESS:
            return action.user
        default:
            return state
    }
}

const userAddError = (state = false, action) => {
    switch (action.type) {
        case USER_ADD_ERROR:
            return action.userAddError
        default:
            return state
    }
}

const userAddLoading = (state = false, action) => {
    switch (action.type) {
        case USER_ADD_LOADING:
            return action.userAddLoading
        default:
            return state
    }
}

const userAddSuccess = (state = false, action) => {
    switch (action.type) {
        case USER_ADD_SUCCESS:
            return action.userAddSuccess
        default:
            return state
    }
}

const userDeleteError = (state = false, action) => {
    switch (action.type) {
        case USER_DELETE_ERROR:
            return action.userDeleteError
        default:
            return state
    }
}

const userDeleteLoading = (state = false, action) => {
    switch (action.type) {
        case USER_DELETE_LOADING:
            return action.userDeleteLoading
        default:
            return state
    }
}

const userDeleteSuccess = (state = false, action) => {
    switch (action.type) {
        case USER_DELETE_SUCCESS:
            return action.userDeleteSuccess
        default:
            return state
    }
}

const userReducer = combineReducers({
    userGetError,
    userGetLoading,
    userGetSuccess,
    users,

    userViewLoading,
    userViewError,
    userViewSuccess,
    user,

    userAddError,
    userAddLoading,
    userAddSuccess,

    userDeleteError,
    userDeleteLoading,
    userDeleteSuccess
})

export default userReducer