import {
    combineReducers
} from 'redux'
import {
    DETAILPO_GET_LOADING,
    DETAILPO_GET_ERROR,
    DETAILPO_GET_SUCCESS,

    DETAILPO_VIEW_ERROR,
    DETAILPO_VIEW_LOADING,
    DETAILPO_VIEW_SUCCESS,

    DETAILPO_ADD_ERROR,
    DETAILPO_ADD_LOADING,
    DETAILPO_ADD_SUCCESS,

    DETAILPO_DELETE_ERROR,
    DETAILPO_DELETE_LOADING,
    DETAILPO_DELETE_SUCCESS
} from '../actions/constant'

const detailpoGetError = (state = false, action) => {
    switch (action.type) {
        case DETAILPO_GET_ERROR:
            return action.detailpoGetError
        default:
            return state
    }
}

const detailpoGetLoading = (state = false, action) => {
    switch (action.type) {
        case DETAILPO_GET_LOADING:
            return action.detailpoGetLoading
        default:
            return state
    }
}

const detailpoGetSuccess = (state = false, action) => {
    switch (action.type) {
        case DETAILPO_GET_SUCCESS:
            return action.detailpoGetSuccess
        default:
            return state
    }
}

const detailpos = (state = [], action) => {
    switch (action.type) {
        case DETAILPO_GET_SUCCESS:
            return action.detailpos
        default:
            return state
    }
}

const detailpoViewLoading = (state = false, action) => {
    switch (action.type) {
        case DETAILPO_VIEW_LOADING:
            return action.detailpoViewLoading
        default:
            return state
    }
}

const detailpoViewError = (state = false, action) => {
    switch (action.type) {
        case DETAILPO_VIEW_ERROR:
            return action.detailpoViewError
        default:
            return state
    }
}

const detailpoViewSuccess = (state = false, action) => {
    switch (action.type) {
        case DETAILPO_VIEW_SUCCESS:
            return action.detailpoViewSuccess
        default:
            return state
    }
}

const detailpo = (state = [], action) => {
    switch (action.type) {
        case DETAILPO_VIEW_SUCCESS:
            return action.detailpo
        default:
            return state
    }
}

const detailpoAddError = (state = false, action) => {
    switch (action.type) {
        case DETAILPO_ADD_ERROR:
            return action.detailpoAddError
        default:
            return state
    }
}

const detailpoAddLoading = (state = false, action) => {
    switch (action.type) {
        case DETAILPO_ADD_LOADING:
            return action.detailpoAddLoading
        default:
            return state
    }
}

const detailpoAddSuccess = (state = false, action) => {
    switch (action.type) {
        case DETAILPO_ADD_SUCCESS:
            return action.detailpoAddSuccess
        default:
            return state
    }
}

const detailpoDeleteError = (state = false, action) => {
    switch (action.type) {
        case DETAILPO_DELETE_ERROR:
            return action.detailpoDeleteError
        default:
            return state
    }
}

const detailpoDeleteLoading = (state = false, action) => {
    switch (action.type) {
        case DETAILPO_DELETE_LOADING:
            return action.detailpoDeleteLoading
        default:
            return state
    }
}

const detailpoDeleteSuccess = (state = false, action) => {
    switch (action.type) {
        case DETAILPO_DELETE_SUCCESS:
            return action.detailpoDeleteSuccess
        default:
            return state
    }
}

const detailpoReducer = combineReducers({
    detailpoGetError,
    detailpoGetLoading,
    detailpoGetSuccess,
    detailpos,

    detailpoViewLoading,
    detailpoViewError,
    detailpoViewSuccess,
    detailpo,

    detailpoAddError,
    detailpoAddLoading,
    detailpoAddSuccess,

    detailpoDeleteError,
    detailpoDeleteLoading,
    detailpoDeleteSuccess
})

export default detailpoReducer