import {
    combineReducers
} from 'redux'
import {
    COMPLAINT_GET_LOADING,
    COMPLAINT_GET_ERROR,
    COMPLAINT_GET_SUCCESS,

    COMPLAINT_VIEW_ERROR,
    COMPLAINT_VIEW_LOADING,
    COMPLAINT_VIEW_SUCCESS,

    COMPLAINT_ADD_ERROR,
    COMPLAINT_ADD_LOADING,
    COMPLAINT_ADD_SUCCESS,

    COMPLAINT_DELETE_ERROR,
    COMPLAINT_DELETE_LOADING,
    COMPLAINT_DELETE_SUCCESS
} from '../actions/constant'

const complaintGetError = (state = false, action) => {
    switch (action.type) {
        case COMPLAINT_GET_ERROR:
            return action.complaintGetError
        default:
            return state
    }
}

const complaintGetLoading = (state = false, action) => {
    switch (action.type) {
        case COMPLAINT_GET_LOADING:
            return action.complaintGetLoading
        default:
            return state
    }
}

const complaintGetSuccess = (state = false, action) => {
    switch (action.type) {
        case COMPLAINT_GET_SUCCESS:
            return action.complaintGetSuccess
        default:
            return state
    }
}

const complaints = (state = [], action) => {
    switch (action.type) {
        case COMPLAINT_GET_SUCCESS:
            return action.complaints
        default:
            return state
    }
}

const complaintViewLoading = (state = false, action) => {
    switch (action.type) {
        case COMPLAINT_VIEW_LOADING:
            return action.complaintViewLoading
        default:
            return state
    }
}

const complaintViewError = (state = false, action) => {
    switch (action.type) {
        case COMPLAINT_VIEW_ERROR:
            return action.complaintViewError
        default:
            return state
    }
}

const complaintViewSuccess = (state = false, action) => {
    switch (action.type) {
        case COMPLAINT_VIEW_SUCCESS:
            return action.complaintViewSuccess
        default:
            return state
    }
}

const complaint = (state = [], action) => {
    switch (action.type) {
        case COMPLAINT_VIEW_SUCCESS:
            return action.complaint
        default:
            return state
    }
}

const complaintAddError = (state = false, action) => {
    switch (action.type) {
        case COMPLAINT_ADD_ERROR:
            return action.complaintAddError
        default:
            return state
    }
}

const complaintAddLoading = (state = false, action) => {
    switch (action.type) {
        case COMPLAINT_ADD_LOADING:
            return action.complaintAddLoading
        default:
            return state
    }
}

const complaintAddSuccess = (state = false, action) => {
    switch (action.type) {
        case COMPLAINT_ADD_SUCCESS:
            return action.complaintAddSuccess
        default:
            return state
    }
}

const complaintDeleteError = (state = false, action) => {
    switch (action.type) {
        case COMPLAINT_DELETE_ERROR:
            return action.complaintDeleteError
        default:
            return state
    }
}

const complaintDeleteLoading = (state = false, action) => {
    switch (action.type) {
        case COMPLAINT_DELETE_LOADING:
            return action.complaintDeleteLoading
        default:
            return state
    }
}

const complaintDeleteSuccess = (state = false, action) => {
    switch (action.type) {
        case COMPLAINT_DELETE_SUCCESS:
            return action.complaintDeleteSuccess
        default:
            return state
    }
}

const complaintReducer = combineReducers({
    complaintGetError,
    complaintGetLoading,
    complaintGetSuccess,
    complaints,

    complaintViewLoading,
    complaintViewError,
    complaintViewSuccess,
    complaint,

    complaintAddError,
    complaintAddLoading,
    complaintAddSuccess,

    complaintDeleteError,
    complaintDeleteLoading,
    complaintDeleteSuccess
})

export default complaintReducer