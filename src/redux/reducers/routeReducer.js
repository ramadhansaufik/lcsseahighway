import {
    combineReducers
} from 'redux'
import {
    ROUTE_GET_LOADING,
    ROUTE_GET_ERROR,
    ROUTE_GET_SUCCESS,

    ROUTE_VIEW_ERROR,
    ROUTE_VIEW_LOADING,
    ROUTE_VIEW_SUCCESS,

    ROUTE_ADD_ERROR,
    ROUTE_ADD_LOADING,
    ROUTE_ADD_SUCCESS,

    ROUTE_DELETE_ERROR,
    ROUTE_DELETE_LOADING,
    ROUTE_DELETE_SUCCESS
} from '../actions/constant'

const routeGetError = (state = false, action) => {
    switch (action.type) {
        case ROUTE_GET_ERROR:
            return action.routeGetError
        default:
            return state
    }
}

const routeGetLoading = (state = false, action) => {
    switch (action.type) {
        case ROUTE_GET_LOADING:
            return action.routeGetLoading
        default:
            return state
    }
}

const routeGetSuccess = (state = false, action) => {
    switch (action.type) {
        case ROUTE_GET_SUCCESS:
            return action.routeGetSuccess
        default:
            return state
    }
}

const routes = (state = [], action) => {
    switch (action.type) {
        case ROUTE_GET_SUCCESS:
            return action.routes
        default:
            return state
    }
}

const routeViewLoading = (state = false, action) => {
    switch (action.type) {
        case ROUTE_VIEW_LOADING:
            return action.routeViewLoading
        default:
            return state
    }
}

const routeViewError = (state = false, action) => {
    switch (action.type) {
        case ROUTE_VIEW_ERROR:
            return action.routeViewError
        default:
            return state
    }
}

const routeViewSuccess = (state = false, action) => {
    switch (action.type) {
        case ROUTE_VIEW_SUCCESS:
            return action.routeViewSuccess
        default:
            return state
    }
}

const route = (state = [], action) => {
    switch (action.type) {
        case ROUTE_VIEW_SUCCESS:
            return action.route
        default:
            return state
    }
}

const routeAdded = (state = [], action) => {
    switch (action.type) {
        case ROUTE_ADD_SUCCESS:
            return action.route
        default:
            return state
    }
}

const routeAddError = (state = false, action) => {
    switch (action.type) {
        case ROUTE_ADD_ERROR:
            return action.routeAddError
        default:
            return state
    }
}

const routeAddLoading = (state = false, action) => {
    switch (action.type) {
        case ROUTE_ADD_LOADING:
            return action.routeAddLoading
        default:
            return state
    }
}

const routeAddSuccess = (state = false, action) => {
    switch (action.type) {
        case ROUTE_ADD_SUCCESS:
            return action.routeAddSuccess
        default:
            return state
    }
}

const routeDeleteError = (state = false, action) => {
    switch (action.type) {
        case ROUTE_DELETE_ERROR:
            return action.routeDeleteError
        default:
            return state
    }
}

const routeDeleteLoading = (state = false, action) => {
    switch (action.type) {
        case ROUTE_DELETE_LOADING:
            return action.routeDeleteLoading
        default:
            return state
    }
}

const routeDeleteSuccess = (state = false, action) => {
    switch (action.type) {
        case ROUTE_DELETE_SUCCESS:
            return action.routeDeleteSuccess
        default:
            return state
    }
}

const routeReducer = combineReducers({
    routeGetError,
    routeGetLoading,
    routeGetSuccess,
    routes,

    routeViewLoading,
    routeViewError,
    routeViewSuccess,
    route,

    routeAddError,
    routeAddLoading,
    routeAddSuccess,
    routeAdded,

    routeDeleteError,
    routeDeleteLoading,
    routeDeleteSuccess,
})

export default routeReducer