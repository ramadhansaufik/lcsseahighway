import {
    combineReducers
} from 'redux'
import {
    TRACKING_DATA_GET_LOADING,
    TRACKING_DATA_GET_ERROR,
    TRACKING_DATA_GET_SUCCESS,
} from '../actions/constant'

const trackingDataGetError = (state = false, action) => {
    switch (action.type) {
        case TRACKING_DATA_GET_ERROR:
            return action.trackingDataGetError
        default:
            return state
    }
}

const trackingDataGetLoading = (state = false, action) => {
    switch (action.type) {
        case TRACKING_DATA_GET_LOADING:
            return action.trackingDataGetLoading
        default:
            return state
    }
}

const trackingDataGetSuccess = (state = false, action) => {
    switch (action.type) {
        case TRACKING_DATA_GET_SUCCESS:
            return action.trackingDataGetSuccess
        default:
            return state
    }
}

const trackingData = (state = [], action) => {
    switch (action.type) {
        case TRACKING_DATA_GET_SUCCESS:
            return action.trackingData
        default:
            return state
    }
}

const trackingDataReducer = combineReducers({
    trackingDataGetError,
    trackingDataGetLoading,
    trackingDataGetSuccess,
    trackingData,
})

export default trackingDataReducer