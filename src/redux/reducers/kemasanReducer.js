import {
    combineReducers
} from 'redux'
import {
    KEMASAN_GET_LOADING,
    KEMASAN_GET_ERROR,
    KEMASAN_GET_SUCCESS,

    KEMASAN_VIEW_ERROR,
    KEMASAN_VIEW_LOADING,
    KEMASAN_VIEW_SUCCESS,

    KEMASAN_ADD_ERROR,
    KEMASAN_ADD_LOADING,
    KEMASAN_ADD_SUCCESS
} from '../actions/constant'

const kemasanGetError = (state = false, action) => {
    switch (action.type) {
        case KEMASAN_GET_ERROR:
            return action.kemasanGetError
        default:
            return state
    }
}

const kemasanGetLoading = (state = false, action) => {
    switch (action.type) {
        case KEMASAN_GET_LOADING:
            return action.kemasanGetLoading
        default:
            return state
    }
}

const kemasanGetSuccess = (state = false, action) => {
    switch (action.type) {
        case KEMASAN_GET_SUCCESS:
            return action.kemasanGetSuccess
        default:
            return state
    }
}

const kemasans = (state = [], action) => {
    switch (action.type) {
        case KEMASAN_GET_SUCCESS:
            return action.kemasans
        default:
            return state
    }
}

const kemasanViewLoading = (state = false, action) => {
    switch (action.type) {
        case KEMASAN_VIEW_LOADING:
            return action.kemasanViewLoading
        default:
            return state
    }
}

const kemasanViewError = (state = false, action) => {
    switch (action.type) {
        case KEMASAN_VIEW_ERROR:
            return action.kemasanViewError
        default:
            return state
    }
}

const kemasanViewSuccess = (state = false, action) => {
    switch (action.type) {
        case KEMASAN_VIEW_SUCCESS:
            return action.kemasanViewSuccess
        default:
            return state
    }
}

const kemasan = (state = [], action) => {
    switch (action.type) {
        case KEMASAN_VIEW_SUCCESS:
            return action.kemasan
        case KEMASAN_ADD_SUCCESS:
            return action.kemasan
        default:
            return state
    }
}

const kemasanAddError = (state = false, action) => {
    switch (action.type) {
        case KEMASAN_ADD_ERROR:
            return action.kemasanAddError
        default:
            return state
    }
}

const kemasanAddLoading = (state = false, action) => {
    switch (action.type) {
        case KEMASAN_ADD_LOADING:
            return action.kemasanAddLoading
        default:
            return state
    }
}

const kemasanAddSuccess = (state = false, action) => {
    switch (action.type) {
        case KEMASAN_ADD_SUCCESS:
            return action.kemasanAddSuccess
        default:
            return state
    }
}

const kemasanReducer = combineReducers({
    kemasanGetError,
    kemasanGetLoading,
    kemasanGetSuccess,
    kemasans,

    kemasanViewLoading,
    kemasanViewError,
    kemasanViewSuccess,
    kemasan,

    kemasanAddError,
    kemasanAddLoading,
    kemasanAddSuccess
})

export default kemasanReducer