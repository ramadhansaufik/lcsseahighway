import {
    combineReducers
} from 'redux'
import {
    SHIPPER_GET_LOADING,
    SHIPPER_GET_ERROR,
    SHIPPER_GET_SUCCESS,

    SHIPPER_VIEW_ERROR,
    SHIPPER_VIEW_LOADING,
    SHIPPER_VIEW_SUCCESS,

    SHIPPER_ADD_ERROR,
    SHIPPER_ADD_LOADING,
    SHIPPER_ADD_INVALID,
    SHIPPER_ADD_SUCCESS,

    SHIPPER_DELETE_ERROR,
    SHIPPER_DELETE_LOADING,
    SHIPPER_DELETE_SUCCESS
} from '../actions/constant'

const shipperGetError = (state = false, action) => {
    switch (action.type) {
        case SHIPPER_GET_ERROR:
            return action.shipperGetError
        default:
            return state
    }
}

const shipperGetLoading = (state = false, action) => {
    switch (action.type) {
        case SHIPPER_GET_LOADING:
            return action.shipperGetLoading
        default:
            return state
    }
}

const shipperGetSuccess = (state = false, action) => {
    switch (action.type) {
        case SHIPPER_GET_SUCCESS:
            return action.shipperGetSuccess
        default:
            return state
    }
}

const shippers = (state = [], action) => {
    switch (action.type) {
        case SHIPPER_GET_SUCCESS:
            return action.shippers
        default:
            return state
    }
}

const shipperViewLoading = (state = false, action) => {
    switch (action.type) {
        case SHIPPER_VIEW_LOADING:
            return action.shipperViewLoading
        default:
            return state
    }
}

const shipperViewError = (state = false, action) => {
    switch (action.type) {
        case SHIPPER_VIEW_ERROR:
            return action.shipperViewError
        default:
            return state
    }
}

const shipperViewSuccess = (state = false, action) => {
    switch (action.type) {
        case SHIPPER_VIEW_SUCCESS:
            return action.shipperViewSuccess
        default:
            return state
    }
}

const shipper = (state = [], action) => {
    switch (action.type) {
        case SHIPPER_VIEW_SUCCESS:
            return action.shipper
        default:
            return state
    }
}

const shipperAddError = (state = false, action) => {
    switch (action.type) {
        case SHIPPER_ADD_ERROR:
            return action.shipperAddError
        default:
            return state
    }
}

const shipperAddLoading = (state = false, action) => {
    switch (action.type) {
        case SHIPPER_ADD_LOADING:
            return action.shipperAddLoading
        default:
            return state
    }
}

const shipperAddInvalid = (state = false, action) => {
    switch (action.type) {
        case SHIPPER_ADD_INVALID:
            return action.shipperAddInvalid
        default:
            return state
    }
}

const invalid = (state = [], action) => {
    switch (action.type) {
        case SHIPPER_ADD_INVALID:
            return action.invalid
        default:
            return state
    }
}

const shipperAddSuccess = (state = false, action) => {
    switch (action.type) {
        case SHIPPER_ADD_SUCCESS:
            return action.shipperAddSuccess
        default:
            return state
    }
}

const shipperDeleteError = (state = false, action) => {
    switch (action.type) {
        case SHIPPER_DELETE_ERROR:
            return action.shipperDeleteError
        default:
            return state
    }
}

const shipperDeleteLoading = (state = false, action) => {
    switch (action.type) {
        case SHIPPER_DELETE_LOADING:
            return action.shipperDeleteLoading
        default:
            return state
    }
}

const shipperDeleteSuccess = (state = false, action) => {
    switch (action.type) {
        case SHIPPER_DELETE_SUCCESS:
            return action.shipperDeleteSuccess
        default:
            return state
    }
}

const shipperReducer = combineReducers({
    shipperGetError,
    shipperGetLoading,
    shipperGetSuccess,
    shippers,

    shipperViewLoading,
    shipperViewError,
    shipperViewSuccess,
    shipper,

    shipperAddError,
    shipperAddLoading,
    shipperAddInvalid,
    shipperAddSuccess,
    invalid,

    shipperDeleteError,
    shipperDeleteLoading,
    shipperDeleteSuccess
})

export default shipperReducer