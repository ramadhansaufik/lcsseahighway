import {
    combineReducers
} from 'redux'
import {
    RESELLER_GET_LOADING,
    RESELLER_GET_ERROR,
    RESELLER_GET_SUCCESS,

    RESELLER_VIEW_ERROR,
    RESELLER_VIEW_LOADING,
    RESELLER_VIEW_SUCCESS,

    RESELLER_ADD_ERROR,
    RESELLER_ADD_LOADING,
    RESELLER_ADD_INVALID,
    RESELLER_ADD_SUCCESS,

    RESELLER_DELETE_ERROR,
    RESELLER_DELETE_LOADING,
    RESELLER_DELETE_SUCCESS
} from '../actions/constant'

const resellerGetError = (state = false, action) => {
    switch (action.type) {
        case RESELLER_GET_ERROR:
            return action.resellerGetError
        default:
            return state
    }
}

const resellerGetLoading = (state = false, action) => {
    switch (action.type) {
        case RESELLER_GET_LOADING:
            return action.resellerGetLoading
        default:
            return state
    }
}

const resellerGetSuccess = (state = false, action) => {
    switch (action.type) {
        case RESELLER_GET_SUCCESS:
            return action.resellerGetSuccess
        default:
            return state
    }
}

const resellers = (state = [], action) => {
    switch (action.type) {
        case RESELLER_GET_SUCCESS:
            return action.resellers
        default:
            return state
    }
}

const resellerViewLoading = (state = false, action) => {
    switch (action.type) {
        case RESELLER_VIEW_LOADING:
            return action.resellerViewLoading
        default:
            return state
    }
}

const resellerViewError = (state = false, action) => {
    switch (action.type) {
        case RESELLER_VIEW_ERROR:
            return action.resellerViewError
        default:
            return state
    }
}

const resellerViewSuccess = (state = false, action) => {
    switch (action.type) {
        case RESELLER_VIEW_SUCCESS:
            return action.resellerViewSuccess
        default:
            return state
    }
}

const reseller = (state = [], action) => {
    switch (action.type) {
        case RESELLER_VIEW_SUCCESS:
            return action.reseller
        default:
            return state
    }
}

const resellerAddError = (state = false, action) => {
    switch (action.type) {
        case RESELLER_ADD_ERROR:
            return action.resellerAddError
        default:
            return state
    }
}

const resellerAddLoading = (state = false, action) => {
    switch (action.type) {
        case RESELLER_ADD_LOADING:
            return action.resellerAddLoading
        default:
            return state
    }
}

const resellerAddInvalid = (state = false, action) => {
    switch (action.type) {
        case RESELLER_ADD_INVALID:
            return action.resellerAddInvalid
        default:
            return state
    }
}

const invalid = (state = [], action) => {
    switch (action.type) {
        case RESELLER_ADD_INVALID:
            return action.invalid
        default:
            return state
    }
}

const resellerAddSuccess = (state = false, action) => {
    switch (action.type) {
        case RESELLER_ADD_SUCCESS:
            return action.resellerAddSuccess
        default:
            return state
    }
}

const resellerDeleteError = (state = false, action) => {
    switch (action.type) {
        case RESELLER_DELETE_ERROR:
            return action.resellerDeleteError
        default:
            return state
    }
}

const resellerDeleteLoading = (state = false, action) => {
    switch (action.type) {
        case RESELLER_DELETE_LOADING:
            return action.resellerDeleteLoading
        default:
            return state
    }
}

const resellerDeleteSuccess = (state = false, action) => {
    switch (action.type) {
        case RESELLER_DELETE_SUCCESS:
            return action.resellerDeleteSuccess
        default:
            return state
    }
}

const resellerReducer = combineReducers({
    resellerGetError,
    resellerGetLoading,
    resellerGetSuccess,
    resellers,

    resellerViewLoading,
    resellerViewError,
    resellerViewSuccess,
    reseller,

    resellerAddError,
    resellerAddLoading,
    resellerAddInvalid,
    resellerAddSuccess,
    invalid,

    resellerDeleteError,
    resellerDeleteLoading,
    resellerDeleteSuccess
})

export default resellerReducer