import { combineReducers } from 'redux'
import auth from './auth.js'
// import data from './data.js'
// import notification from './notification.js'

export default combineReducers({
	auth: auth,
})