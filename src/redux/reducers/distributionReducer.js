import {
    combineReducers
} from 'redux'
import {
    DISTRIBUTION_GET_LOADING,
    DISTRIBUTION_GET_ERROR,
    DISTRIBUTION_GET_SUCCESS,

    DISTRIBUTION_VIEW_ERROR,
    DISTRIBUTION_VIEW_LOADING,
    DISTRIBUTION_VIEW_SUCCESS,

    DISTRIBUTION_ADD_ERROR,
    DISTRIBUTION_ADD_LOADING,
    DISTRIBUTION_ADD_SUCCESS
} from '../actions/constant'

const distributionGetError = (state = false, action) => {
    switch (action.type) {
        case DISTRIBUTION_GET_ERROR:
            return action.distributionGetError
        default:
            return state
    }
}

const distributionGetLoading = (state = false, action) => {
    switch (action.type) {
        case DISTRIBUTION_GET_LOADING:
            return action.distributionGetLoading
        default:
            return state
    }
}

const distributionGetSuccess = (state = false, action) => {
    switch (action.type) {
        case DISTRIBUTION_GET_SUCCESS:
            return action.distributionGetSuccess
        default:
            return state
    }
}

const distributions = (state = [], action) => {
    switch (action.type) {
        case DISTRIBUTION_GET_SUCCESS:
            return action.distributions
        default:
            return state
    }
}

const distributionViewLoading = (state = false, action) => {
    switch (action.type) {
        case DISTRIBUTION_VIEW_LOADING:
            return action.distributionViewLoading
        default:
            return state
    }
}

const distributionViewError = (state = false, action) => {
    switch (action.type) {
        case DISTRIBUTION_VIEW_ERROR:
            return action.distributionViewError
        default:
            return state
    }
}

const distributionViewSuccess = (state = false, action) => {
    switch (action.type) {
        case DISTRIBUTION_VIEW_SUCCESS:
            return action.distributionViewSuccess
        default:
            return state
    }
}

const distribution = (state = [], action) => {
    switch (action.type) {
        case DISTRIBUTION_VIEW_SUCCESS:
            return action.distribution
        default:
            return state
    }
}

const distributionAddError = (state = false, action) => {
    switch (action.type) {
        case DISTRIBUTION_ADD_ERROR:
            return action.distributionAddError
        default:
            return state
    }
}

const distributionAddLoading = (state = false, action) => {
    switch (action.type) {
        case DISTRIBUTION_ADD_LOADING:
            return action.distributionAddLoading
        default:
            return state
    }
}

const distributionAddSuccess = (state = false, action) => {
    switch (action.type) {
        case DISTRIBUTION_ADD_SUCCESS:
            return action.distributionAddSuccess
        default:
            return state
    }
}

const distributionReducer = combineReducers({
    distributionGetError,
    distributionGetLoading,
    distributionGetSuccess,
    distributions,

    distributionViewLoading,
    distributionViewError,
    distributionViewSuccess,
    distribution,

    distributionAddError,
    distributionAddLoading,
    distributionAddSuccess
})

export default distributionReducer