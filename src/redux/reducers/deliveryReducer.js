import {
    combineReducers
} from 'redux'
import {
    DELIVERY_GET_LOADING,
    DELIVERY_GET_ERROR,
    DELIVERY_GET_SUCCESS,

    DELIVERY_VIEW_ERROR,
    DELIVERY_VIEW_LOADING,
    DELIVERY_VIEW_SUCCESS,

    DELIVERY_ADD_ERROR,
    DELIVERY_ADD_LOADING,
    DELIVERY_ADD_SUCCESS,

    DELIVERY_DELETE_ERROR,
    DELIVERY_DELETE_LOADING,
    DELIVERY_DELETE_SUCCESS
} from '../actions/constant'

const deliveryGetError = (state = false, action) => {
    switch (action.type) {
        case DELIVERY_GET_ERROR:
            return action.deliveryGetError
        default:
            return state
    }
}

const deliveryGetLoading = (state = false, action) => {
    switch (action.type) {
        case DELIVERY_GET_LOADING:
            return action.deliveryGetLoading
        default:
            return state
    }
}

const deliveryGetSuccess = (state = false, action) => {
    switch (action.type) {
        case DELIVERY_GET_SUCCESS:
            return action.deliveryGetSuccess
        default:
            return state
    }
}

const deliveries = (state = [], action) => {
    switch (action.type) {
        case DELIVERY_GET_SUCCESS:
            return action.deliveries
        default:
            return state
    }
}

const deliveryViewLoading = (state = false, action) => {
    switch (action.type) {
        case DELIVERY_VIEW_LOADING:
            return action.deliveryViewLoading
        default:
            return state
    }
}

const deliveryViewError = (state = false, action) => {
    switch (action.type) {
        case DELIVERY_VIEW_ERROR:
            return action.deliveryViewError
        default:
            return state
    }
}

const deliveryViewSuccess = (state = false, action) => {
    switch (action.type) {
        case DELIVERY_VIEW_SUCCESS:
            return action.deliveryViewSuccess
        default:
            return state
    }
}

const delivery = (state = [], action) => {
    switch (action.type) {
        case DELIVERY_VIEW_SUCCESS:
            return action.delivery
        default:
            return state
    }
}

const deliveryAddError = (state = false, action) => {
    switch (action.type) {
        case DELIVERY_ADD_ERROR:
            return action.deliveryAddError
        default:
            return state
    }
}

const deliveryAddLoading = (state = false, action) => {
    switch (action.type) {
        case DELIVERY_ADD_LOADING:
            return action.deliveryAddLoading
        default:
            return state
    }
}

const deliveryAddSuccess = (state = false, action) => {
    switch (action.type) {
        case DELIVERY_ADD_SUCCESS:
            return action.deliveryAddSuccess
        default:
            return state
    }
}

const deliveryAdded = (state = [], action) => {
    switch (action.type) {
        case DELIVERY_ADD_SUCCESS:
            return action.delivery
        default:
            return state
    }
}

const deliveryDeleteError = (state = false, action) => {
    switch (action.type) {
        case DELIVERY_DELETE_ERROR:
            return action.deliveryDeleteError
        default:
            return state
    }
}

const deliveryDeleteLoading = (state = false, action) => {
    switch (action.type) {
        case DELIVERY_DELETE_LOADING:
            return action.deliveryDeleteLoading
        default:
            return state
    }
}

const deliveryDeleteSuccess = (state = false, action) => {
    switch (action.type) {
        case DELIVERY_DELETE_SUCCESS:
            return action.deliveryDeleteSuccess
        default:
            return state
    }
}

const deliveryReducer = combineReducers({
    deliveryGetError,
    deliveryGetLoading,
    deliveryGetSuccess,
    deliveries,

    deliveryViewLoading,
    deliveryViewError,
    deliveryViewSuccess,
    delivery,

    deliveryAddError,
    deliveryAddLoading,
    deliveryAddSuccess,
    deliveryAdded,

    deliveryDeleteError,
    deliveryDeleteLoading,
    deliveryDeleteSuccess
})

export default deliveryReducer