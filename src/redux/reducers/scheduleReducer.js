import {
    combineReducers
} from 'redux'
import {
    SCHEDULE_GET_LOADING,
    SCHEDULE_GET_ERROR,
    SCHEDULE_GET_SUCCESS,

    SCHEDULE_VIEW_ERROR,
    SCHEDULE_VIEW_LOADING,
    SCHEDULE_VIEW_SUCCESS,

    SCHEDULE_ADD_ERROR,
    SCHEDULE_ADD_LOADING,
    SCHEDULE_ADD_SUCCESS,

    SCHEDULE_DELETE_ERROR,
    SCHEDULE_DELETE_LOADING,
    SCHEDULE_DELETE_SUCCESS
} from '../actions/constant'

const scheduleGetError = (state = false, action) => {
    switch (action.type) {
        case SCHEDULE_GET_ERROR:
            return action.scheduleGetError
        default:
            return state
    }
}

const scheduleGetLoading = (state = false, action) => {
    switch (action.type) {
        case SCHEDULE_GET_LOADING:
            return action.scheduleGetLoading
        default:
            return state
    }
}

const scheduleGetSuccess = (state = false, action) => {
    switch (action.type) {
        case SCHEDULE_GET_SUCCESS:
            return action.scheduleGetSuccess
        default:
            return state
    }
}

const schedules = (state = [], action) => {
    switch (action.type) {
        case SCHEDULE_GET_SUCCESS:
            return action.schedules
        default:
            return state
    }
}

const scheduleViewLoading = (state = false, action) => {
    switch (action.type) {
        case SCHEDULE_VIEW_LOADING:
            return action.scheduleViewLoading
        default:
            return state
    }
}

const scheduleViewError = (state = false, action) => {
    switch (action.type) {
        case SCHEDULE_VIEW_ERROR:
            return action.scheduleViewError
        default:
            return state
    }
}

const scheduleViewSuccess = (state = false, action) => {
    switch (action.type) {
        case SCHEDULE_VIEW_SUCCESS:
            return action.scheduleViewSuccess
        default:
            return state
    }
}

const schedule = (state = [], action) => {
    switch (action.type) {
        case SCHEDULE_VIEW_SUCCESS:
            return action.schedule
        default:
            return state
    }
}

const scheduleAddError = (state = false, action) => {
    switch (action.type) {
        case SCHEDULE_ADD_ERROR:
            return action.scheduleAddError
        default:
            return state
    }
}

const scheduleAddLoading = (state = false, action) => {
    switch (action.type) {
        case SCHEDULE_ADD_LOADING:
            return action.scheduleAddLoading
        default:
            return state
    }
}

const scheduleAddSuccess = (state = false, action) => {
    switch (action.type) {
        case SCHEDULE_ADD_SUCCESS:
            return action.scheduleAddSuccess
        default:
            return state
    }
}

const scheduleDeleteError = (state = false, action) => {
    switch (action.type) {
        case SCHEDULE_DELETE_ERROR:
            return action.scheduleDeleteError
        default:
            return state
    }
}

const scheduleDeleteLoading = (state = false, action) => {
    switch (action.type) {
        case SCHEDULE_DELETE_LOADING:
            return action.scheduleDeleteLoading
        default:
            return state
    }
}

const scheduleDeleteSuccess = (state = false, action) => {
    switch (action.type) {
        case SCHEDULE_DELETE_SUCCESS:
            return action.scheduleDeleteSuccess
        default:
            return state
    }
}

const scheduleReducer = combineReducers({
    scheduleGetError,
    scheduleGetLoading,
    scheduleGetSuccess,
    schedules,

    scheduleViewLoading,
    scheduleViewError,
    scheduleViewSuccess,
    schedule,

    scheduleAddError,
    scheduleAddLoading,
    scheduleAddSuccess,

    scheduleDeleteError,
    scheduleDeleteLoading,
    scheduleDeleteSuccess
})

export default scheduleReducer