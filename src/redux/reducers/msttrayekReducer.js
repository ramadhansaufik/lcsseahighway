import {
    combineReducers
} from 'redux'
import {
    MSTTRAYEK_GET_LOADING,
    MSTTRAYEK_GET_ERROR,
    MSTTRAYEK_GET_SUCCESS,

    MSTTRAYEK_VIEW_ERROR,
    MSTTRAYEK_VIEW_LOADING,
    MSTTRAYEK_VIEW_SUCCESS,

    MSTTRAYEK_ADD_ERROR,
    MSTTRAYEK_ADD_LOADING,
    MSTTRAYEK_ADD_SUCCESS,

    MSTTRAYEK_DELETE_ERROR,
    MSTTRAYEK_DELETE_LOADING,
    MSTTRAYEK_DELETE_SUCCESS
} from '../actions/constant'

const msttrayekGetError = (state = false, action) => {
    switch (action.type) {
        case MSTTRAYEK_GET_ERROR:
            return action.msttrayekGetError
        default:
            return state
    }
}

const msttrayekGetLoading = (state = false, action) => {
    switch (action.type) {
        case MSTTRAYEK_GET_LOADING:
            return action.msttrayekGetLoading
        default:
            return state
    }
}

const msttrayekGetSuccess = (state = false, action) => {
    switch (action.type) {
        case MSTTRAYEK_GET_SUCCESS:
            return action.msttrayekGetSuccess
        default:
            return state
    }
}

const msttrayeks = (state = [], action) => {
    switch (action.type) {
        case MSTTRAYEK_GET_SUCCESS:
            return action.msttrayeks
        default:
            return state
    }
}

const msttrayekViewLoading = (state = false, action) => {
    switch (action.type) {
        case MSTTRAYEK_VIEW_LOADING:
            return action.msttrayekViewLoading
        default:
            return state
    }
}

const msttrayekViewError = (state = false, action) => {
    switch (action.type) {
        case MSTTRAYEK_VIEW_ERROR:
            return action.msttrayekViewError
        default:
            return state
    }
}

const msttrayekViewSuccess = (state = false, action) => {
    switch (action.type) {
        case MSTTRAYEK_VIEW_SUCCESS:
            return action.msttrayekViewSuccess
        default:
            return state
    }
}

const msttrayek = (state = [], action) => {
    switch (action.type) {
        case MSTTRAYEK_VIEW_SUCCESS:
            return action.msttrayek
        default:
            return state
    }
}

const msttrayekAdded = (state = [], action) => {
    switch (action.type) {
        case MSTTRAYEK_ADD_SUCCESS:
            return action.msttrayek
        default:
            return state
    }
}

const msttrayekAddError = (state = false, action) => {
    switch (action.type) {
        case MSTTRAYEK_ADD_ERROR:
            return action.msttrayekAddError
        default:
            return state
    }
}

const msttrayekAddLoading = (state = false, action) => {
    switch (action.type) {
        case MSTTRAYEK_ADD_LOADING:
            return action.msttrayekAddLoading
        default:
            return state
    }
}

const msttrayekAddSuccess = (state = false, action) => {
    switch (action.type) {
        case MSTTRAYEK_ADD_SUCCESS:
            return action.msttrayekAddSuccess
        default:
            return state
    }
}

const msttrayekDeleteError = (state = false, action) => {
    switch (action.type) {
        case MSTTRAYEK_DELETE_ERROR:
            return action.msttrayekDeleteError
        default:
            return state
    }
}

const msttrayekDeleteLoading = (state = false, action) => {
    switch (action.type) {
        case MSTTRAYEK_DELETE_LOADING:
            return action.msttrayekDeleteLoading
        default:
            return state
    }
}

const msttrayekDeleteSuccess = (state = false, action) => {
    switch (action.type) {
        case MSTTRAYEK_DELETE_SUCCESS:
            return action.msttrayekDeleteSuccess
        default:
            return state
    }
}

const msttrayekReducer = combineReducers({
    msttrayekGetError,
    msttrayekGetLoading,
    msttrayekGetSuccess,
    msttrayeks,

    msttrayekViewLoading,
    msttrayekViewError,
    msttrayekViewSuccess,
    msttrayek,

    msttrayekAddError,
    msttrayekAddLoading,
    msttrayekAddSuccess,
    msttrayekAdded,

    msttrayekDeleteError,
    msttrayekDeleteLoading,
    msttrayekDeleteSuccess
})

export default msttrayekReducer