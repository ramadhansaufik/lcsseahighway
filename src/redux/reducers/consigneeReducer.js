import {
    combineReducers
} from 'redux'
import {
    CONSIGNEE_GET_LOADING,
    CONSIGNEE_GET_ERROR,
    CONSIGNEE_GET_SUCCESS,

    CONSIGNEE_VIEW_ERROR,
    CONSIGNEE_VIEW_LOADING,
    CONSIGNEE_VIEW_SUCCESS,

    CONSIGNEE_ADD_ERROR,
    CONSIGNEE_ADD_LOADING,
    CONSIGNEE_ADD_INVALID,
    CONSIGNEE_ADD_SUCCESS,

    CONSIGNEE_DELETE_ERROR,
    CONSIGNEE_DELETE_LOADING,
    CONSIGNEE_DELETE_SUCCESS
} from '../actions/constant'

const consigneeGetError = (state = false, action) => {
    switch (action.type) {
        case CONSIGNEE_GET_ERROR:
            return action.consigneeGetError
        default:
            return state
    }
}

const consigneeGetLoading = (state = false, action) => {
    switch (action.type) {
        case CONSIGNEE_GET_LOADING:
            return action.consigneeGetLoading
        default:
            return state
    }
}

const consigneeGetSuccess = (state = false, action) => {
    switch (action.type) {
        case CONSIGNEE_GET_SUCCESS:
            return action.consigneeGetSuccess
        default:
            return state
    }
}

const consignees = (state = [], action) => {
    switch (action.type) {
        case CONSIGNEE_GET_SUCCESS:
            return action.consignees
        default:
            return state
    }
}

const consigneeViewLoading = (state = false, action) => {
    switch (action.type) {
        case CONSIGNEE_VIEW_LOADING:
            return action.consigneeViewLoading
        default:
            return state
    }
}

const consigneeViewError = (state = false, action) => {
    switch (action.type) {
        case CONSIGNEE_VIEW_ERROR:
            return action.consigneeViewError
        default:
            return state
    }
}

const consigneeViewSuccess = (state = false, action) => {
    switch (action.type) {
        case CONSIGNEE_VIEW_SUCCESS:
            return action.consigneeViewSuccess
        default:
            return state
    }
}

const consignee = (state = [], action) => {
    switch (action.type) {
        case CONSIGNEE_VIEW_SUCCESS:
            return action.consignee
        default:
            return state
    }
}

const consigneeAddError = (state = false, action) => {
    switch (action.type) {
        case CONSIGNEE_ADD_ERROR:
            return action.consigneeAddError
        default:
            return state
    }
}

const consigneeAddLoading = (state = false, action) => {
    switch (action.type) {
        case CONSIGNEE_ADD_LOADING:
            return action.consigneeAddLoading
        default:
            return state
    }
}

const consigneeAddInvalid = (state = false, action) => {
    switch (action.type) {
        case CONSIGNEE_ADD_INVALID:
            return action.consigneeAddInvalid
        default:
            return state
    }
}

const invalid = (state = [], action) => {
    switch (action.type) {
        case CONSIGNEE_ADD_INVALID:
            return action.invalid
        default:
            return state
    }
}

const consigneeAddSuccess = (state = false, action) => {
    switch (action.type) {
        case CONSIGNEE_ADD_SUCCESS:
            return action.consigneeAddSuccess
        default:
            return state
    }
}

const consigneeDeleteError = (state = false, action) => {
    switch (action.type) {
        case CONSIGNEE_DELETE_ERROR:
            return action.consigneeDeleteError
        default:
            return state
    }
}

const consigneeDeleteLoading = (state = false, action) => {
    switch (action.type) {
        case CONSIGNEE_DELETE_LOADING:
            return action.consigneeDeleteLoading
        default:
            return state
    }
}

const consigneeDeleteSuccess = (state = false, action) => {
    switch (action.type) {
        case CONSIGNEE_DELETE_SUCCESS:
            return action.consigneeDeleteSuccess
        default:
            return state
    }
}

const consigneeReducer = combineReducers({
    consigneeGetError,
    consigneeGetLoading,
    consigneeGetSuccess,
    consignees,

    consigneeViewLoading,
    consigneeViewError,
    consigneeViewSuccess,
    consignee,

    consigneeAddError,
    consigneeAddLoading,
    consigneeAddInvalid,
    invalid,
    consigneeAddSuccess,

    consigneeDeleteError,
    consigneeDeleteLoading,
    consigneeDeleteSuccess
})

export default consigneeReducer