const initialState = {
	loading: false,
	payload: {},
	error: ''
}

export default function auth (state = initialState, action) {
	switch(action.type) {

		case 'AUTH_LOGIN':
			return {
				...state,
				loading: true,
			}
		case 'AUTH_LOGIN_SUCCESS':
			return {
				loading: false,
				payload: action.payload,
				error: ''
			}
		case 'AUTH_LOGIN_FAILED':
			return {
				loading: false,
				payload: {},
				error: action.payload
			}

		case 'AUTH_REGISTER':
			return {
				...state,
				loading: true,
			}
		case 'AUTH_LOGIN_SUCCESS':
			return {
				loading: false,
				payload: action.payload,
				error: ''
			}
		case 'AUTH_LOGIN_FAILED':
			return {
				loading: false,
				payload: {},
				error: action.payload
			}
			
		default : return initialState
	}
}