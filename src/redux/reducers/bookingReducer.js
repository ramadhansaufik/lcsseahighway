import {
    combineReducers
} from 'redux'
import {
    BOOKING_GET_LOADING,
    BOOKING_GET_ERROR,
    BOOKING_GET_SUCCESS,

    BOOKING_VIEW_ERROR,
    BOOKING_VIEW_LOADING,
    BOOKING_VIEW_SUCCESS,

    BOOKING_ADD_ERROR,
    BOOKING_ADD_LOADING,
    BOOKING_ADD_SUCCESS,

    BOOKING_HAS_DO_VIEW_ERROR,
    BOOKING_HAS_DO_VIEW_LOADING,
    BOOKING_HAS_DO_VIEW_SUCCESS,

    BOOKING_HAS_DO_UPDATE_ERROR,
    BOOKING_HAS_DO_UPDATE_LOADING,
    BOOKING_HAS_DO_UPDATE_INVALID,
    BOOKING_HAS_DO_UPDATE_SUCCESS
} from '../actions/constant'

const bookingGetError = (state = false, action) => {
    switch (action.type) {
        case BOOKING_GET_ERROR:
            return action.bookingGetError
        default:
            return state
    }
}

const bookingGetLoading = (state = false, action) => {
    switch (action.type) {
        case BOOKING_GET_LOADING:
            return action.bookingGetLoading
        default:
            return state
    }
}

const bookingGetSuccess = (state = false, action) => {
    switch (action.type) {
        case BOOKING_GET_SUCCESS:
            return action.bookingGetSuccess
        default:
            return state
    }
}

const bookings = (state = [], action) => {
    switch (action.type) {
        case BOOKING_GET_SUCCESS:
            return action.bookings
        default:
            return state
    }
}

const bookingViewLoading = (state = false, action) => {
    switch (action.type) {
        case BOOKING_VIEW_LOADING:
            return action.bookingViewLoading
        default:
            return state
    }
}

const bookingViewError = (state = false, action) => {
    switch (action.type) {
        case BOOKING_VIEW_ERROR:
            return action.bookingViewError
        default:
            return state
    }
}

const bookingViewSuccess = (state = false, action) => {
    switch (action.type) {
        case BOOKING_VIEW_SUCCESS:
            return action.bookingViewSuccess
        default:
            return state
    }
}

const booking = (state = [], action) => {
    switch (action.type) {
        case BOOKING_VIEW_SUCCESS:
            return action.booking
        default:
            return state
    }
}

const bookingAdded = (state = [], action) => {
    switch (action.type) {
        case BOOKING_ADD_SUCCESS:
            return action.booking
        default:
            return state
    }
}

const bookingAddError = (state = false, action) => {
    switch (action.type) {
        case BOOKING_ADD_ERROR:
            return action.bookingAddError
        default:
            return state
    }
}

const bookingAddLoading = (state = false, action) => {
    switch (action.type) {
        case BOOKING_ADD_LOADING:
            return action.bookingAddLoading
        default:
            return state
    }
}

const bookingAddSuccess = (state = false, action) => {
    switch (action.type) {
        case BOOKING_ADD_SUCCESS:
            return action.bookingAddSuccess
        default:
            return state
    }
}


// Booking has DO
const bookingHasDOViewError = (state = false, action) => {
    switch (action.type) {
        case BOOKING_HAS_DO_VIEW_ERROR:
            return action.bookingHasDOViewError
        default:
            return state
    }
}

const bookingHasDOViewLoading = (state = false, action) => {
    switch (action.type) {
        case BOOKING_HAS_DO_VIEW_LOADING:
            return action.bookingHasDOViewLoading
        default:
            return state
    }
}

const bookingHasDOViewSuccess = (state = false, action) => {
    switch (action.type) {
        case BOOKING_HAS_DO_VIEW_SUCCESS:
            return action.bookingHasDOViewSuccess
        default:
            return state
    }
}

const bookinghdo = (state = [], action) => {
    switch (action.type) {
        case BOOKING_HAS_DO_VIEW_SUCCESS:
            return action.bookinghdo
        default:
            return state
    }
}

const bookingHasDOUpdateError = (state = false, action) => {
    switch (action.type) {
        case BOOKING_HAS_DO_UPDATE_ERROR:
            return action.bookingHasDOUpdateError
        default:
            return state
    }
}

const bookingHasDOUpdateInvalid = (state = false, action) => {
    switch (action.type) {
        case BOOKING_HAS_DO_UPDATE_INVALID:
            return action.bookingHasDOUpdateInvalid
        default:
            return state
    }
}

const invalid = (state = [], action) => {
    switch (action.type) {
        case BOOKING_HAS_DO_UPDATE_INVALID:
            return action.invalid
        default:
            return state
    }
}

const bookingHasDOUpdateLoading = (state = false, action) => {
    switch (action.type) {
        case BOOKING_HAS_DO_UPDATE_LOADING:
            return action.bookingHasDOUpdateLoading
        default:
            return state
    }
}

const bookingHasDOUpdateSuccess = (state = false, action) => {
    switch (action.type) {
        case BOOKING_HAS_DO_UPDATE_SUCCESS:
            return action.bookingHasDOUpdateSuccess
        default:
            return state
    }
}

const bookingReducer = combineReducers({
    bookingGetError,
    bookingGetLoading,
    bookingGetSuccess,
    bookings,

    bookingViewLoading,
    bookingViewError,
    bookingViewSuccess,
    booking,

    bookingAddError,
    bookingAddLoading,
    bookingAddSuccess,
    bookingAdded,

    bookingHasDOViewError,
    bookingHasDOViewLoading,
    bookingHasDOViewSuccess,
    bookinghdo,

    bookingHasDOUpdateError,
    bookingHasDOUpdateLoading,
    bookingHasDOUpdateInvalid,
    invalid,
    bookingHasDOUpdateSuccess,
})

export default bookingReducer