import {
    combineReducers
} from 'redux'
import {
    SUPPLIER_GET_LOADING,
    SUPPLIER_GET_ERROR,
    SUPPLIER_GET_SUCCESS,

    SUPPLIER_VIEW_ERROR,
    SUPPLIER_VIEW_LOADING,
    SUPPLIER_VIEW_SUCCESS,

    SUPPLIER_ADD_ERROR,
    SUPPLIER_ADD_LOADING,
    SUPPLIER_ADD_INVALID,
    SUPPLIER_ADD_SUCCESS,

    SUPPLIER_DELETE_ERROR,
    SUPPLIER_DELETE_LOADING,
    SUPPLIER_DELETE_SUCCESS
} from '../actions/constant'

const supplierGetError = (state = false, action) => {
    switch (action.type) {
        case SUPPLIER_GET_ERROR:
            return action.supplierGetError
        default:
            return state
    }
}

const supplierGetLoading = (state = false, action) => {
    switch (action.type) {
        case SUPPLIER_GET_LOADING:
            return action.supplierGetLoading
        default:
            return state
    }
}

const supplierGetSuccess = (state = false, action) => {
    switch (action.type) {
        case SUPPLIER_GET_SUCCESS:
            return action.supplierGetSuccess
        default:
            return state
    }
}

const suppliers = (state = [], action) => {
    switch (action.type) {
        case SUPPLIER_GET_SUCCESS:
            return action.suppliers
        default:
            return state
    }
}

const supplierViewLoading = (state = false, action) => {
    switch (action.type) {
        case SUPPLIER_VIEW_LOADING:
            return action.supplierViewLoading
        default:
            return state
    }
}

const supplierViewError = (state = false, action) => {
    switch (action.type) {
        case SUPPLIER_VIEW_ERROR:
            return action.supplierViewError
        default:
            return state
    }
}

const supplierViewSuccess = (state = false, action) => {
    switch (action.type) {
        case SUPPLIER_VIEW_SUCCESS:
            return action.supplierViewSuccess
        default:
            return state
    }
}

const supplier = (state = [], action) => {
    switch (action.type) {
        case SUPPLIER_VIEW_SUCCESS:
            return action.supplier
        default:
            return state
    }
}

const supplierAddError = (state = false, action) => {
    switch (action.type) {
        case SUPPLIER_ADD_ERROR:
            return action.supplierAddError
        default:
            return state
    }
}

const supplierAddInvalid = (state = false, action) => {
    switch (action.type) {
        case SUPPLIER_ADD_INVALID:
            return action.supplierAddInvalid
        default:
            return state
    }
}

const invalid = (state = [], action) => {
    switch (action.type) {
        case SUPPLIER_ADD_INVALID:
            return action.invalid
        default:
            return state
    }
}

const supplierAddLoading = (state = false, action) => {
    switch (action.type) {
        case SUPPLIER_ADD_LOADING:
            return action.supplierAddLoading
        default:
            return state
    }
}

const supplierAddSuccess = (state = false, action) => {
    switch (action.type) {
        case SUPPLIER_ADD_SUCCESS:
            return action.supplierAddSuccess
        default:
            return state
    }
}

const supplierDeleteError = (state = false, action) => {
    switch (action.type) {
        case SUPPLIER_DELETE_ERROR:
            return action.supplierDeleteError
        default:
            return state
    }
}

const supplierDeleteLoading = (state = false, action) => {
    switch (action.type) {
        case SUPPLIER_DELETE_LOADING:
            return action.supplierDeleteLoading
        default:
            return state
    }
}

const supplierDeleteSuccess = (state = false, action) => {
    switch (action.type) {
        case SUPPLIER_DELETE_SUCCESS:
            return action.supplierDeleteSuccess
        default:
            return state
    }
}

const supplierReducer = combineReducers({
    supplierGetError,
    supplierGetLoading,
    supplierGetSuccess,
    suppliers,

    supplierViewLoading,
    supplierViewError,
    supplierViewSuccess,
    supplier,

    supplierAddError,
    supplierAddLoading,
    supplierAddInvalid,
    supplierAddSuccess,
    invalid,

    supplierDeleteError,
    supplierDeleteLoading,
    supplierDeleteSuccess
})

export default supplierReducer