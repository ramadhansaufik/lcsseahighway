import {
    combineReducers
} from 'redux'
import {
    JENISCOMMODITY_GET_LOADING,
    JENISCOMMODITY_GET_ERROR,
    JENISCOMMODITY_GET_SUCCESS,

    JENISCOMMODITY_ADD_LOADING,
    JENISCOMMODITY_ADD_ERROR,
    JENISCOMMODITY_ADD_SUCCESS,

    JENISCOMMODITY_DELETE_LOADING,
    JENISCOMMODITY_DELETE_ERROR,
    JENISCOMMODITY_DELETE_SUCCESS,

    JENISCOMMODITY_VIEW_LOADING,
    JENISCOMMODITY_VIEW_ERROR,
    JENISCOMMODITY_VIEW_SUCCESS,
} from '../actions/constant'

const jeniscommodityGetError = (state = false, action) => {
    switch (action.type) {
        case JENISCOMMODITY_GET_ERROR:
            return action.jeniscommodityGetError
        default:
            return state
    }
}

const jeniscommodityGetLoading = (state = false, action) => {
    switch (action.type) {
        case JENISCOMMODITY_GET_LOADING:
            return action.jeniscommodityGetLoading
        default:
            return state
    }
}

const jeniscommodityGetSuccess = (state = false, action) => {
    switch (action.type) {
        case JENISCOMMODITY_GET_SUCCESS:
            return action.jeniscommodityGetSuccess
        default:
            return state
    }
}

const jeniscommodities = (state = [], action) => {
    switch (action.type) {
        case JENISCOMMODITY_GET_SUCCESS:
            return action.jeniscommodities
        default:
            return state
    }
}

const jeniscommodityViewError = (state = false, action) => {
    switch (action.type) {
        case JENISCOMMODITY_VIEW_ERROR:
            return action.jeniscommodityViewError
        default:
            return state
    }
}

const jeniscommodityViewLoading = (state = false, action) => {
    switch (action.type) {
        case JENISCOMMODITY_VIEW_LOADING:
            return action.jeniscommodityViewLoading
        default:
            return state
    }
}

const jeniscommodityViewSuccess = (state = false, action) => {
    switch (action.type) {
        case JENISCOMMODITY_VIEW_SUCCESS:
            return action.jeniscommodityViewSuccess
        default:
            return state
    }
}

const jeniscommodity = (state = [], action) => {
    switch (action.type) {
        case JENISCOMMODITY_VIEW_SUCCESS:
            return action.jeniscommodity
        default:
            return state
    }
}

const jeniscommodityAddError = (state = false, action) => {
    switch (action.type) {
        case JENISCOMMODITY_ADD_ERROR:
            return action.jeniscommodityAddError
        default:
            return state
    }
}

const jeniscommodityAddLoading = (state = false, action) => {
    switch (action.type) {
        case JENISCOMMODITY_ADD_LOADING:
            return action.jeniscommodityAddLoading
        default:
            return state
    }
}

const jeniscommodityAddSuccess = (state = false, action) => {
    switch (action.type) {
        case JENISCOMMODITY_ADD_SUCCESS:
            return action.jeniscommodityAddSuccess
        default:
            return state
    }
}

const jeniscommodityDeleteError = (state = false, action) => {
    switch (action.type) {
        case JENISCOMMODITY_DELETE_ERROR:
            return action.jeniscommodityDeleteError
        default:
            return state
    }
}

const jeniscommodityDeleteLoading = (state = false, action) => {
    switch (action.type) {
        case JENISCOMMODITY_DELETE_LOADING:
            return action.jeniscommodityDeleteLoading
        default:
            return state
    }
}

const jeniscommodityDeleteSuccess = (state = false, action) => {
    switch (action.type) {
        case JENISCOMMODITY_DELETE_SUCCESS:
            return action.jeniscommodityDeleteSuccess
        default:
            return state
    }
}

const jeniscommodityReducer = combineReducers({
    jeniscommodityGetError,
    jeniscommodityGetLoading,
    jeniscommodityGetSuccess,
    jeniscommodities,

    jeniscommodityViewError,
    jeniscommodityViewLoading,
    jeniscommodityViewSuccess,
    jeniscommodity,

    jeniscommodityAddError,
    jeniscommodityAddLoading,
    jeniscommodityAddSuccess,

    jeniscommodityDeleteError,
    jeniscommodityDeleteLoading,
    jeniscommodityDeleteSuccess,
})

export default jeniscommodityReducer