import {
    combineReducers
} from 'redux'
import {
    KOTA_GET_LOADING,
    KOTA_GET_ERROR,
    KOTA_GET_SUCCESS,

    KOTA_VIEW_ERROR,
    KOTA_VIEW_LOADING,
    KOTA_VIEW_SUCCESS,

    KOTA_ADD_ERROR,
    KOTA_ADD_LOADING,
    KOTA_ADD_SUCCESS
} from '../actions/constant'

const kotaGetError = (state = false, action) => {
    switch (action.type) {
        case KOTA_GET_ERROR:
            return action.kotaGetError
        default:
            return state
    }
}

const kotaGetLoading = (state = false, action) => {
    switch (action.type) {
        case KOTA_GET_LOADING:
            return action.kotaGetLoading
        default:
            return state
    }
}

const kotaGetSuccess = (state = false, action) => {
    switch (action.type) {
        case KOTA_GET_SUCCESS:
            return action.kotaGetSuccess
        default:
            return state
    }
}

const kotas = (state = [], action) => {
    switch (action.type) {
        case KOTA_GET_SUCCESS:
            return action.kotas
        default:
            return state
    }
}

const kotaViewLoading = (state = false, action) => {
    switch (action.type) {
        case KOTA_VIEW_LOADING:
            return action.kotaViewLoading
        default:
            return state
    }
}

const kotaViewError = (state = false, action) => {
    switch (action.type) {
        case KOTA_VIEW_ERROR:
            return action.kotaViewError
        default:
            return state
    }
}

const kotaViewSuccess = (state = false, action) => {
    switch (action.type) {
        case KOTA_VIEW_SUCCESS:
            return action.kotaViewSuccess
        default:
            return state
    }
}

const kota = (state = [], action) => {
    switch (action.type) {
        case KOTA_VIEW_SUCCESS:
            return action.kota
        default:
            return state
    }
}

const kotaAddError = (state = false, action) => {
    switch (action.type) {
        case KOTA_ADD_ERROR:
            return action.kotaAddError
        default:
            return state
    }
}

const kotaAddLoading = (state = false, action) => {
    switch (action.type) {
        case KOTA_ADD_LOADING:
            return action.kotaAddLoading
        default:
            return state
    }
}

const kotaAddSuccess = (state = false, action) => {
    switch (action.type) {
        case KOTA_ADD_SUCCESS:
            return action.kotaAddSuccess
        default:
            return state
    }
}

const kotaReducer = combineReducers({
    kotaGetError,
    kotaGetLoading,
    kotaGetSuccess,
    kotas,

    kotaViewLoading,
    kotaViewError,
    kotaViewSuccess,
    kota,

    kotaAddError,
    kotaAddLoading,
    kotaAddSuccess
})

export default kotaReducer