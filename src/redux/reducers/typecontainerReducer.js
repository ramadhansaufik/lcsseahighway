import {
    combineReducers
} from 'redux'
import {
    TYPECONTAINER_GET_LOADING,
    TYPECONTAINER_GET_ERROR,
    TYPECONTAINER_GET_SUCCESS,

    TYPECONTAINER_VIEW_ERROR,
    TYPECONTAINER_VIEW_LOADING,
    TYPECONTAINER_VIEW_SUCCESS,

    TYPECONTAINER_ADD_ERROR,
    TYPECONTAINER_ADD_LOADING,
    TYPECONTAINER_ADD_SUCCESS,

    TYPECONTAINER_DELETE_ERROR,
    TYPECONTAINER_DELETE_LOADING,
    TYPECONTAINER_DELETE_SUCCESS,

    TYPECONTAINER_PRICE_UPDATE_ERROR,
    TYPECONTAINER_PRICE_UPDATE_LOADING,
    TYPECONTAINER_PRICE_UPDATE_SUCCESS
} from '../actions/constant'

const typecontainerGetError = (state = false, action) => {
    switch (action.type) {
        case TYPECONTAINER_GET_ERROR:
            return action.typecontainerGetError
        default:
            return state
    }
}

const typecontainerGetLoading = (state = false, action) => {
    switch (action.type) {
        case TYPECONTAINER_GET_LOADING:
            return action.typecontainerGetLoading
        default:
            return state
    }
}

const typecontainerGetSuccess = (state = false, action) => {
    switch (action.type) {
        case TYPECONTAINER_GET_SUCCESS:
            return action.typecontainerGetSuccess
        default:
            return state
    }
}

const typecontainers = (state = [], action) => {
    switch (action.type) {
        case TYPECONTAINER_GET_SUCCESS:
            return action.typecontainers
        default:
            return state
    }
}

const typecontainerViewLoading = (state = false, action) => {
    switch (action.type) {
        case TYPECONTAINER_VIEW_LOADING:
            return action.typecontainerViewLoading
        default:
            return state
    }
}

const typecontainerViewError = (state = false, action) => {
    switch (action.type) {
        case TYPECONTAINER_VIEW_ERROR:
            return action.typecontainerViewError
        default:
            return state
    }
}

const typecontainerViewSuccess = (state = false, action) => {
    switch (action.type) {
        case TYPECONTAINER_VIEW_SUCCESS:
            return action.typecontainerViewSuccess
        default:
            return state
    }
}

const typecontainer = (state = [], action) => {
    switch (action.type) {
        case TYPECONTAINER_VIEW_SUCCESS:
            return action.typecontainer
        default:
            return state
    }
}

const typecontainerAddError = (state = false, action) => {
    switch (action.type) {
        case TYPECONTAINER_ADD_ERROR:
            return action.typecontainerAddError
        default:
            return state
    }
}

const typecontainerAddLoading = (state = false, action) => {
    switch (action.type) {
        case TYPECONTAINER_ADD_LOADING:
            return action.typecontainerAddLoading
        default:
            return state
    }
}

const typecontainerAddSuccess = (state = false, action) => {
    switch (action.type) {
        case TYPECONTAINER_ADD_SUCCESS:
            return action.typecontainerAddSuccess
        default:
            return state
    }
}

const typecontainerDeleteError = (state = false, action) => {
    switch (action.type) {
        case TYPECONTAINER_DELETE_ERROR:
            return action.typecontainerDeleteError
        default:
            return state
    }
}

const typecontainerDeleteLoading = (state = false, action) => {
    switch (action.type) {
        case TYPECONTAINER_DELETE_LOADING:
            return action.typecontainerDeleteLoading
        default:
            return state
    }
}

const typecontainerDeleteSuccess = (state = false, action) => {
    switch (action.type) {
        case TYPECONTAINER_DELETE_SUCCESS:
            return action.typecontainerDeleteSuccess
        default:
            return state
    }
}

const typecontainerPriceUpdateError = (state = false, action) => {
    switch (action.type) {
        case TYPECONTAINER_PRICE_UPDATE_ERROR:
            return action.typecontainerPriceUpdateError
        default:
            return state
    }
}

const typecontainerPriceUpdateLoading = (state = false, action) => {
    switch (action.type) {
        case TYPECONTAINER_PRICE_UPDATE_LOADING:
            return action.typecontainerPriceUpdateLoading
        default:
            return state
    }
}

const typecontainerPriceUpdateSuccess = (state = false, action) => {
    switch (action.type) {
        case TYPECONTAINER_PRICE_UPDATE_SUCCESS:
            return action.typecontainerPriceUpdateSuccess
        default:
            return state
    }
}

const typecontainerReducer = combineReducers({
    typecontainerGetError,
    typecontainerGetLoading,
    typecontainerGetSuccess,
    typecontainers,

    typecontainerViewLoading,
    typecontainerViewError,
    typecontainerViewSuccess,
    typecontainer,

    typecontainerAddError,
    typecontainerAddLoading,
    typecontainerAddSuccess,

    typecontainerDeleteError,
    typecontainerDeleteLoading,
    typecontainerDeleteSuccess,

    typecontainerPriceUpdateError,
    typecontainerPriceUpdateLoading,
    typecontainerPriceUpdateSuccess
})

export default typecontainerReducer