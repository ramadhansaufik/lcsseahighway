import {
    combineReducers
} from 'redux'
import {
    PROVINSI_GET_LOADING,
    PROVINSI_GET_ERROR,
    PROVINSI_GET_SUCCESS,

    PROVINSI_VIEW_ERROR,
    PROVINSI_VIEW_LOADING,
    PROVINSI_VIEW_SUCCESS,

    PROVINSI_ADD_ERROR,
    PROVINSI_ADD_LOADING,
    PROVINSI_ADD_SUCCESS
} from '../actions/constant'

const provinsiGetError = (state = false, action) => {
    switch (action.type) {
        case PROVINSI_GET_ERROR:
            return action.provinsiGetError
        default:
            return state
    }
}

const provinsiGetLoading = (state = false, action) => {
    switch (action.type) {
        case PROVINSI_GET_LOADING:
            return action.provinsiGetLoading
        default:
            return state
    }
}

const provinsiGetSuccess = (state = false, action) => {
    switch (action.type) {
        case PROVINSI_GET_SUCCESS:
            return action.provinsiGetSuccess
        default:
            return state
    }
}

const provinsis = (state = [], action) => {
    switch (action.type) {
        case PROVINSI_GET_SUCCESS:
            return action.provinsis
        default:
            return state
    }
}

const provinsiViewLoading = (state = false, action) => {
    switch (action.type) {
        case PROVINSI_VIEW_LOADING:
            return action.provinsiViewLoading
        default:
            return state
    }
}

const provinsiViewError = (state = false, action) => {
    switch (action.type) {
        case PROVINSI_VIEW_ERROR:
            return action.provinsiViewError
        default:
            return state
    }
}

const provinsiViewSuccess = (state = false, action) => {
    switch (action.type) {
        case PROVINSI_VIEW_SUCCESS:
            return action.provinsiViewSuccess
        default:
            return state
    }
}

const provinsi = (state = [], action) => {
    switch (action.type) {
        case PROVINSI_VIEW_SUCCESS:
            return action.provinsi
        default:
            return state
    }
}

const provinsiAddError = (state = false, action) => {
    switch (action.type) {
        case PROVINSI_ADD_ERROR:
            return action.provinsiAddError
        default:
            return state
    }
}

const provinsiAddLoading = (state = false, action) => {
    switch (action.type) {
        case PROVINSI_ADD_LOADING:
            return action.provinsiAddLoading
        default:
            return state
    }
}

const provinsiAddSuccess = (state = false, action) => {
    switch (action.type) {
        case PROVINSI_ADD_SUCCESS:
            return action.provinsiAddSuccess
        default:
            return state
    }
}

const provinsiReducer = combineReducers({
    provinsiGetError,
    provinsiGetLoading,
    provinsiGetSuccess,
    provinsis,

    provinsiViewLoading,
    provinsiViewError,
    provinsiViewSuccess,
    provinsi,

    provinsiAddError,
    provinsiAddLoading,
    provinsiAddSuccess
})

export default provinsiReducer