import {
    combineReducers
} from 'redux'
import {
    DETAILCONTAINER_GET_LOADING,
    DETAILCONTAINER_GET_ERROR,
    DETAILCONTAINER_GET_SUCCESS,

    DETAILCONTAINER_VIEW_ERROR,
    DETAILCONTAINER_VIEW_LOADING,
    DETAILCONTAINER_VIEW_SUCCESS,

    DETAILCONTAINER_ADD_ERROR,
    DETAILCONTAINER_ADD_LOADING,
    DETAILCONTAINER_ADD_SUCCESS,

    DETAILCONTAINER_DELETE_ERROR,
    DETAILCONTAINER_DELETE_LOADING,
    DETAILCONTAINER_DELETE_SUCCESS
} from '../actions/constant'

const detailcontainerGetError = (state = false, action) => {
    switch (action.type) {
        case DETAILCONTAINER_GET_ERROR:
            return action.detailcontainerGetError
        default:
            return state
    }
}

const detailcontainerGetLoading = (state = false, action) => {
    switch (action.type) {
        case DETAILCONTAINER_GET_LOADING:
            return action.detailcontainerGetLoading
        default:
            return state
    }
}

const detailcontainerGetSuccess = (state = false, action) => {
    switch (action.type) {
        case DETAILCONTAINER_GET_SUCCESS:
            return action.detailcontainerGetSuccess
        default:
            return state
    }
}

const detailcontainers = (state = [], action) => {
    switch (action.type) {
        case DETAILCONTAINER_GET_SUCCESS:
            return action.detailcontainers
        default:
            return state
    }
}

const detailcontainerViewLoading = (state = false, action) => {
    switch (action.type) {
        case DETAILCONTAINER_VIEW_LOADING:
            return action.detailcontainerViewLoading
        default:
            return state
    }
}

const detailcontainerViewError = (state = false, action) => {
    switch (action.type) {
        case DETAILCONTAINER_VIEW_ERROR:
            return action.detailcontainerViewError
        default:
            return state
    }
}

const detailcontainerViewSuccess = (state = false, action) => {
    switch (action.type) {
        case DETAILCONTAINER_VIEW_SUCCESS:
            return action.detailcontainerViewSuccess
        default:
            return state
    }
}

const detailcontainer = (state = [], action) => {
    switch (action.type) {
        case DETAILCONTAINER_VIEW_SUCCESS:
            return action.detailcontainer
        case DETAILCONTAINER_ADD_SUCCESS:
            return action.detailcontainer
        default:
            return state
    }
}

const detailcontainerAddError = (state = false, action) => {
    switch (action.type) {
        case DETAILCONTAINER_ADD_ERROR:
            return action.detailcontainerAddError
        default:
            return state
    }
}

const detailcontainerAddLoading = (state = false, action) => {
    switch (action.type) {
        case DETAILCONTAINER_ADD_LOADING:
            return action.detailcontainerAddLoading
        default:
            return state
    }
}

const detailcontainerAddSuccess = (state = false, action) => {
    switch (action.type) {
        case DETAILCONTAINER_ADD_SUCCESS:
            return action.detailcontainerAddSuccess
        default:
            return state
    }
}

const detailcontainerDeleteError = (state = false, action) => {
    switch (action.type) {
        case DETAILCONTAINER_DELETE_ERROR:
            return action.detailcontainerDeleteError
        default:
            return state
    }
}

const detailcontainerDeleteSuccess = (state = false, action) => {
    switch (action.type) {
        case DETAILCONTAINER_DELETE_SUCCESS:
            return action.detailcontainerDeleteSuccess
        default:
            return state
    }
}

const detailcontainerDeleteLoading = (state = false, action) => {
    switch (action.type) {
        case DETAILCONTAINER_DELETE_LOADING:
            return action.detailcontainerDeleteLoading
        default:
            return state
    }
}

const detailcontainerReducer = combineReducers({
    detailcontainerGetError,
    detailcontainerGetLoading,
    detailcontainerGetSuccess,
    detailcontainers,

    detailcontainerViewLoading,
    detailcontainerViewError,
    detailcontainerViewSuccess,
    detailcontainer,

    detailcontainerAddError,
    detailcontainerAddLoading,
    detailcontainerAddSuccess,

    detailcontainerDeleteError,
    detailcontainerDeleteLoading,
    detailcontainerDeleteSuccess
})

export default detailcontainerReducer