import {
    combineReducers
} from 'redux'
import {
    VESSEL_GET_LOADING,
    VESSEL_GET_ERROR,
    VESSEL_GET_SUCCESS,

    VESSEL_ADD_LOADING,
    VESSEL_ADD_ERROR,
    VESSEL_ADD_INVALID,
    VESSEL_ADD_SUCCESS,

    VESSEL_DELETE_LOADING,
    VESSEL_DELETE_ERROR,
    VESSEL_DELETE_SUCCESS,

    VESSEL_VIEW_LOADING,
    VESSEL_VIEW_ERROR,
    VESSEL_VIEW_SUCCESS,
} from '../actions/constant'

const vesselGetError = (state = false, action) => {
    switch (action.type) {
        case VESSEL_GET_ERROR:
            return action.vesselGetError
        default:
            return state
    }
}

const vesselGetLoading = (state = false, action) => {
    switch (action.type) {
        case VESSEL_GET_LOADING:
            return action.vesselGetLoading
        default:
            return state
    }
}

const vesselGetSuccess = (state = false, action) => {
    switch (action.type) {
        case VESSEL_GET_SUCCESS:
            return action.vesselGetSuccess
        default:
            return state
    }
}

const vessels = (state = [], action) => {
    switch (action.type) {
        case VESSEL_GET_SUCCESS:
            return action.vessels
        default:
            return state
    }
}

const vesselViewError = (state = false, action) => {
    switch (action.type) {
        case VESSEL_VIEW_ERROR:
            return action.vesselViewError
        default:
            return state
    }
}

const vesselViewLoading = (state = false, action) => {
    switch (action.type) {
        case VESSEL_VIEW_LOADING:
            return action.vesselViewLoading
        default:
            return state
    }
}

const vesselViewSuccess = (state = false, action) => {
    switch (action.type) {
        case VESSEL_VIEW_SUCCESS:
            return action.vesselViewSuccess
        default:
            return state
    }
}

const vessel = (state = [], action) => {
    switch (action.type) {
        case VESSEL_VIEW_SUCCESS:
            return action.vessel
        default:
            return state
    }
}

const vesselAddError = (state = false, action) => {
    switch (action.type) {
        case VESSEL_ADD_ERROR:
            return action.vesselAddError
        default:
            return state
    }
}

const vesselAddLoading = (state = false, action) => {
    switch (action.type) {
        case VESSEL_ADD_LOADING:
            return action.vesselAddLoading
        default:
            return state
    }
}

const vesselAddInvalid = (state = false, action) => {
    switch (action.type) {
        case VESSEL_ADD_INVALID:
            return action.vesselAddInvalid
        default:
            return state
    }
}

const invalid = (state = [], action) => {
    switch (action.type) {
        case VESSEL_ADD_INVALID:
            return action.invalid
        default:
            return state
    }
}

const vesselAddSuccess = (state = false, action) => {
    switch (action.type) {
        case VESSEL_ADD_SUCCESS:
            return action.vesselAddSuccess
        default:
            return state
    }
}

const vesselDeleteError = (state = false, action) => {
    switch (action.type) {
        case VESSEL_DELETE_ERROR:
            return action.vesselDeleteError
        default:
            return state
    }
}

const vesselDeleteLoading = (state = false, action) => {
    switch (action.type) {
        case VESSEL_DELETE_LOADING:
            return action.vesselDeleteLoading
        default:
            return state
    }
}

const vesselDeleteSuccess = (state = false, action) => {
    switch (action.type) {
        case VESSEL_DELETE_SUCCESS:
            return action.vesselDeleteSuccess
        default:
            return state
    }
}

const vesselReducer = combineReducers({
    vesselGetError,
    vesselGetLoading,
    vesselGetSuccess,
    vessels,

    vesselViewError,
    vesselViewLoading,
    vesselViewSuccess,
    vessel,

    vesselAddError,
    vesselAddLoading,
    vesselAddInvalid,
    vesselAddSuccess,
    invalid,

    vesselDeleteError,
    vesselDeleteLoading,
    vesselDeleteSuccess,
})

export default vesselReducer