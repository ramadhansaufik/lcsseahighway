import {
    combineReducers
} from 'redux'
import {
    CONTAINER_GET_LOADING,
    CONTAINER_GET_ERROR,
    CONTAINER_GET_SUCCESS,

    CONTAINER_VIEW_ERROR,
    CONTAINER_VIEW_LOADING,
    CONTAINER_VIEW_SUCCESS,

    CONTAINER_ADD_ERROR,
    CONTAINER_ADD_LOADING,
    CONTAINER_ADD_SUCCESS
} from '../actions/constant'

const containerGetError = (state = false, action) => {
    switch (action.type) {
        case CONTAINER_GET_ERROR:
            return action.containerGetError
        default:
            return state
    }
}

const containerGetLoading = (state = false, action) => {
    switch (action.type) {
        case CONTAINER_GET_LOADING:
            return action.containerGetLoading
        default:
            return state
    }
}

const containerGetSuccess = (state = false, action) => {
    switch (action.type) {
        case CONTAINER_GET_SUCCESS:
            return action.containerGetSuccess
        default:
            return state
    }
}

const containers = (state = [], action) => {
    switch (action.type) {
        case CONTAINER_GET_SUCCESS:
            return action.containers
        default:
            return state
    }
}

const containerViewLoading = (state = false, action) => {
    switch (action.type) {
        case CONTAINER_VIEW_LOADING:
            return action.containerViewLoading
        default:
            return state
    }
}

const containerViewError = (state = false, action) => {
    switch (action.type) {
        case CONTAINER_VIEW_ERROR:
            return action.containerViewError
        default:
            return state
    }
}

const containerViewSuccess = (state = false, action) => {
    switch (action.type) {
        case CONTAINER_VIEW_SUCCESS:
            return action.containerViewSuccess
        default:
            return state
    }
}

const container = (state = [], action) => {
    switch (action.type) {
        case CONTAINER_VIEW_SUCCESS:
            return action.container
        case CONTAINER_ADD_SUCCESS:
            return action.container
        default:
            return state
    }
}

const containerAddError = (state = false, action) => {
    switch (action.type) {
        case CONTAINER_ADD_ERROR:
            return action.containerAddError
        default:
            return state
    }
}

const containerAddLoading = (state = false, action) => {
    switch (action.type) {
        case CONTAINER_ADD_LOADING:
            return action.containerAddLoading
        default:
            return state
    }
}

const containerAddSuccess = (state = false, action) => {
    switch (action.type) {
        case CONTAINER_ADD_SUCCESS:
            return action.containerAddSuccess
        default:
            return state
    }
}

const containerReducer = combineReducers({
    containerGetError,
    containerGetLoading,
    containerGetSuccess,
    containers,

    containerViewLoading,
    containerViewError,
    containerViewSuccess,
    container,

    containerAddError,
    containerAddLoading,
    containerAddSuccess
})

export default containerReducer