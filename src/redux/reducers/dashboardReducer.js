import {
    combineReducers
} from 'redux'
import {
    CONSIGNEE_STTC_ALL_ERROR,
    CONSIGNEE_STTC_ALL_LOADING,
    CONSIGNEE_STTC_ALL_SUCCESS,

    CONSIGNEE_STTC_ACTIVE_ERROR,
    CONSIGNEE_STTC_ACTIVE_LOADING,
    CONSIGNEE_STTC_ACTIVE_SUCCESS,

    SUPPLIER_STTC_ALL_ERROR,
    SUPPLIER_STTC_ALL_LOADING,
    SUPPLIER_STTC_ALL_SUCCESS,

    SUPPLIER_STTC_ACTIVE_ERROR,
    SUPPLIER_STTC_ACTIVE_LOADING,
    SUPPLIER_STTC_ACTIVE_SUCCESS,

    SHIPPER_STTC_ALL_ERROR,
    SHIPPER_STTC_ALL_LOADING,
    SHIPPER_STTC_ALL_SUCCESS,

    SHIPPER_STTC_ACTIVE_ERROR,
    SHIPPER_STTC_ACTIVE_LOADING,
    SHIPPER_STTC_ACTIVE_SUCCESS,

    RESELLER_STTC_ALL_ERROR,
    RESELLER_STTC_ALL_LOADING,
    RESELLER_STTC_ALL_SUCCESS,

    RESELLER_STTC_ACTIVE_ERROR,
    RESELLER_STTC_ACTIVE_LOADING,
    RESELLER_STTC_ACTIVE_SUCCESS,

    OPERATOR_STTC_ALL_ERROR,
    OPERATOR_STTC_ALL_LOADING,
    OPERATOR_STTC_ALL_SUCCESS,

    OPERATOR_STTC_ACTIVE_ERROR,
    OPERATOR_STTC_ACTIVE_LOADING,
    OPERATOR_STTC_ACTIVE_SUCCESS,

    REGULATOR_STTC_ALL_ERROR,
    REGULATOR_STTC_ALL_LOADING,
    REGULATOR_STTC_ALL_SUCCESS,

    REGULATOR_STTC_ACTIVE_ERROR,
    REGULATOR_STTC_ACTIVE_LOADING,
    REGULATOR_STTC_ACTIVE_SUCCESS,

    VESSEL_TRACKING_GET_ERROR,
    VESSEL_TRACKING_GET_LOADING,
    VESSEL_TRACKING_GET_SUCCESS,

    VESSEL_TRACKING_VIEW_ERROR,
    VESSEL_TRACKING_VIEW_LOADING,
    VESSEL_TRACKING_VIEW_SUCCESS,

    OPERATOR_NAME_ERROR,
    OPERATOR_NAME_LOADING,
    OPERATOR_NAME_SUCCESS,

    ORDER_PER_JENIS_PRIORITAS_GET_LOADING,
    ORDER_PER_JENIS_PRIORITAS_GET_ERROR,
    ORDER_PER_JENIS_PRIORITAS_GET_SUCCESS,

    MUATAN_TERBANYAK_GET_LOADING,
    MUATAN_TERBANYAK_GET_ERROR,
    MUATAN_TERBANYAK_GET_SUCCESS,

    CONTAINER_PER_TAHUN_GET_LOADING,
    CONTAINER_PER_TAHUN_GET_ERROR,
    CONTAINER_PER_TAHUN_GET_SUCCESS,

    MUATAN_PER_WILAYAH_GET_LOADING,
    MUATAN_PER_WILAYAH_GET_ERROR,
    MUATAN_PER_WILAYAH_GET_SUCCESS,

    ORDER_PAID_GET_LOADING,
    ORDER_PAID_GET_ERROR,
    ORDER_PAID_GET_SUCCESS,

    ORDER_UNPAID_GET_LOADING,
    ORDER_UNPAID_GET_ERROR,
    ORDER_UNPAID_GET_SUCCESS,

    ORDER_DENIED_GET_LOADING,
    ORDER_DENIED_GET_ERROR,
    ORDER_DENIED_GET_SUCCESS,

    ALL_ORDER_GET_LOADING,
    ALL_ORDER_GET_ERROR,
    ALL_ORDER_GET_SUCCESS,

    ORDER_PER_BULAN_GET_LOADING,
    ORDER_PER_BULAN_GET_ERROR,
    ORDER_PER_BULAN_GET_SUCCESS,

    MUATAN_PER_OPERATOR_GET_LOADING,
    MUATAN_PER_OPERATOR_GET_ERROR,
    MUATAN_PER_OPERATOR_GET_SUCCESS,

    PO_GET_LOADING,
    PO_GET_ERROR,
    PO_GET_SUCCESS,

    DO_GET_LOADING,
    DO_GET_ERROR,
    DO_GET_SUCCESS,

    SISA_QUOTA_TRAYEK_GET_LOADING,
    SISA_QUOTA_TRAYEK_GET_ERROR,
    SISA_QUOTA_TRAYEK_GET_SUCCESS,

    VOYAGE_PER_TRAYEK_GET_LOADING,
    VOYAGE_PER_TRAYEK_GET_ERROR,
    VOYAGE_PER_TRAYEK_GET_SUCCESS,

    DISPARITAS_HARGA_GET_LOADING,
    DISPARITAS_HARGA_GET_ERROR,
    DISPARITAS_HARGA_GET_SUCCESS,

    REALISASI_GET_LOADING,
    REALISASI_GET_ERROR,
    REALISASI_GET_SUCCESS,

    WAKTU_TEMPUH_GET_LOADING,
    WAKTU_TEMPUH_GET_ERROR,
    WAKTU_TEMPUH_GET_SUCCESS,

    JADWAL_KAPAL_GET_LOADING,
    JADWAL_KAPAL_GET_ERROR,
    JADWAL_KAPAL_GET_SUCCESS,

    BOOKING_VESSEL_GET_LOADING,
    BOOKING_VESSEL_GET_ERROR,
    BOOKING_VESSEL_GET_SUCCESS,

    DISTRIBUSI_BARANG_GET_LOADING,
    DISTRIBUSI_BARANG_GET_ERROR,
    DISTRIBUSI_BARANG_GET_SUCCESS,
} from '../actions/constant'

const consigneeSttcAllError = (state = false, action) => {
    switch (action.type) {
        case CONSIGNEE_STTC_ALL_ERROR:
            return action.consigneeSttcAllError
        default:
            return state
    }
}

const consigneeSttcAllLoading = (state = false, action) => {
    switch (action.type) {
        case CONSIGNEE_STTC_ALL_LOADING:
            return action.consigneeSttcAllLoading
        default:
            return state
    }
}

const consigneeSttcAllSuccess = (state = false, action) => {
    switch (action.type) {
        case CONSIGNEE_STTC_ALL_SUCCESS:
            return action.consigneeSttcAllSuccess
        default:
            return state
    }
}

const consigneeAll = (state = [], action) => {
    switch (action.type) {
        case CONSIGNEE_STTC_ALL_SUCCESS:
            return action.consigneeAll
        default:
            return state
    }
}

const consigneeSttcActiveError = (state = false, action) => {
    switch (action.type) {
        case CONSIGNEE_STTC_ACTIVE_ERROR:
            return action.consigneeSttcActiveError
        default:
            return state
    }
}

const consigneeSttcActiveLoading = (state = false, action) => {
    switch (action.type) {
        case CONSIGNEE_STTC_ACTIVE_LOADING:
            return action.consigneeSttcActiveLoading
        default:
            return state
    }
}

const consigneeSttcActiveSuccess = (state = false, action) => {
    switch (action.type) {
        case CONSIGNEE_STTC_ACTIVE_SUCCESS:
            return action.consigneeSttcActiveSuccess
        default:
            return state
    }
}

const consigneeActive = (state = [], action) => {
    switch (action.type) {
        case CONSIGNEE_STTC_ACTIVE_SUCCESS:
            return action.consigneeActive
        default:
            return state
    }
}

const supplierSttcAllError = (state = false, action) => {
    switch (action.type) {
        case SUPPLIER_STTC_ALL_ERROR:
            return action.supplierSttcAllError
        default:
            return state
    }
}

const supplierSttcAllLoading = (state = false, action) => {
    switch (action.type) {
        case SUPPLIER_STTC_ALL_LOADING:
            return action.supplierSttcAllLoading
        default:
            return state
    }
}

const supplierSttcAllSuccess = (state = false, action) => {
    switch (action.type) {
        case SUPPLIER_STTC_ALL_SUCCESS:
            return action.supplierSttcAllSuccess
        default:
            return state
    }
}

const supplierAll = (state = [], action) => {
    switch (action.type) {
        case SUPPLIER_STTC_ALL_SUCCESS:
            return action.supplierAll
        default:
            return state
    }
}

const supplierSttcActiveError = (state = false, action) => {
    switch (action.type) {
        case SUPPLIER_STTC_ACTIVE_ERROR:
            return action.supplierSttcActiveError
        default:
            return state
    }
}

const supplierSttcActiveLoading = (state = false, action) => {
    switch (action.type) {
        case SUPPLIER_STTC_ACTIVE_LOADING:
            return action.supplierSttcActiveLoading
        default:
            return state
    }
}

const supplierSttcActiveSuccess = (state = false, action) => {
    switch (action.type) {
        case SUPPLIER_STTC_ACTIVE_SUCCESS:
            return action.supplierSttcActiveSuccess
        default:
            return state
    }
}

const supplierActive = (state = [], action) => {
    switch (action.type) {
        case SUPPLIER_STTC_ACTIVE_SUCCESS:
            return action.supplierActive
        default:
            return state
    }
}

const shipperSttcAllError = (state = false, action) => {
    switch (action.type) {
        case SHIPPER_STTC_ALL_ERROR:
            return action.shipperSttcAllError
        default:
            return state
    }
}

const shipperSttcAllLoading = (state = false, action) => {
    switch (action.type) {
        case SHIPPER_STTC_ALL_LOADING:
            return action.shipperSttcAllLoading
        default:
            return state
    }
}

const shipperSttcAllSuccess = (state = false, action) => {
    switch (action.type) {
        case SHIPPER_STTC_ALL_SUCCESS:
            return action.shipperSttcAllSuccess
        default:
            return state
    }
}

const shipperAll = (state = [], action) => {
    switch (action.type) {
        case SHIPPER_STTC_ALL_SUCCESS:
            return action.shipperAll
        default:
            return state
    }
}

const shipperSttcActiveError = (state = false, action) => {
    switch (action.type) {
        case SHIPPER_STTC_ACTIVE_ERROR:
            return action.shipperSttcActiveError
        default:
            return state
    }
}

const shipperSttcActiveLoading = (state = false, action) => {
    switch (action.type) {
        case SHIPPER_STTC_ACTIVE_LOADING:
            return action.shipperSttcActiveLoading
        default:
            return state
    }
}

const shipperSttcActiveSuccess = (state = false, action) => {
    switch (action.type) {
        case SHIPPER_STTC_ACTIVE_SUCCESS:
            return action.shipperSttcActiveSuccess
        default:
            return state
    }
}

const shipperActive = (state = [], action) => {
    switch (action.type) {
        case SHIPPER_STTC_ACTIVE_SUCCESS:
            return action.shipperActive
        default:
            return state
    }
}

const resellerSttcAllError = (state = false, action) => {
    switch (action.type) {
        case RESELLER_STTC_ALL_ERROR:
            return action.resellerSttcAllError
        default:
            return state
    }
}

const resellerSttcAllLoading = (state = false, action) => {
    switch (action.type) {
        case RESELLER_STTC_ALL_LOADING:
            return action.resellerSttcAllLoading
        default:
            return state
    }
}

const resellerSttcAllSuccess = (state = false, action) => {
    switch (action.type) {
        case RESELLER_STTC_ALL_SUCCESS:
            return action.resellerSttcAllSuccess
        default:
            return state
    }
}

const resellerAll = (state = [], action) => {
    switch (action.type) {
        case RESELLER_STTC_ALL_SUCCESS:
            return action.resellerAll
        default:
            return state
    }
}

const resellerSttcActiveError = (state = false, action) => {
    switch (action.type) {
        case RESELLER_STTC_ACTIVE_ERROR:
            return action.resellerSttcActiveError
        default:
            return state
    }
}

const resellerSttcActiveLoading = (state = false, action) => {
    switch (action.type) {
        case RESELLER_STTC_ACTIVE_LOADING:
            return action.resellerSttcActiveLoading
        default:
            return state
    }
}

const resellerSttcActiveSuccess = (state = false, action) => {
    switch (action.type) {
        case RESELLER_STTC_ACTIVE_SUCCESS:
            return action.resellerSttcActiveSuccess
        default:
            return state
    }
}

const resellerActive = (state = [], action) => {
    switch (action.type) {
        case RESELLER_STTC_ACTIVE_SUCCESS:
            return action.resellerActive
        default:
            return state
    }
}

const operatorSttcAllError = (state = false, action) => {
    switch (action.type) {
        case OPERATOR_STTC_ALL_ERROR:
            return action.operatorSttcAllError
        default:
            return state
    }
}

const operatorSttcAllLoading = (state = false, action) => {
    switch (action.type) {
        case OPERATOR_STTC_ALL_LOADING:
            return action.operatorSttcAllLoading
        default:
            return state
    }
}

const operatorSttcAllSuccess = (state = false, action) => {
    switch (action.type) {
        case OPERATOR_STTC_ALL_SUCCESS:
            return action.operatorSttcAllSuccess
        default:
            return state
    }
}

const operatorAll = (state = [], action) => {
    switch (action.type) {
        case OPERATOR_STTC_ALL_SUCCESS:
            return action.operatorAll
        default:
            return state
    }
}

const operatorSttcActiveError = (state = false, action) => {
    switch (action.type) {
        case OPERATOR_STTC_ACTIVE_ERROR:
            return action.operatorSttcActiveError
        default:
            return state
    }
}

const operatorSttcActiveLoading = (state = false, action) => {
    switch (action.type) {
        case OPERATOR_STTC_ACTIVE_LOADING:
            return action.operatorSttcActiveLoading
        default:
            return state
    }
}

const operatorSttcActiveSuccess = (state = false, action) => {
    switch (action.type) {
        case OPERATOR_STTC_ACTIVE_SUCCESS:
            return action.operatorSttcActiveSuccess
        default:
            return state
    }
}

const operatorActive = (state = [], action) => {
    switch (action.type) {
        case OPERATOR_STTC_ACTIVE_SUCCESS:
            return action.operatorActive
        default:
            return state
    }
}

const regulatorSttcAllError = (state = false, action) => {
    switch (action.type) {
        case REGULATOR_STTC_ALL_ERROR:
            return action.regulatorSttcAllError
        default:
            return state
    }
}

const regulatorSttcAllLoading = (state = false, action) => {
    switch (action.type) {
        case REGULATOR_STTC_ALL_LOADING:
            return action.regulatorSttcAllLoading
        default:
            return state
    }
}

const regulatorSttcAllSuccess = (state = false, action) => {
    switch (action.type) {
        case REGULATOR_STTC_ALL_SUCCESS:
            return action.regulatorSttcAllSuccess
        default:
            return state
    }
}

const regulatorAll = (state = [], action) => {
    switch (action.type) {
        case REGULATOR_STTC_ALL_SUCCESS:
            return action.regulatorAll
        default:
            return state
    }
}

const regulatorSttcActiveError = (state = false, action) => {
    switch (action.type) {
        case REGULATOR_STTC_ACTIVE_ERROR:
            return action.regulatorSttcActiveError
        default:
            return state
    }
}

const regulatorSttcActiveLoading = (state = false, action) => {
    switch (action.type) {
        case REGULATOR_STTC_ACTIVE_LOADING:
            return action.regulatorSttcActiveLoading
        default:
            return state
    }
}

const regulatorSttcActiveSuccess = (state = false, action) => {
    switch (action.type) {
        case REGULATOR_STTC_ACTIVE_SUCCESS:
            return action.regulatorSttcActiveSuccess
        default:
            return state
    }
}

const regulatorActive = (state = [], action) => {
    switch (action.type) {
        case REGULATOR_STTC_ACTIVE_SUCCESS:
            return action.regulatorActive
        default:
            return state
    }
}

const operatorNameError = (state = false, action) => {
    switch (action.type) {
        case OPERATOR_NAME_ERROR:
            return action.operatorNameError
        default:
            return state
    }
}

const operatorNameLoading = (state = false, action) => {
    switch (action.type) {
        case OPERATOR_NAME_LOADING:
            return action.operatorNameLoading
        default:
            return state
    }
}

const operatorNameSuccess = (state = false, action) => {
    switch (action.type) {
        case OPERATOR_NAME_SUCCESS:
            return action.operatorNameSuccess
        default:
            return state
    }
}

const operatorName = (state = [], action) => {
    switch (action.type) {
        case OPERATOR_NAME_SUCCESS:
            return action.operatorName
        default:
            return state
    }
}


// Vessel Tracking
const vesselTrackingGetError = (state = false, action) => {
    switch (action.type) {
        case VESSEL_TRACKING_GET_ERROR:
            return action.vesselTrackingGetError
        default:
            return state
    }
}

const vesselTrackingGetLoading = (state = false, action) => {
    switch (action.type) {
        case VESSEL_TRACKING_GET_LOADING:
            return action.vesselTrackingGetLoading
        default:
            return state
    }
}

const vesselTrackingGetSuccess = (state = false, action) => {
    switch (action.type) {
        case VESSEL_TRACKING_GET_SUCCESS:
            return action.vesselTrackingGetSuccess
        default:
            return state
    }
}

const vesselTrackingGet = (state = [], action) => {
    switch (action.type) {
        case VESSEL_TRACKING_GET_SUCCESS:
            return action.vesselTrackingGet
        default:
            return state
    }
}


const vesselTrackingViewError = (state = false, action) => {
    switch (action.type) {
        case VESSEL_TRACKING_VIEW_ERROR:
            return action.vesselTrackingViewError
        default:
            return state
    }
}

const vesselTrackingViewLoading = (state = false, action) => {
    switch (action.type) {
        case VESSEL_TRACKING_VIEW_LOADING:
            return action.vesselTrackingViewLoading
        default:
            return state
    }
}

const vesselTrackingViewSuccess = (state = false, action) => {
    switch (action.type) {
        case VESSEL_TRACKING_VIEW_SUCCESS:
            return action.vesselTrackingViewSuccess
        default:
            return state
    }
}

const vesselTrackingView = (state = [], action) => {
    switch (action.type) {
        case VESSEL_TRACKING_VIEW_SUCCESS:
            return action.vesselTrackingView
        default:
            return state
    }
}

// order per jenis prioritas
const orderPerJenisPrioritasLoading = (state = false, action) => {
    switch (action.type) {
        case ORDER_PER_JENIS_PRIORITAS_GET_LOADING:
            return action.orderPerJenisPrioritasLoading
        default:
            return state
    }
}

const orderPerJenisPrioritasError = (state = false, action) => {
    switch (action.type) {
        case ORDER_PER_JENIS_PRIORITAS_GET_ERROR:
            return action.orderPerJenisPrioritasError
        default:
            return state
    }
}

const orderPerJenisPrioritasSuccess = (state = false, action) => {
    switch (action.type) {
        case ORDER_PER_JENIS_PRIORITAS_GET_SUCCESS:
            return action.orderPerJenisPrioritasSuccess
        default:
            return state
    }
}

const orderPerJenisPrioritasData = (state = [], action) => {
    switch (action.type) {
        case ORDER_PER_JENIS_PRIORITAS_GET_SUCCESS:
            return action.orderPerJenisPrioritasData
        default:
            return state
    }
}

// lima muatan terbanyak
const muatanTerbanyakLoading = (state = false, action) => {
    switch (action.type) {
        case MUATAN_TERBANYAK_GET_LOADING:
            return action.muatanTerbanyakLoading
        default:
            return state
    }
}

const muatanTerbanyakError = (state = false, action) => {
    switch (action.type) {
        case MUATAN_TERBANYAK_GET_ERROR:
            return action.muatanTerbanyakError
        default:
            return state
    }
}

const muatanTerbanyakSuccess = (state = false, action) => {
    switch (action.type) {
        case MUATAN_TERBANYAK_GET_SUCCESS:
            return action.muatanTerbanyakSuccess
        default:
            return state
    }
}

const muatanTerbanyakData = (state = [], action) => {
    switch (action.type) {
        case MUATAN_TERBANYAK_GET_SUCCESS:
            return action.muatanTerbanyakData
        default:
            return state
    }
}

// container pertahun
const containerPerTahunLoading = (state = false, action) => {
    switch (action.type) {
        case CONTAINER_PER_TAHUN_GET_LOADING:
            return action.containerPerTahunLoading
        default:
            return state
    }
}

const containerPerTahunError = (state = false, action) => {
    switch (action.type) {
        case CONTAINER_PER_TAHUN_GET_ERROR:
            return action.containerPerTahunError
        default:
            return state
    }
}

const containerPerTahunSuccess = (state = false, action) => {
    switch (action.type) {
        case CONTAINER_PER_TAHUN_GET_SUCCESS:
            return action.containerPerTahunSuccess
        default:
            return state
    }
}

const containerPerTahunData = (state = [], action) => {
    switch (action.type) {
        case CONTAINER_PER_TAHUN_GET_SUCCESS:
            return action.containerPerTahunData
        default:
            return state
    }
}

// muatan perwilayah
const muatanPerWilayahLoading = (state = false, action) => {
    switch (action.type) {
        case MUATAN_PER_WILAYAH_GET_LOADING:
            return action.muatanPerWilayahLoading
        default:
            return state
    }
}

const muatanPerWilayahError = (state = false, action) => {
    switch (action.type) {
        case MUATAN_PER_WILAYAH_GET_ERROR:
            return action.muatanPerWilayahError
        default:
            return state
    }
}

const muatanPerWilayahSuccess = (state = false, action) => {
    switch (action.type) {
        case MUATAN_PER_WILAYAH_GET_SUCCESS:
            return action.muatanPerWilayahSuccess
        default:
            return state
    }
}

const muatanPerWilayahData = (state = [], action) => {
    switch (action.type) {
        case MUATAN_PER_WILAYAH_GET_SUCCESS:
            return action.muatanPerWilayahData
        default:
            return state
    }
}

// order paid
const orderPaidLoading = (state = false, action) => {
    switch (action.type) {
        case ORDER_PAID_GET_LOADING:
            return action.orderPaidLoading
        default:
            return state
    }
}

const orderPaidError = (state = false, action) => {
    switch (action.type) {
        case ORDER_PAID_GET_ERROR:
            return action.orderPaidError
        default:
            return state
    }
}

const orderPaidSuccess = (state = false, action) => {
    switch (action.type) {
        case ORDER_PAID_GET_SUCCESS:
            return action.orderPaidSuccess
        default:
            return state
    }
}

const orderPaidData = (state = [], action) => {
    switch (action.type) {
        case ORDER_PAID_GET_SUCCESS:
            return action.orderPaidData
        default:
            return state
    }
}

// order unpaid
const orderUnpaidLoading = (state = false, action) => {
    switch (action.type) {
        case ORDER_UNPAID_GET_LOADING:
            return action.orderUnpaidLoading
        default:
            return state
    }
}

const orderUnpaidError = (state = false, action) => {
    switch (action.type) {
        case ORDER_UNPAID_GET_ERROR:
            return action.orderUnpaidError
        default:
            return state
    }
}

const orderUnpaidSuccess = (state = false, action) => {
    switch (action.type) {
        case ORDER_UNPAID_GET_SUCCESS:
            return action.orderUnpaidSuccess
        default:
            return state
    }
}

const orderUnpaidData = (state = [], action) => {
    switch (action.type) {
        case ORDER_UNPAID_GET_SUCCESS:
            return action.orderUnpaidData
        default:
            return state
    }
}

// order paid
const orderDeniedLoading = (state = false, action) => {
    switch (action.type) {
        case ORDER_DENIED_GET_LOADING:
            return action.orderDeniedLoading
        default:
            return state
    }
}

const orderDeniedError = (state = false, action) => {
    switch (action.type) {
        case ORDER_DENIED_GET_ERROR:
            return action.orderDeniedError
        default:
            return state
    }
}

const orderDeniedSuccess = (state = false, action) => {
    switch (action.type) {
        case ORDER_DENIED_GET_SUCCESS:
            return action.orderDeniedSuccess
        default:
            return state
    }
}

const orderDeniedData = (state = [], action) => {
    switch (action.type) {
        case ORDER_DENIED_GET_SUCCESS:
            return action.orderDeniedData
        default:
            return state
    }
}

// all orders
const allOrderLoading = (state = false, action) => {
    switch (action.type) {
        case ALL_ORDER_GET_LOADING:
            return action.allOrderLoading
        default:
            return state
    }
}

const allOrderError = (state = false, action) => {
    switch (action.type) {
        case ALL_ORDER_GET_ERROR:
            return action.allOrderError
        default:
            return state
    }
}

const allOrderSuccess = (state = false, action) => {
    switch (action.type) {
        case ALL_ORDER_GET_SUCCESS:
            return action.allOrderSuccess
        default:
            return state
    }
}

const allOrderData = (state = [], action) => {
    switch (action.type) {
        case ALL_ORDER_GET_SUCCESS:
            return action.allOrderData
        default:
            return state
    }
}

// order per bulan
const orderPerBulanLoading = (state = false, action) => {
    switch (action.type) {
        case ORDER_PER_BULAN_GET_LOADING:
            return action.orderPerBulanLoading
        default:
            return state
    }
}

const orderPerBulanError = (state = false, action) => {
    switch (action.type) {
        case ORDER_PER_BULAN_GET_ERROR:
            return action.orderPerBulanError
        default:
            return state
    }
}

const orderPerBulanSuccess = (state = false, action) => {
    switch (action.type) {
        case ORDER_PER_BULAN_GET_SUCCESS:
            return action.orderPerBulanSuccess
        default:
            return state
    }
}

const orderPerBulanData = (state = [], action) => {
    switch (action.type) {
        case ORDER_PER_BULAN_GET_SUCCESS:
            return action.orderPerBulanData
        default:
            return state
    }
}

// muatan per operator
const muatanPerOperatorLoading = (state = false, action) => {
    switch (action.type) {
        case MUATAN_PER_OPERATOR_GET_LOADING:
            return action.muatanPerOperatorLoading
        default:
            return state
    }
}

const muatanPerOperatorError = (state = false, action) => {
    switch (action.type) {
        case MUATAN_PER_OPERATOR_GET_ERROR:
            return action.muatanPerOperatorError
        default:
            return state
    }
}

const muatanPerOperatorSuccess = (state = false, action) => {
    switch (action.type) {
        case MUATAN_PER_OPERATOR_GET_SUCCESS:
            return action.muatanPerOperatorSuccess
        default:
            return state
    }
}

const muatanPerOperatorData = (state = [], action) => {
    switch (action.type) {
        case MUATAN_PER_OPERATOR_GET_SUCCESS:
            return action.muatanPerOperatorData
        default:
            return state
    }
}

// PO
const purchaseOrderLoading = (state = false, action) => {
    switch (action.type) {
        case PO_GET_LOADING:
            return action.purchaseOrderLoading
        default:
            return state
    }
}

const purchaseOrderError = (state = false, action) => {
    switch (action.type) {
        case PO_GET_ERROR:
            return action.purchaseOrderError
        default:
            return state
    }
}

const purchaseOrderSuccess = (state = false, action) => {
    switch (action.type) {
        case PO_GET_SUCCESS:
            return action.purchaseOrderSuccess
        default:
            return state
    }
}

const purchaseOrderData = (state = [], action) => {
    switch (action.type) {
        case PO_GET_SUCCESS:
            return action.purchaseOrderData
        default:
            return state
    }
}

// DO
const deliveryOrderLoading = (state = false, action) => {
    switch (action.type) {
        case DO_GET_LOADING:
            return action.deliveryOrderLoading
        default:
            return state
    }
}

const deliveryOrderError = (state = false, action) => {
    switch (action.type) {
        case DO_GET_ERROR:
            return action.deliveryOrderError
        default:
            return state
    }
}

const deliveryOrderSuccess = (state = false, action) => {
    switch (action.type) {
        case DO_GET_SUCCESS:
            return action.deliveryOrderSuccess
        default:
            return state
    }
}

const deliveryOrderData = (state = [], action) => {
    switch (action.type) {
        case DO_GET_SUCCESS:
            return action.deliveryOrderData
        default:
            return state
    }
}

// sisa quota trayek bulan ini
const sisaQuotaTrayekLoading = (state = false, action) => {
    switch (action.type) {
        case SISA_QUOTA_TRAYEK_GET_LOADING:
            return action.sisaQuotaTrayekLoading
        default:
            return state
    }
}

const sisaQuotaTrayekError = (state = false, action) => {
    switch (action.type) {
        case SISA_QUOTA_TRAYEK_GET_ERROR:
            return action.sisaQuotaTrayekError
        default:
            return state
    }
}

const sisaQuotaTrayekSuccess = (state = false, action) => {
    switch (action.type) {
        case SISA_QUOTA_TRAYEK_GET_SUCCESS:
            return action.sisaQuotaTrayekSuccess
        default:
            return state
    }
}

const sisaQuotaTrayekData = (state = [], action) => {
    switch (action.type) {
        case SISA_QUOTA_TRAYEK_GET_SUCCESS:
            return action.sisaQuotaTrayekData
        default:
            return state
    }
}

// voyage per trayek
const voyagePerTrayekLoading = (state = false, action) => {
    switch (action.type) {
        case VOYAGE_PER_TRAYEK_GET_LOADING:
            return action.voyagePerTrayekLoading
        default:
            return state
    }
}

const voyagePerTrayekError = (state = false, action) => {
    switch (action.type) {
        case VOYAGE_PER_TRAYEK_GET_ERROR:
            return action.voyagePerTrayekError
        default:
            return state
    }
}

const voyagePerTrayekSuccess = (state = false, action) => {
    switch (action.type) {
        case VOYAGE_PER_TRAYEK_GET_SUCCESS:
            return action.voyagePerTrayekSuccess
        default:
            return state
    }
}

const voyagePerTrayekData = (state = [], action) => {
    switch (action.type) {
        case VOYAGE_PER_TRAYEK_GET_SUCCESS:
            return action.voyagePerTrayekData
        default:
            return state
    }
}

// disparitas harga
const disparitasHargaLoading = (state = false, action) => {
    switch (action.type) {
        case DISPARITAS_HARGA_GET_LOADING:
            return action.disparitasHargaLoading
        default:
            return state
    }
}

const disparitasHargaError = (state = false, action) => {
    switch (action.type) {
        case DISPARITAS_HARGA_GET_ERROR:
            return action.disparitasHargaError
        default:
            return state
    }
}

const disparitasHargaSuccess = (state = false, action) => {
    switch (action.type) {
        case DISPARITAS_HARGA_GET_SUCCESS:
            return action.disparitasHargaSuccess
        default:
            return state
    }
}

const disparitasHargaData = (state = [], action) => {
    switch (action.type) {
        case DISPARITAS_HARGA_GET_SUCCESS:
            return action.disparitasHargaData
        default:
            return state
    }
}

// realisasi
const realisasiLoading = (state = false, action) => {
    switch (action.type) {
        case REALISASI_GET_LOADING:
            return action.realisasiLoading
        default:
            return state
    }
}

const realisasiError = (state = false, action) => {
    switch (action.type) {
        case REALISASI_GET_ERROR:
            return action.realisasiError
        default:
            return state
    }
}

const realisasiSuccess = (state = false, action) => {
    switch (action.type) {
        case REALISASI_GET_SUCCESS:
            return action.realisasiSuccess
        default:
            return state
    }
}

const realisasiData = (state = [], action) => {
    switch (action.type) {
        case REALISASI_GET_SUCCESS:
            return action.realisasiData
        default:
            return state
    }
}

// waktu tempuh
const waktuTempuhLoading = (state = false, action) => {
    switch (action.type) {
        case WAKTU_TEMPUH_GET_LOADING:
            return action.waktuTempuhLoading
        default:
            return state
    }
}

const waktuTempuhError = (state = false, action) => {
    switch (action.type) {
        case WAKTU_TEMPUH_GET_ERROR:
            return action.waktuTempuhError
        default:
            return state
    }
}

const waktuTempuhSuccess = (state = false, action) => {
    switch (action.type) {
        case WAKTU_TEMPUH_GET_SUCCESS:
            return action.waktuTempuhSuccess
        default:
            return state
    }
}

const waktuTempuhData = (state = [], action) => {
    switch (action.type) {
        case WAKTU_TEMPUH_GET_SUCCESS:
            return action.waktuTempuhData
        default:
            return state
    }
}

// jadwal kapal
const jadwalKapalLoading = (state = false, action) => {
    switch (action.type) {
        case JADWAL_KAPAL_GET_LOADING:
            return action.jadwalKapalLoading
        default:
            return state
    }
}

const jadwalKapalError = (state = false, action) => {
    switch (action.type) {
        case JADWAL_KAPAL_GET_ERROR:
            return action.jadwalKapalError
        default:
            return state
    }
}

const jadwalKapalSuccess = (state = false, action) => {
    switch (action.type) {
        case JADWAL_KAPAL_GET_SUCCESS:
            return action.jadwalKapalSuccess
        default:
            return state
    }
}

const jadwalKapalData = (state = [], action) => {
    switch (action.type) {
        case JADWAL_KAPAL_GET_SUCCESS:
            return action.jadwalKapalData
        default:
            return state
    }
}

// booking vessel
const bookingVesselLoading = (state = false, action) => {
    switch (action.type) {
        case BOOKING_VESSEL_GET_LOADING:
            return action.bookingVesselLoading
        default:
            return state
    }
}

const bookingVesselError = (state = false, action) => {
    switch (action.type) {
        case BOOKING_VESSEL_GET_ERROR:
            return action.bookingVesselError
        default:
            return state
    }
}

const bookingVesselSuccess = (state = false, action) => {
    switch (action.type) {
        case BOOKING_VESSEL_GET_SUCCESS:
            return action.bookingVesselSuccess
        default:
            return state
    }
}

const bookingVesselData = (state = [], action) => {
    switch (action.type) {
        case BOOKING_VESSEL_GET_SUCCESS:
            return action.bookingVesselData
        default:
            return state
    }
}

// distribusi barang
const distribusiBarangLoading = (state = false, action) => {
    switch (action.type) {
        case DISTRIBUSI_BARANG_GET_LOADING:
            return action.distribusiBarangLoading
        default:
            return state
    }
}

const distribusiBarangError = (state = false, action) => {
    switch (action.type) {
        case DISTRIBUSI_BARANG_GET_ERROR:
            return action.distribusiBarangError
        default:
            return state
    }
}

const distribusiBarangSuccess = (state = false, action) => {
    switch (action.type) {
        case DISTRIBUSI_BARANG_GET_SUCCESS:
            return action.distribusiBarangSuccess
        default:
            return state
    }
}

const distribusiBarangData = (state = [], action) => {
    switch (action.type) {
        case DISTRIBUSI_BARANG_GET_SUCCESS:
            return action.distribusiBarangData
        default:
            return state
    }
}

const dashboardReducer = combineReducers({
    consigneeSttcAllError,
    consigneeSttcAllLoading,
    consigneeSttcAllSuccess,
    consigneeAll,

    consigneeSttcActiveError,
    consigneeSttcActiveLoading,
    consigneeSttcActiveSuccess,
    consigneeActive,

    supplierSttcAllError,
    supplierSttcAllLoading,
    supplierSttcAllSuccess,
    supplierAll,

    supplierSttcActiveError,
    supplierSttcActiveLoading,
    supplierSttcActiveSuccess,
    supplierActive,

    shipperSttcAllError,
    shipperSttcAllLoading,
    shipperSttcAllSuccess,
    shipperAll,

    shipperSttcActiveError,
    shipperSttcActiveLoading,
    shipperSttcActiveSuccess,
    shipperActive,

    resellerSttcAllError,
    resellerSttcAllLoading,
    resellerSttcAllSuccess,
    resellerAll,

    resellerSttcActiveError,
    resellerSttcActiveLoading,
    resellerSttcActiveSuccess,
    resellerActive,

    operatorSttcAllError,
    operatorSttcAllLoading,
    operatorSttcAllSuccess,
    operatorAll,

    operatorSttcActiveError,
    operatorSttcActiveLoading,
    operatorSttcActiveSuccess,
    operatorActive,

    regulatorSttcAllError,
    regulatorSttcAllLoading,
    regulatorSttcAllSuccess,
    regulatorAll,

    regulatorSttcActiveError,
    regulatorSttcActiveLoading,
    regulatorSttcActiveSuccess,
    regulatorActive,

    operatorNameError,
    operatorNameLoading,
    operatorNameSuccess,
    operatorName,

    vesselTrackingGetError,
    vesselTrackingGetLoading,
    vesselTrackingGetSuccess,
    vesselTrackingGet,

    vesselTrackingViewError,
    vesselTrackingViewLoading,
    vesselTrackingViewSuccess,
    vesselTrackingView,

    orderPerJenisPrioritasLoading,
    orderPerJenisPrioritasError,
    orderPerJenisPrioritasSuccess,
    orderPerJenisPrioritasData,

    muatanTerbanyakLoading,
    muatanTerbanyakError,
    muatanTerbanyakSuccess,
    muatanTerbanyakData,

    containerPerTahunLoading,
    containerPerTahunError,
    containerPerTahunSuccess,
    containerPerTahunData,

    muatanPerWilayahLoading,
    muatanPerWilayahError,
    muatanPerWilayahSuccess,
    muatanPerWilayahData,

    orderPaidLoading,
    orderPaidError,
    orderPaidSuccess,
    orderPaidData,

    orderUnpaidLoading,
    orderUnpaidError,
    orderUnpaidSuccess,
    orderUnpaidData,

    orderDeniedLoading,
    orderDeniedError,
    orderDeniedSuccess,
    orderDeniedData,

    allOrderLoading,
    allOrderError,
    allOrderSuccess,
    allOrderData,

    orderPerBulanLoading,
    orderPerBulanError,
    orderPerBulanSuccess,
    orderPerBulanData,

    muatanPerOperatorLoading,
    muatanPerOperatorError,
    muatanPerOperatorSuccess,
    muatanPerOperatorData,

    purchaseOrderLoading,
    purchaseOrderError,
    purchaseOrderSuccess,
    purchaseOrderData,

    deliveryOrderLoading,
    deliveryOrderError,
    deliveryOrderSuccess,
    deliveryOrderData,

    sisaQuotaTrayekLoading,
    sisaQuotaTrayekError,
    sisaQuotaTrayekSuccess,
    sisaQuotaTrayekData,

    voyagePerTrayekLoading,
    voyagePerTrayekError,
    voyagePerTrayekSuccess,
    voyagePerTrayekData,

    disparitasHargaLoading,
    disparitasHargaError,
    disparitasHargaSuccess,
    disparitasHargaData,

    realisasiLoading,
    realisasiError,
    realisasiSuccess,
    realisasiData,

    waktuTempuhLoading,
    waktuTempuhError,
    waktuTempuhSuccess,
    waktuTempuhData,

    jadwalKapalLoading,
    jadwalKapalError,
    jadwalKapalSuccess,
    jadwalKapalData,

    bookingVesselLoading,
    bookingVesselError,
    bookingVesselSuccess,
    bookingVesselData,

    distribusiBarangLoading,
    distribusiBarangError,
    distribusiBarangSuccess,
    distribusiBarangData,
})

export default dashboardReducer