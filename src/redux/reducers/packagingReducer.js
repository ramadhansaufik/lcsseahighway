import {
    combineReducers
} from 'redux'
import {
    PACKAGING_GET_LOADING,
    PACKAGING_GET_ERROR,
    PACKAGING_GET_SUCCESS,

    PACKAGING_VIEW_ERROR,
    PACKAGING_VIEW_LOADING,
    PACKAGING_VIEW_SUCCESS,

    PACKAGING_ADD_ERROR,
    PACKAGING_ADD_LOADING,
    PACKAGING_ADD_SUCCESS,

    PACKAGING_DELETE_ERROR,
    PACKAGING_DELETE_LOADING,
    PACKAGING_DELETE_SUCCESS
} from '../actions/constant'

const packagingGetError = (state = false, action) => {
    switch (action.type) {
        case PACKAGING_GET_ERROR:
            return action.packagingGetError
        default:
            return state
    }
}

const packagingGetLoading = (state = false, action) => {
    switch (action.type) {
        case PACKAGING_GET_LOADING:
            return action.packagingGetLoading
        default:
            return state
    }
}

const packagingGetSuccess = (state = false, action) => {
    switch (action.type) {
        case PACKAGING_GET_SUCCESS:
            return action.packagingGetSuccess
        default:
            return state
    }
}

const packagings = (state = [], action) => {
    switch (action.type) {
        case PACKAGING_GET_SUCCESS:
            return action.packagings
        default:
            return state
    }
}

const packagingViewLoading = (state = false, action) => {
    switch (action.type) {
        case PACKAGING_VIEW_LOADING:
            return action.packagingViewLoading
        default:
            return state
    }
}

const packagingViewError = (state = false, action) => {
    switch (action.type) {
        case PACKAGING_VIEW_ERROR:
            return action.packagingViewError
        default:
            return state
    }
}

const packagingViewSuccess = (state = false, action) => {
    switch (action.type) {
        case PACKAGING_VIEW_SUCCESS:
            return action.packagingViewSuccess
        default:
            return state
    }
}

const packaging = (state = [], action) => {
    switch (action.type) {
        case PACKAGING_VIEW_SUCCESS:
            return action.packaging
        default:
            return state
    }
}

const packagingAddError = (state = false, action) => {
    switch (action.type) {
        case PACKAGING_ADD_ERROR:
            return action.packagingAddError
        default:
            return state
    }
}

const packagingAddLoading = (state = false, action) => {
    switch (action.type) {
        case PACKAGING_ADD_LOADING:
            return action.packagingAddLoading
        default:
            return state
    }
}

const packagingAddSuccess = (state = false, action) => {
    switch (action.type) {
        case PACKAGING_ADD_SUCCESS:
            return action.packagingAddSuccess
        default:
            return state
    }
}

const packagingDeleteError = (state = false, action) => {
    switch (action.type) {
        case PACKAGING_DELETE_ERROR:
            return action.packagingDeleteError
        default:
            return state
    }
}

const packagingDeleteLoading = (state = false, action) => {
    switch (action.type) {
        case PACKAGING_DELETE_LOADING:
            return action.packagingDeleteLoading
        default:
            return state
    }
}

const packagingDeleteSuccess = (state = false, action) => {
    switch (action.type) {
        case PACKAGING_DELETE_SUCCESS:
            return action.packagingDeleteSuccess
        default:
            return state
    }
}

const packagingReducer = combineReducers({
    packagingGetError,
    packagingGetLoading,
    packagingGetSuccess,
    packagings,

    packagingViewLoading,
    packagingViewError,
    packagingViewSuccess,
    packaging,

    packagingAddError,
    packagingAddLoading,
    packagingAddSuccess,

    packagingDeleteError,
    packagingDeleteLoading,
    packagingDeleteSuccess
})

export default packagingReducer