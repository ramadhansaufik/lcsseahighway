import {
    doLogout
} from './authAction'
import {
    BASE_URL,
    USER_URL,
    REGULATOR_URL,

    USER_GET_ERROR,
    USER_GET_LOADING,
    USER_GET_SUCCESS,

    USER_VIEW_ERROR,
    USER_VIEW_LOADING,
    USER_VIEW_SUCCESS,

    USER_ADD_ERROR,
    USER_ADD_LOADING,
    USER_ADD_SUCCESS,

    USER_DELETE_ERROR,
    USER_DELETE_LOADING,
    USER_DELETE_SUCCESS,
} from './constant'
import { AsyncStorage } from 'react-native'

const userGetError = (bool) => {
    return {
        type: USER_GET_ERROR,
        userGetError: bool
    }
}

const userGetLoading = (bool) => {
    return {
        type: USER_GET_LOADING,
        userGetLoading: bool
    }
}

const userGetSuccess = (bool, users) => {
    return {
        type: USER_GET_SUCCESS,
        userGetSuccess: bool,
        users
    }
}

const userViewError = (bool) => {
    return {
        type: USER_VIEW_ERROR,
        userViewError: bool
    }
}

const userViewLoading = (bool) => {
    return {
        type: USER_VIEW_LOADING,
        userViewLoading: bool
    }
}

const userViewSuccess = (bool, user) => {
    return {
        type: USER_VIEW_SUCCESS,
        userViewSuccess: bool,
        user: user
    }
}

const userAddError = (bool) => {
    return {
        type: USER_ADD_ERROR,
        userAddError: bool
    }
}

const userAddLoading = (bool) => {
    return {
        type: USER_ADD_LOADING,
        userAddLoading: bool
    }
}

const userAddSuccess = (bool, user) => {
    return {
        type: USER_ADD_SUCCESS,
        userAddSuccess: bool,
        user
    }
}

const userDeleteError = (bool) => {
    return {
        type: USER_DELETE_ERROR,
        userDeleteError: bool
    }
}

const userDeleteLoading = (bool) => {
    return {
        type: USER_DELETE_LOADING,
        userDeleteLoading: bool
    }
}

const userDeleteSuccess = (bool, user) => {
    return {
        type: USER_DELETE_SUCCESS,
        userDeleteSuccess: bool,
        user
    }
}

export function userFetchAll() {
    return (dispatch) => {
        dispatch(userGetSuccess(false, null))
        dispatch(userGetError(false))
        dispatch(userGetLoading(true))

        let token = AsyncStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + REGULATOR_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(userGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(userGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(userGetError(true))
            })
    }
}

export function userFetchSub(company_id) {
    return (dispatch) => {
        dispatch(userGetSuccess(false, null))
        dispatch(userGetError(false))
        dispatch(userGetLoading(true))

        let token = AsyncStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    company: company_id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + USER_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(userGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(userGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(userGetError(true))
            })
    }
}

export function userFetchPage(item) {
    return (dispatch) => {
        dispatch(userGetSuccess(false, null))
        dispatch(userGetError(false))
        dispatch(userGetLoading(true))

        let token = AsyncStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(item)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + USER_URL + '/page', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(userGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(userGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(userGetError(true))
            })
    }
}

export function userAdd(data) {
    return (dispatch) => {
        dispatch(userAddError(false))
        dispatch(userAddSuccess(false, null))
        dispatch(userAddLoading(true))

        let token = AsyncStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + REGULATOR_URL + '/new', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(userAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((user) => {
                dispatch(userAddSuccess(true, user))
            })
            .catch((err) => {
                dispatch(userAddError(true))
            })
    }
}

export function userChangePassword(data) {
    return (dispatch) => {
        dispatch(userAddError(false))
        dispatch(userAddSuccess(false, null))
        dispatch(userAddLoading(true))

        let token = AsyncStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + USER_URL + '/change_password', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(userAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((user) => {
                dispatch(userAddSuccess(true, user))
            })
            .catch((err) => {
                dispatch(userAddError(true))
            })
    }
}

export function userResetPassword(data) {
    return (dispatch) => {
        dispatch(userAddError(false))
        dispatch(userAddSuccess(false, null))
        dispatch(userAddLoading(true))

        let token = AsyncStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + USER_URL + '/reset_password', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(userAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((user) => {
                dispatch(userAddSuccess(true, user))
            })
            .catch((err) => {
                dispatch(userAddError(true))
            })
    }
}

export function userFetchOne(id) {
    return (dispatch) => {
        dispatch(userViewError(false))
        dispatch(userViewSuccess(false, null))
        dispatch(userViewLoading(true))

        let token = AsyncStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + USER_URL + "/" + id, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(userViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(userViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(userViewError(true))
            })
    }
}

export function userFetchUser(id) {
    return async (dispatch) => {
        dispatch(userViewError(false))
        dispatch(userViewSuccess(false, null))
        dispatch(userViewLoading(true))
        let token = await AsyncStorage.getItem('tolaut@token') || null
        let config = {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: id
            })
        }
        fetch(BASE_URL + REGULATOR_URL + '/profile', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(userViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(userViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(userViewError(true))
            })
    }
}

export function userUpdate(data) {
    return (dispatch) => {
        dispatch(userAddError(false))
        dispatch(userAddSuccess(false, null))
        dispatch(userAddLoading(true))

        let token = AsyncStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'PUT',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + REGULATOR_URL, config)
            .then((response) => {

                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(userAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((user) => {

                if (user.success) {
                    dispatch(userAddSuccess(true, user))
                } else {
                    dispatch(userAddError(true))
                }
            })
            .catch((err) => {
                dispatch(userAddError(true))
            })
    }
}

export function handleStatus(data) {
    return (dispatch) => {
        dispatch(userAddError(false))
        dispatch(userAddSuccess(false, null))
        dispatch(userAddLoading(true))

        let token = AsyncStorage.getItem('tolaut@token') || null
        let config = {}

        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    id: data.id
                })
            }

        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + 'user/' + data.status, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(userAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((user) => {
                dispatch(userAddSuccess(true, user))
            })
            .catch((err) => {
                dispatch(userAddError(true))
            })
    }
}

export function userDelete(id) {
    return (dispatch) => {
        dispatch(userViewSuccess(false, null))
        dispatch(userDeleteError(false))
        dispatch(userDeleteSuccess(false, null))
        dispatch(userDeleteLoading(true))

        let token = AsyncStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'id': id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + USER_URL, config)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(userDeleteLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(userDeleteSuccess(true, data))
            })
            .catch((err) => {
                dispatch(userDeleteError(true))
            })
    }
}