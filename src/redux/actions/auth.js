// Before doing auth in auth navigation stack
// check if there is any token credential in LocalStorage
// so the user not need to login
// if the credential already exists

import axios from 'axios';

// Login
// ---------------------------------- SYNC
const AUTH_LOGIN = 'AUTH_LOGIN' 
export function authLogin() {
	return {
		type: AUTH_LOGIN,
		info: 'Request login API from API taking email and password as parameters'
	}
}

const AUTH_LOGIN_SUCCESS = 'AUTH_LOGIN_SUCCESS'
export function authLoginSuccess(payload) {
	return {
		type: AUTH_LOGIN_SUCCESS,
		payload: payload,
		info: 'Return user information when the login authentication success'
	}
}

const AUTH_LOGIN_FAILED = 'AUTH_LOGIN_FAILED'
export function authLoginFailed(payload) {
	return {
		type: AUTH_LOGIN_FAILED,
		payload: payload,
		info: 'Return error from the server when the login authentication failed'
	}
}
// ---------------------------------- ASYNC
export function requestLogin(payload) {
	return (dispatch, getState) => {
		dispatch(authLogin());
		return axios.post('http://103.107.103.112/api/v1/login', payload)
      .then(response => response.data)
      .then(data => data.status === 404 ? 
      	dispatch(authLoginFailed(data.message)) : 
      	dispatch(authLoginSuccess(data.token))
      )
      .catch(error => console.warn(`terjadi error : ${error}`))
	}
}


// Register
// ---------------------------------- SYNC
const AUTH_REGISTER = 'AUTH_REGISTER'
export function authRegister() {
	return {
		type: AUTH_REGISTER,
		info: 'Dispath this when user request user registration'
	}
}

const AUTH_REGISTER_SUCCESS = 'AUTH_REGISTER_SUCCESS'
export function authRegisterSuccess(payload) {
	return {
		type: AUTH_REGISTER_SUCCESS,
		payload: payload,
		info: 'Dispatch this when user successfully register the user'
	}
}

const AUTH_REGISTER_FAILED = 'AUTH_REGISTER_FAILED'
export function authRegisterFailed(payload) {
	return {
		type: AUTH_REGISTER_FAILED,
		payload: payload,
		info: 'Dispatch this when use fail to register the user'
	}
}

const REQUEST_REGISTER = 'REQUEST_REGISTER'
export function requestRegister(payload) {
	return {
		type: REQUEST_REGISTER,
		payload: payload,
		info: 'Insert user_type_id, email, and password for new user'
	}
}

// ---------------------------------- ASYNC
export function submitRegister(payload) {
	return (dispatch, getState) => {
	}
}