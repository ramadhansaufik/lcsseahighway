import {
    doLogout
} from './authAction'
import {
    BASE_URL,
    TYPECONTAINER_URL,

    TYPECONTAINER_GET_ERROR,
    TYPECONTAINER_GET_LOADING,
    TYPECONTAINER_GET_SUCCESS,

    TYPECONTAINER_VIEW_ERROR,
    TYPECONTAINER_VIEW_LOADING,
    TYPECONTAINER_VIEW_SUCCESS,

    TYPECONTAINER_ADD_ERROR,
    TYPECONTAINER_ADD_LOADING,
    TYPECONTAINER_ADD_SUCCESS,

    TYPECONTAINER_DELETE_ERROR,
    TYPECONTAINER_DELETE_LOADING,
    TYPECONTAINER_DELETE_SUCCESS,

    TYPECONTAINER_PRICE_UPDATE_URL,
    TYPECONTAINER_PRICE_UPDATE_ERROR,
    TYPECONTAINER_PRICE_UPDATE_LOADING,
    TYPECONTAINER_PRICE_UPDATE_SUCCESS
} from './constant'

const typecontainerGetError = (bool) => {
    return {
        type: TYPECONTAINER_GET_ERROR,
        typecontainerGetError: bool
    }
}

const typecontainerGetLoading = (bool) => {
    return {
        type: TYPECONTAINER_GET_LOADING,
        typecontainerGetLoading: bool
    }
}

const typecontainerGetSuccess = (bool, typecontainers) => {
    return {
        type: TYPECONTAINER_GET_SUCCESS,
        typecontainerGetSuccess: bool,
        typecontainers
    }
}

const typecontainerViewError = (bool) => {
    return {
        type: TYPECONTAINER_VIEW_ERROR,
        typecontainerViewError: bool
    }
}

const typecontainerViewLoading = (bool) => {
    return {
        type: TYPECONTAINER_VIEW_LOADING,
        typecontainerViewLoading: bool
    }
}

const typecontainerViewSuccess = (bool, typecontainer) => {
    return {
        type: TYPECONTAINER_VIEW_SUCCESS,
        typecontainerViewSuccess: bool,
        typecontainer
    }
}

const typecontainerAddError = (bool) => {
    return {
        type: TYPECONTAINER_ADD_ERROR,
        typecontainerAddError: bool
    }
}

const typecontainerAddLoading = (bool) => {
    return {
        type: TYPECONTAINER_ADD_LOADING,
        typecontainerAddLoading: bool
    }
}

const typecontainerAddSuccess = (bool, typecontainer) => {
    return {
        type: TYPECONTAINER_ADD_SUCCESS,
        typecontainerAddSuccess: bool,
        typecontainer
    }
}

const typecontainerDeleteError = (bool) => {
    return {
        type: TYPECONTAINER_DELETE_ERROR,
        typecontainerDeleteError: bool
    }
}

const typecontainerDeleteLoading = (bool) => {
    return {
        type: TYPECONTAINER_DELETE_LOADING,
        typecontainerDeleteLoading: bool
    }
}

const typecontainerDeleteSuccess = (bool, typecontainer) => {
    return {
        type: TYPECONTAINER_DELETE_SUCCESS,
        typecontainerDeleteSuccess: bool,
        typecontainer
    }
}

export function typecontainerFetchAll(tokenAuth) {
    return (dispatch) => {
        dispatch(typecontainerAddSuccess(false, null))
        dispatch(typecontainerGetSuccess(false, null))
        dispatch(typecontainerGetError(false))
        dispatch(typecontainerGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + TYPECONTAINER_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(typecontainerGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {

                dispatch(typecontainerGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(typecontainerGetError(true))
            })
    }
}

export function typecontainerFetchSub(path) {
    return (dispatch) => {
        dispatch(typecontainerAddSuccess(false, null))
        dispatch(typecontainerGetSuccess(false, null))
        dispatch(typecontainerGetError(false))
        dispatch(typecontainerGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + TYPECONTAINER_URL + '/' + path, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(typecontainerGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(typecontainerGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(typecontainerGetError(true))
            })
    }
}

export function typecontainerFetchPage(params) {
    return (dispatch) => {
        dispatch(typecontainerAddSuccess(false, null))
        dispatch(typecontainerGetSuccess(false, null))
        dispatch(typecontainerGetError(false))
        dispatch(typecontainerGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + TYPECONTAINER_URL + '/page', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(typecontainerGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(typecontainerGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(typecontainerGetError(true))
            })
    }
}

export function typecontainerAdd(data) {
    return (dispatch) => {
        dispatch(typecontainerAddError(false))
        dispatch(typecontainerAddSuccess(false, null))
        dispatch(typecontainerAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + TYPECONTAINER_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(typecontainerAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((typecontainer) => {
                dispatch(typecontainerAddSuccess(true, typecontainer))
            })
            .catch((err) => {
                dispatch(typecontainerAddError(true))
            })
    }
}

export function typecontainerFetchOne(id) {
    return (dispatch) => {
        dispatch(typecontainerViewError(false))
        dispatch(typecontainerViewSuccess(false, null))
        dispatch(typecontainerViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + TYPECONTAINER_URL + '/' + id, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(typecontainerViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(typecontainerViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(typecontainerViewError(true))
            })
    }
}

export function typecontainerUpdate(data) {
    return (dispatch) => {
        dispatch(typecontainerAddError(false))
        dispatch(typecontainerAddSuccess(false, null))
        dispatch(typecontainerAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'PUT',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + TYPECONTAINER_URL, config)
            .then((response) => {

                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(typecontainerAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((typecontainer) => {

                if (typecontainer.success) {
                    dispatch(typecontainerAddSuccess(true, typecontainer))
                } else {
                    dispatch(typecontainerAddError(true))
                }
            })
            .catch((err) => {
                dispatch(typecontainerAddError(true))
            })
    }
}

export function typecontainerDelete(id) {
    return (dispatch) => {
        dispatch(typecontainerViewSuccess(false, null))
        dispatch(typecontainerDeleteError(false))
        dispatch(typecontainerDeleteSuccess(false, null))
        dispatch(typecontainerDeleteLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'id': id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + TYPECONTAINER_URL, config)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(typecontainerDeleteLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(typecontainerDeleteSuccess(true, data))
            })
            .catch((err) => {
                dispatch(typecontainerDeleteError(true))
            })
    }
}

//Update Price
const typecontainerPriceUpdateError = (bool) => {
    return {
        type: TYPECONTAINER_PRICE_UPDATE_ERROR,
        typecontainerPriceUpdateError: bool
    }
}

const typecontainerPriceUpdateLoading = (bool) => {
    return {
        type: TYPECONTAINER_PRICE_UPDATE_LOADING,
        typecontainerPriceUpdateLoading: bool
    }
}

const typecontainerPriceUpdateSuccess = (bool, typecontainer) => {
    return {
        type: TYPECONTAINER_PRICE_UPDATE_SUCCESS,
        typecontainerPriceUpdateSuccess: bool,
        typecontainer
    }
}

export function typecontainerPriceUpdate(data) {
    return (dispatch) => {
        dispatch(typecontainerPriceUpdateError(false))
        dispatch(typecontainerPriceUpdateSuccess(false, null))
        dispatch(typecontainerPriceUpdateLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + TYPECONTAINER_PRICE_UPDATE_URL, config)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(typecontainerPriceUpdateLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(typecontainerPriceUpdateSuccess(true, data))
            })
            .catch((err) => {
                dispatch(typecontainerPriceUpdateError(true))
            })
    }
}