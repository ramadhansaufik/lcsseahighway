import axios from 'axios'
import { AsyncStorage } from 'react-native'
import {
    BASE_URL,
    AUTH_LOGIN_URL,
    AUTH_REGISTER_URL,

    AUTH_LOGIN_ERROR,
    AUTH_LOGIN_LOADING,
    AUTH_LOGIN_SUCCESS,

    AUTH_LOGOUT_LOADING,
    AUTH_LOGOUT_SUCCESS,

    AUTH_REGISTER_ERROR,
    AUTH_REGISTER_LOADING,
    AUTH_REGISTER_SUCCESS,

    AUTH_REGISTER_CHECK_URL,
    AUTH_REGISTER_CHECK_ERROR,
    AUTH_REGISTER_CHECK_LOADING,
    AUTH_REGISTER_CHECK_SUCCESS,

    IS_AUTHENTICATED
} from './constant'
import { userFetchUser } from './userAction'
import { consigneeFetchUser } from './consigneeAction' 

// login Actions
const authLoginError = (bool) => {
    return {
        type: AUTH_LOGIN_ERROR,
        authLoginError: bool,
        message: 'Login Gagal'
    }
}

const authLoginLoading = (bool) => {
    return {
        type: AUTH_LOGIN_LOADING,
        authLoginLoading: bool
    }
}

const authLoginSuccess = (bool, account) => {
    return {
        type: AUTH_LOGIN_SUCCESS,
        authLoginSuccess: bool,
        isAuthenticated: bool,
        account: account
    }
}

export function doLogin(cred) {
    return (dispatch) => {
        dispatch(authLoginSuccess(false, null))
        dispatch(authLoginError(false))
        dispatch(authLoginLoading(true))

        let config = {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            url: BASE_URL + AUTH_LOGIN_URL,
            data: {
                email: cred.email,
                password: cred.password,
                usery_type_id: cred.usery_type_id
            }
        }

        axios(config)
            .then(response => {
                AsyncStorage.removeItem('tolaut@email');
                AsyncStorage.removeItem('tolaut@typeUser');
                AsyncStorage.removeItem('tolaut@token');
                AsyncStorage.removeItem('tolaut@userId');
                
                if (response.data.status === 200) {
                    AsyncStorage.setItem('tolaut@email', response.data.email);
                    AsyncStorage.setItem('tolaut@typeUser', cred.usery_type_id);
                    AsyncStorage.setItem('tolaut@token', response.data.token);
                    AsyncStorage.setItem('tolaut@userId', response.data.id);
                    switch(cred.usery_type_id) {
                        case '1':
                            dispatch(consigneeFetchUser(response.data.data[0].id))
                        break;
                        case '2':
                        break;
                        case '3':
                        break;
                        case '4':
                        break;
                        case '5':
                        break;
                        default :
                            dispatch(userFetchUser(response.data.data[0].id))
                    }
                    dispatch(authLoginSuccess(true, response.data))
                } else {
                    console.warn(response.data.message)
                    dispatch(authLoginError(true))
                }
            })
            .catch(error => {
                console.warn(`Terjadi error, ${error}`)
            })
    }
}

// Register Actions
const authRegisterError = (bool) => {
    return {
        type: AUTH_REGISTER_ERROR,
        authRegisterError: bool
    }
}

const authRegisterLoading = (bool) => {
    return {
        type: AUTH_REGISTER_LOADING,
        authRegisterLoading: bool
    }
}

const authRegisterSuccess = (bool, account) => {
    return {
        type: AUTH_REGISTER_SUCCESS,
        authRegisterSuccess: bool,
        isAuthenticated: bool,
        account: account
    }
}

export function doRegister(cred) {
    return (dispatch) => {
        dispatch(authRegisterSuccess(false, null))
        dispatch(authRegisterError(false))
        dispatch(authRegisterLoading(true))

        let config = {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            url: BASE_URL + AUTH_REGISTER_URL,
            data: {
                email: cred.email,
                password: cred.password,
                usery_type_id: cred.usery_type_id
            }
        }

        axios(config)
            .then(response => {

                AsyncStorage.removeItem('tolaut@email');
                AsyncStorage.removeItem('tolaut@typeUser');
                AsyncStorage.removeItem('tolaut@token');
                AsyncStorage.removeItem('tolaut@userId');

                if(response.data.status === 200) {
                    AsyncStorage.setItem('tolaut@email', response.data.email);
                    AsyncStorage.setItem('tolaut@typeUser', response.data.usery_type_id);
                    AsyncStorage.setItem('tolaut@token', response.data.token);
                    AsyncStorage.setItem('tolaut@userId', response.data.id);
                    dispatch(authRegisterSuccess(true, response.data)) // Success
                    dispatch(authRegisterError(false))
                    // console.warn(response.data);
                } else {
                    dispatch(authRegisterError(true)) // Error
                    dispatch(authRegisterSuccess(false, null))
                    // console.warn(response.data);
                }

                dispatch(authRegisterLoading(false))
            })
            .catch(error => {
                console.warn(`error euy: ${error}`);
                dispatch(authRegisterError(true)) // Error
                dispatch(authRegisterLoading(false))
                dispatch(authRegisterSuccess(false, null))
            })

        // fetch(BASE_URL + AUTH_REGISTER_URL, config)
        //     .then((response) => {
        //         if (!response.ok) {
        //             dispatch(authRegisterError(true))
        //         }
        //         dispatch(authRegisterLoading(false))
        //         return response
        //     })
        //     .then((response) => response.json())
        //     .then((account) => {

        //         if (account.success) {
        //             dispatch(authRegisterSuccess(true, account))
        //         } else {
        //             dispatch(authRegisterError(true))
        //         }
        //     })
        //     .catch((err) => {
        //         dispatch(authRegisterError(true))
        //     })
    }
}

// Logout Actions
const authLogoutLoading = (bool) => {
    return {
        type: AUTH_LOGOUT_LOADING,
        authLogoutLoading: bool
    }
}

const authLogoutSuccess = (bool) => {
    return {
        type: AUTH_LOGOUT_SUCCESS,
        authLogoutSuccess: bool,
        isAuthenticated: !bool
    }
}

export function doLogout() {
    return dispatch => {
        dispatch(authLogoutLoading(true))
        AsyncStorage.removeItem('tolaut@email')
        AsyncStorage.removeItem('tolaut@typeUser')
        AsyncStorage.removeItem('tolaut@userId')
        AsyncStorage.removeItem('tolaut@token')
        dispatch(authLogoutLoading(false))
        dispatch(authLogoutSuccess(true))
    }
}

// Register Check if use had been registered before
const authRegisterCheckError = (bool) => {
    return {
        type: AUTH_REGISTER_CHECK_ERROR,
        authRegisterCheckError: bool
    }
}

const authRegisterCheckLoading = (bool) => {
    return {
        type: AUTH_REGISTER_CHECK_LOADING,
        authRegisterCheckLoading: bool
    }
}

const authRegisterCheckSuccess = (bool, check_data) => {
    return {
        type: AUTH_REGISTER_CHECK_SUCCESS,
        authRegisterCheckSuccess: bool,
        isAuthenticated: bool,
        check_data: check_data
    }
}

export function doRegisterCheck(cred) {
    return (dispatch) => {
        dispatch(authRegisterCheckSuccess(false, null))
        dispatch(authRegisterCheckError(false))
        dispatch(authRegisterCheckLoading(true))

        let config = {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            url: BASE_URL + AUTH_REGISTER_CHECK_URL,
            data: {
                email: cred.email,
                usery_type_id: cred.usery_type_id
            }
        }

        axios(config)
            .then(response => {
                if(response.data.success) {
                    console.warn(response.data.data)
                    console.warn(response.data.message)
                    dispatch(authRegisterCheckSuccess(true, response.data.data))
                    dispatch(authRegisterCheckError(false))

                } else {
                    console.warn(response.data.data)
                    console.warn(response.data.message)
                    dispatch(authRegisterCheckError(true))
                }
                dispatch(authRegisterCheckLoading(false))
            })
            .catch(error => {
                console.warn(`Terjadi Error: ${error}`);
                dispatch(authRegisterCheckError(true))
            })

        // let config = {
        //     method: 'POST',
        //     headers: {
        //         'Content-Type': 'application/json'
        //     },
        //     body: JSON.stringify({
        //         email: cred.email,
        //         usery_type_id: cred.usery_type_id
        //     })
        // }


        // fetch(BASE_URL + AUTH_REGISTER_CHECK_URL, config)
        //     .then((response) => {
        //         if (!response.ok) {
        //             dispatch(authRegisterCheckError(true))
        //         }
        //         dispatch(authRegisterCheckLoading(false))
        //         return response
        //     })
        //     .then((response) => response.json())
        //     .then((check_data) => {
        //         if (check_data.success) {
        //             dispatch(authRegisterCheckSuccess(true, check_data))
        //         } else {
        //             dispatch(authRegisterCheckError(true))
        //         }
        //     })
        //     .catch((err) => {
        //         dispatch(authRegisterCheckError(true))
        //     })
    }
}