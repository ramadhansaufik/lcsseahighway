import {
    doLogout
} from './authAction'
import {
    BASE_URL,
    VESSEL_URL,

    VESSEL_GET_ERROR,
    VESSEL_GET_LOADING,
    VESSEL_GET_SUCCESS,

    VESSEL_VIEW_ERROR,
    VESSEL_VIEW_LOADING,
    VESSEL_VIEW_SUCCESS,

    VESSEL_ADD_ERROR,
    VESSEL_ADD_LOADING,
    VESSEL_ADD_INVALID,
    VESSEL_ADD_SUCCESS,

    VESSEL_DELETE_ERROR,
    VESSEL_DELETE_LOADING,
    VESSEL_DELETE_SUCCESS
} from './constant'

const vesselGetError = (bool) => {
    return {
        type: VESSEL_GET_ERROR,
        vesselGetError: bool
    }
}

const vesselGetLoading = (bool) => {
    return {
        type: VESSEL_GET_LOADING,
        vesselGetLoading: bool
    }
}

const vesselGetSuccess = (bool, vessels) => {
    return {
        type: VESSEL_GET_SUCCESS,
        vesselGetSuccess: bool,
        vessels
    }
}

const vesselViewError = (bool) => {
    return {
        type: VESSEL_VIEW_ERROR,
        vesselViewError: bool
    }
}

const vesselViewLoading = (bool) => {
    return {
        type: VESSEL_VIEW_LOADING,
        vesselViewLoading: bool
    }
}

const vesselViewSuccess = (bool, vessel) => {
    return {
        type: VESSEL_VIEW_SUCCESS,
        vesselViewSuccess: bool,
        vessel
    }
}

const vesselAddError = (bool) => {
    return {
        type: VESSEL_ADD_ERROR,
        vesselAddError: bool
    }
}

const vesselAddInvalid = (bool, invalid) => {
    return {
        type: VESSEL_ADD_INVALID,
        vesselAddInvalid: bool,
        invalid
    }
}

const vesselAddLoading = (bool) => {
    return {
        type: VESSEL_ADD_LOADING,
        vesselAddLoading: bool
    }
}

const vesselAddSuccess = (bool) => {
    return {
        type: VESSEL_ADD_SUCCESS,
        vesselAddSuccess: bool
    }
}

const vesselDeleteError = (bool) => {
    return {
        type: VESSEL_DELETE_ERROR,
        vesselDeleteError: bool
    }
}

const vesselDeleteLoading = (bool) => {
    return {
        type: VESSEL_DELETE_LOADING,
        vesselDeleteLoading: bool
    }
}

const vesselDeleteSuccess = (bool) => {
    return {
        type: VESSEL_DELETE_SUCCESS,
        vesselDeleteSuccess: bool
    }
}

export function vesselFetchAll(item) {
    return (dispatch) => {
        dispatch(vesselGetSuccess(false, null))
        dispatch(vesselGetError(false))
        dispatch(vesselGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(item)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + VESSEL_URL + '/all', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(vesselGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(vesselGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(vesselGetError(true))
            })
    }
}

export function vesselFetchSub(path) {
    return (dispatch) => {
        dispatch(vesselGetSuccess(false, null))
        dispatch(vesselGetError(false))
        dispatch(vesselGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + VESSEL_URL + path, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(vesselGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(vesselGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(vesselGetError(true))
            })
    }
}

export function vesselFetchPage(item) {
    return (dispatch) => {
        dispatch(vesselGetSuccess(false, null))
        dispatch(vesselGetError(false))
        dispatch(vesselGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(item)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + VESSEL_URL + '/page', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(vesselGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(vesselGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(vesselGetError(true))
            })
    }
}

export function vesselFetchOne(id) {
    return (dispatch) => {
        dispatch(vesselViewSuccess(false, null))
        dispatch(vesselViewError(false))
        dispatch(vesselViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + VESSEL_URL + '/' + id, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(vesselViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(vesselViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(vesselViewError(true))
            })
    }
}

export function vesselAdd(data) {
    return (dispatch) => {
        dispatch(vesselAddError(false))
        dispatch(vesselAddSuccess(false))
        dispatch(vesselAddLoading(true))
        dispatch(vesselAddInvalid(false, null))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        let formData = new FormData();
        for (var key in data) {
            formData.append(key, data[key])
        }

        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`
                },
                body: formData
            }
        } else {
            throw Error("Authentication error")
        }

        fetch(BASE_URL + VESSEL_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(vesselAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                if (data.success) {
                    dispatch(vesselAddSuccess(true))
                } else {
                    dispatch(vesselAddInvalid(true, data))
                }
            })
            .catch((err) => {
                dispatch(vesselAddError(true))
            })
    }
}

export function vesselUpdate(data) {

    return (dispatch) => {
        dispatch(vesselAddError(false))
        dispatch(vesselAddSuccess(false))
        dispatch(vesselAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        let formData = new FormData();
        for (var key in data) {
            formData.append(key, data[key])
        }

        if (token) {
            config = {
                method: 'PUT',
                headers: {
                    'Authorization': `Bearer ${token}`,
                },
                body: formData
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + VESSEL_URL, config)
            .then((response) => {

                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(vesselAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                if (data.success) {
                    dispatch(vesselAddSuccess(true))
                } else {
                    dispatch(vesselAddError(true))
                }
            })
            .catch((err) => {
                dispatch(vesselAddError(true))
            })
    }
}

export function vesselDelete(id) {
    return (dispatch) => {
        dispatch(vesselDeleteError(false))
        dispatch(vesselDeleteSuccess(false))
        dispatch(vesselDeleteLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'id': id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + VESSEL_URL, config)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(vesselDeleteLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                if (data.success) {
                    dispatch(vesselDeleteSuccess(true))
                }
            })
            .catch((err) => {
                dispatch(vesselDeleteError(true))
            })
    }
}