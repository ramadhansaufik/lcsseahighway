import {
    doLogout
} from './authAction'
import {
    BASE_URL,
    UNIT_URL,

    UNIT_GET_ERROR,
    UNIT_GET_LOADING,
    UNIT_GET_SUCCESS,

    UNIT_VIEW_ERROR,
    UNIT_VIEW_LOADING,
    UNIT_VIEW_SUCCESS,

    UNIT_ADD_ERROR,
    UNIT_ADD_LOADING,
    UNIT_ADD_SUCCESS,

    UNIT_DELETE_ERROR,
    UNIT_DELETE_LOADING,
    UNIT_DELETE_SUCCESS
} from './constant'

const unitGetError = (bool) => {
    return {
        type: UNIT_GET_ERROR,
        unitGetError: bool
    }
}

const unitGetLoading = (bool) => {
    return {
        type: UNIT_GET_LOADING,
        unitGetLoading: bool
    }
}

const unitGetSuccess = (bool, units) => {
    return {
        type: UNIT_GET_SUCCESS,
        unitGetSuccess: bool,
        units
    }
}

const unitViewError = (bool) => {
    return {
        type: UNIT_VIEW_ERROR,
        unitViewError: bool
    }
}

const unitViewLoading = (bool) => {
    return {
        type: UNIT_VIEW_LOADING,
        unitViewLoading: bool
    }
}

const unitViewSuccess = (bool, unit) => {
    return {
        type: UNIT_VIEW_SUCCESS,
        unitViewSuccess: bool,
        unit
    }
}

const unitAddError = (bool) => {
    return {
        type: UNIT_ADD_ERROR,
        unitAddError: bool
    }
}

const unitAddLoading = (bool) => {
    return {
        type: UNIT_ADD_LOADING,
        unitAddLoading: bool
    }
}

const unitAddSuccess = (bool, unit) => {
    return {
        type: UNIT_ADD_SUCCESS,
        unitAddSuccess: bool,
        unit
    }
}

const unitDeleteError = (bool) => {
    return {
        type: UNIT_DELETE_ERROR,
        unitDeleteError: bool
    }
}

const unitDeleteLoading = (bool) => {
    return {
        type: UNIT_DELETE_LOADING,
        unitDeleteLoading: bool
    }
}

const unitDeleteSuccess = (bool, unit) => {
    return {
        type: UNIT_DELETE_SUCCESS,
        unitDeleteSuccess: bool,
        unit
    }
}

export function unitFetchAll(tokenAuth) {
    return (dispatch) => {
        dispatch(unitAddSuccess(false, null))
        dispatch(unitGetSuccess(false, null))
        dispatch(unitGetError(false))
        dispatch(unitGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + UNIT_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(unitGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {

                dispatch(unitGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(unitGetError(true))
            })
    }
}

export function unitFetchSub(path) {
    return (dispatch) => {
        dispatch(unitAddSuccess(false, null))
        dispatch(unitGetSuccess(false, null))
        dispatch(unitGetError(false))
        dispatch(unitGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + UNIT_URL + '/' + path, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(unitGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(unitGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(unitGetError(true))
            })
    }
}

export function unitFetchPage(params) {
    return (dispatch) => {
        dispatch(unitAddSuccess(false, null))
        dispatch(unitGetSuccess(false, null))
        dispatch(unitGetError(false))
        dispatch(unitGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + UNIT_URL + '/page', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(unitGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(unitGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(unitGetError(true))
            })
    }
}

export function unitAdd(data) {
    return (dispatch) => {
        dispatch(unitAddError(false))
        dispatch(unitAddSuccess(false, null))
        dispatch(unitAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + UNIT_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(unitAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((unit) => {
                dispatch(unitAddSuccess(true, unit))
            })
            .catch((err) => {
                dispatch(unitAddError(true))
            })
    }
}

export function unitFetchOne(id) {
    return (dispatch) => {
        dispatch(unitViewError(false))
        dispatch(unitViewSuccess(false, null))
        dispatch(unitViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + UNIT_URL + '/' + id, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(unitViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(unitViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(unitViewError(true))
            })
    }
}

export function unitUpdate(data) {
    return (dispatch) => {
        dispatch(unitAddError(false))
        dispatch(unitAddSuccess(false, null))
        dispatch(unitAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'PUT',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + UNIT_URL, config)
            .then((response) => {

                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(unitAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((unit) => {

                if (unit.success) {
                    dispatch(unitAddSuccess(true, unit))
                } else {
                    dispatch(unitAddError(true))
                }
            })
            .catch((err) => {
                dispatch(unitAddError(true))
            })
    }
}

export function unitDelete(id) {
    return (dispatch) => {
        dispatch(unitViewSuccess(false, null))
        dispatch(unitDeleteError(false))
        dispatch(unitDeleteSuccess(false, null))
        dispatch(unitDeleteLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'id': id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + UNIT_URL, config)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(unitDeleteLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(unitDeleteSuccess(true, data))
            })
            .catch((err) => {
                dispatch(unitDeleteError(true))
            })
    }
}