import {
    doLogout
} from './authAction'
import {
    BASE_URL,
    PROVINSI_URL,

    PROVINSI_GET_ERROR,
    PROVINSI_GET_LOADING,
    PROVINSI_GET_SUCCESS,

    PROVINSI_VIEW_ERROR,
    PROVINSI_VIEW_LOADING,
    PROVINSI_VIEW_SUCCESS,

    PROVINSI_ADD_ERROR,
    PROVINSI_ADD_LOADING,
    PROVINSI_ADD_SUCCESS
} from './constant'

const provinsiGetError = (bool) => {
    return {
        type: PROVINSI_GET_ERROR,
        provinsiGetError: bool
    }
}

const provinsiGetLoading = (bool) => {
    return {
        type: PROVINSI_GET_LOADING,
        provinsiGetLoading: bool
    }
}

const provinsiGetSuccess = (bool, provinsis) => {
    return {
        type: PROVINSI_GET_SUCCESS,
        provinsiGetSuccess: bool,
        provinsis
    }
}

const provinsiViewError = (bool) => {
    return {
        type: PROVINSI_VIEW_ERROR,
        provinsiViewError: bool
    }
}

const provinsiViewLoading = (bool) => {
    return {
        type: PROVINSI_VIEW_LOADING,
        provinsiViewLoading: bool
    }
}

const provinsiViewSuccess = (bool, provinsi) => {
    return {
        type: PROVINSI_VIEW_SUCCESS,
        provinsiViewSuccess: bool,
        provinsi
    }
}

const provinsiAddError = (bool) => {
    return {
        type: PROVINSI_ADD_ERROR,
        provinsiAddError: bool
    }
}

const provinsiAddLoading = (bool) => {
    return {
        type: PROVINSI_ADD_LOADING,
        provinsiAddLoading: bool
    }
}

const provinsiAddSuccess = (bool, provinsi) => {
    return {
        type: PROVINSI_ADD_SUCCESS,
        provinsiAddSuccess: bool,
        provinsi
    }
}

export function provinsiFetchAll() {
    return (dispatch) => {
        dispatch(provinsiGetSuccess(false, null))
        dispatch(provinsiGetError(false))
        dispatch(provinsiGetLoading(true))

        let config = {}
        config = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }
        fetch(BASE_URL + PROVINSI_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(provinsiGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(provinsiGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(provinsiGetError(true))
            })
    }
}

export function provinsiFetchSub(path) {
    return (dispatch) => {
        dispatch(provinsiGetSuccess(false, null))
        dispatch(provinsiGetError(false))
        dispatch(provinsiGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + PROVINSI_URL + '/' + path, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(provinsiGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(provinsiGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(provinsiGetError(true))
            })
    }
}

export function provinsiAdd(data) {
    return (dispatch) => {
        dispatch(provinsiAddError(false))
        dispatch(provinsiAddSuccess(false, null))
        dispatch(provinsiAddLoading(true))
        let config = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }
        fetch(BASE_URL + PROVINSI_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(provinsiAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((provinsi) => {
                dispatch(provinsiAddSuccess(true, provinsi))
            })
            .catch((err) => {
                dispatch(provinsiAddError(true))
            })
    }
}

export function provinsiFetchOne(id) {
    return (dispatch) => {
        dispatch(provinsiViewError(false))
        dispatch(provinsiViewSuccess(false, null))
        dispatch(provinsiViewLoading(true))
        let config = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: id
            })
        }
        fetch(BASE_URL + PROVINSI_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(provinsiViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(provinsiViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(provinsiViewError(true))
            })
    }
}