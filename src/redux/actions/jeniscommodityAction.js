import {
    doLogout
} from './authAction'
import {
    BASE_URL,
    JENISCOMMODITY_URL,

    JENISCOMMODITY_GET_ERROR,
    JENISCOMMODITY_GET_LOADING,
    JENISCOMMODITY_GET_SUCCESS,

    JENISCOMMODITY_VIEW_ERROR,
    JENISCOMMODITY_VIEW_LOADING,
    JENISCOMMODITY_VIEW_SUCCESS,

    JENISCOMMODITY_ADD_ERROR,
    JENISCOMMODITY_ADD_LOADING,
    JENISCOMMODITY_ADD_SUCCESS,

    JENISCOMMODITY_DELETE_ERROR,
    JENISCOMMODITY_DELETE_LOADING,
    JENISCOMMODITY_DELETE_SUCCESS
} from './constant'

const jeniscommodityGetError = (bool) => {
    return {
        type: JENISCOMMODITY_GET_ERROR,
        jeniscommodityGetError: bool
    }
}

const jeniscommodityGetLoading = (bool) => {
    return {
        type: JENISCOMMODITY_GET_LOADING,
        jeniscommodityGetLoading: bool
    }
}

const jeniscommodityGetSuccess = (bool, jeniscommodities) => {
    return {
        type: JENISCOMMODITY_GET_SUCCESS,
        jeniscommodityGetSuccess: bool,
        jeniscommodities
    }
}

const jeniscommodityViewError = (bool) => {
    return {
        type: JENISCOMMODITY_VIEW_ERROR,
        jeniscommodityViewError: bool
    }
}

const jeniscommodityViewLoading = (bool) => {
    return {
        type: JENISCOMMODITY_VIEW_LOADING,
        jeniscommodityViewLoading: bool
    }
}

const jeniscommodityViewSuccess = (bool, jeniscommodity) => {
    return {
        type: JENISCOMMODITY_VIEW_SUCCESS,
        jeniscommodityViewSuccess: bool,
        jeniscommodity
    }
}

const jeniscommodityAddError = (bool) => {
    return {
        type: JENISCOMMODITY_ADD_ERROR,
        jeniscommodityAddError: bool
    }
}

const jeniscommodityAddLoading = (bool) => {
    return {
        type: JENISCOMMODITY_ADD_LOADING,
        jeniscommodityAddLoading: bool
    }
}

const jeniscommodityAddSuccess = (bool) => {
    return {
        type: JENISCOMMODITY_ADD_SUCCESS,
        jeniscommodityAddSuccess: bool
    }
}

const jeniscommodityDeleteError = (bool) => {
    return {
        type: JENISCOMMODITY_DELETE_ERROR,
        jeniscommodityDeleteError: bool
    }
}

const jeniscommodityDeleteLoading = (bool) => {
    return {
        type: JENISCOMMODITY_DELETE_LOADING,
        jeniscommodityDeleteLoading: bool
    }
}

const jeniscommodityDeleteSuccess = (bool) => {
    return {
        type: JENISCOMMODITY_DELETE_SUCCESS,
        jeniscommodityDeleteSuccess: bool
    }
}

export function jeniscommodityFetchAll() {
    return (dispatch) => {
        dispatch(jeniscommodityGetSuccess(false, null))
        dispatch(jeniscommodityGetError(false))
        dispatch(jeniscommodityGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + JENISCOMMODITY_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(jeniscommodityGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(jeniscommodityGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(jeniscommodityGetError(true))
            })
    }
}

export function jeniscommodityFetchSub(path) {
    return (dispatch) => {
        dispatch(jeniscommodityGetSuccess(false, null))
        dispatch(jeniscommodityGetError(false))
        dispatch(jeniscommodityGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + JENISCOMMODITY_URL + path, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(jeniscommodityGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(jeniscommodityGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(jeniscommodityGetError(true))
            })
    }
}

export function jeniscommodityFetchPage(item) {
    return (dispatch) => {
        dispatch(jeniscommodityGetSuccess(false, null))
        dispatch(jeniscommodityGetError(false))
        dispatch(jeniscommodityGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(item)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + JENISCOMMODITY_URL + '/page', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(jeniscommodityGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(jeniscommodityGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(jeniscommodityGetError(true))
            })
    }
}

export function jeniscommodityFetchOne(id) {
    return (dispatch) => {
        dispatch(jeniscommodityViewSuccess(false, null))
        dispatch(jeniscommodityViewError(false))
        dispatch(jeniscommodityViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + JENISCOMMODITY_URL + '/' + id, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(jeniscommodityViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(jeniscommodityViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(jeniscommodityViewError(true))
            })
    }
}

export function jeniscommodityAdd(data) {
    return (dispatch) => {
        dispatch(jeniscommodityAddError(false))
        dispatch(jeniscommodityAddSuccess(false))
        dispatch(jeniscommodityAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        let formData = new FormData();
        for (var key in data) {
            formData.append(key, data[key])
        }

        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                },
                body: formData
            }
        } else {
            throw Error("Authentication error")
        }

        fetch(BASE_URL + JENISCOMMODITY_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(jeniscommodityAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                if (data.success) {
                    dispatch(jeniscommodityAddSuccess(true))
                }
            })
            .catch((err) => {
                dispatch(jeniscommodityAddError(true))
            })
    }
}

export function jeniscommodityUpdate(data) {
    return (dispatch) => {
        dispatch(jeniscommodityAddError(false))
        dispatch(jeniscommodityAddSuccess(false))
        dispatch(jeniscommodityAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        let formData = new FormData();
        for (var key in data) {
            formData.append(key, data[key])
        }

        if (token) {
            config = {
                method: 'PUT',
                headers: {
                    'Authorization': `Bearer ${token}`,
                },
                body: formData
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + JENISCOMMODITY_URL, config)
            .then((response) => {

                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(jeniscommodityAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                if (data.success) {
                    dispatch(jeniscommodityAddSuccess(true))
                } else {
                    dispatch(jeniscommodityAddError(true))
                }
            })
            .catch((err) => {
                dispatch(jeniscommodityAddError(true))
            })
    }
}

export function jeniscommodityDelete(id) {
    return (dispatch) => {
        dispatch(jeniscommodityDeleteError(false))
        dispatch(jeniscommodityDeleteSuccess(false))
        dispatch(jeniscommodityDeleteLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'id': id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + JENISCOMMODITY_URL, config)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(jeniscommodityDeleteLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                if (data.success) {
                    dispatch(jeniscommodityDeleteSuccess(true))
                }
            })
            .catch((err) => {
                dispatch(jeniscommodityDeleteError(true))
            })
    }
}