import {
    doLogout
} from './authAction'
import {
    BASE_URL,
    OPERATOR_URL,

    OPERATOR_GET_ERROR,
    OPERATOR_GET_LOADING,
    OPERATOR_GET_SUCCESS,

    OPERATOR_VIEW_ERROR,
    OPERATOR_VIEW_LOADING,
    OPERATOR_VIEW_SUCCESS,

    OPERATOR_ADD_ERROR,
    OPERATOR_ADD_LOADING,
    OPERATOR_ADD_INVALID,
    OPERATOR_ADD_SUCCESS,

    OPERATOR_DELETE_ERROR,
    OPERATOR_DELETE_LOADING,
    OPERATOR_DELETE_SUCCESS
} from './constant'

const operatorGetError = (bool) => {
    return {
        type: OPERATOR_GET_ERROR,
        operatorGetError: bool
    }
}

const operatorGetLoading = (bool) => {
    return {
        type: OPERATOR_GET_LOADING,
        operatorGetLoading: bool
    }
}

const operatorGetSuccess = (bool, operators) => {
    return {
        type: OPERATOR_GET_SUCCESS,
        operatorGetSuccess: bool,
        operators
    }
}

const operatorViewError = (bool) => {
    return {
        type: OPERATOR_VIEW_ERROR,
        operatorViewError: bool
    }
}

const operatorViewLoading = (bool) => {
    return {
        type: OPERATOR_VIEW_LOADING,
        operatorViewLoading: bool
    }
}

const operatorViewSuccess = (bool, operator) => {
    return {
        type: OPERATOR_VIEW_SUCCESS,
        operatorViewSuccess: bool,
        operator
    }
}

const operatorAddError = (bool) => {
    return {
        type: OPERATOR_ADD_ERROR,
        operatorAddError: bool
    }
}

const operatorAddLoading = (bool) => {
    return {
        type: OPERATOR_ADD_LOADING,
        operatorAddLoading: bool
    }
}

const operatorAddInvalid = (bool, invalid) => {
    return {
        type: OPERATOR_ADD_INVALID,
        operatorAddInvalid: bool,
        invalid
    }
}

const operatorAddSuccess = (bool, operator) => {
    return {
        type: OPERATOR_ADD_SUCCESS,
        operatorAddSuccess: bool,
        operator
    }
}

const operatorDeleteError = (bool) => {
    return {
        type: OPERATOR_DELETE_ERROR,
        operatorDeleteError: bool
    }
}

const operatorDeleteLoading = (bool) => {
    return {
        type: OPERATOR_DELETE_LOADING,
        operatorDeleteLoading: bool
    }
}

const operatorDeleteSuccess = (bool, operator) => {
    return {
        type: OPERATOR_DELETE_SUCCESS,
        operatorDeleteSuccess: bool,
        operator
    }
}

export function operatorFetchAll() {
    return (dispatch) => {
        dispatch(operatorGetSuccess(false, null))
        dispatch(operatorGetError(false))
        dispatch(operatorGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + OPERATOR_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(operatorGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(operatorGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(operatorGetError(true))
            })
    }
}

export function operatorFetchSub(operator_id) {
    return (dispatch) => {
        dispatch(operatorGetSuccess(false, null))
        dispatch(operatorGetError(false))
        dispatch(operatorGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + OPERATOR_URL + '/' + operator_id, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(operatorGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(operatorGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(operatorGetError(true))
            })
    }
}

export function operatorFetchPage(item) {
    return (dispatch) => {
        dispatch(operatorGetSuccess(false, null))
        dispatch(operatorGetError(false))
        dispatch(operatorGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(item)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + OPERATOR_URL + '/page', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(operatorGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(operatorGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(operatorGetError(true))
            })
    }
}

export function operatorAdd(data) {
    return (dispatch) => {
        dispatch(operatorAddError(false))
        dispatch(operatorAddSuccess(false, null))
        dispatch(operatorAddInvalid(false, null))
        dispatch(operatorAddLoading(true))

        let token = data.tokenAuth || null
        let config = {}
        let formData = new FormData();
        for (var key in data) {
            formData.append(key, data[key])
        }
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                },
                body: formData
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + OPERATOR_URL + '/new', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(operatorAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((operator) => {
                if (operator.success) {
                    dispatch(operatorAddSuccess(true, operator))
                } else {
                    dispatch(operatorAddInvalid(true, operator))
                }
            })
            .catch((err) => {
                dispatch(operatorAddError(true))
            })
    }
}

export function operatorUpdate(data) {
    return (dispatch) => {
        dispatch(operatorAddError(false))
        dispatch(operatorAddSuccess(false, null))
        dispatch(operatorAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        let formData = new FormData();
        for (var key in data) {
            formData.append(key, data[key])
        }
        if (token) {
            config = {
                method: 'PUT',
                headers: {
                    'Authorization': `Bearer ${token}`,
                },
                body: formData
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + OPERATOR_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(operatorAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((operator) => {
                dispatch(operatorAddSuccess(true, operator))
            })
            .catch((err) => {
                dispatch(operatorAddError(true))
            })
    }
}

export function operatorConfirm(data) {

    return (dispatch) => {
        dispatch(operatorAddError(false))
        dispatch(operatorAddSuccess(false, null))
        dispatch(operatorAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + OPERATOR_URL + '/approve', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(operatorAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((operator) => {
                if (operator.status === '200 OK') {
                    dispatch(operatorAddSuccess(true, operator))
                } else {
                    dispatch(operatorAddError(true))
                }
            })
            .catch((err) => {
                dispatch(operatorAddError(true))
            })
    }
}

export function operatorFetchOne(id) {
    return (dispatch) => {
        dispatch(operatorViewError(false))
        dispatch(operatorViewSuccess(false, null))
        dispatch(operatorViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + OPERATOR_URL + '/' + id, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(operatorViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(operatorViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(operatorViewError(true))
            })
    }
}

export function operatorFetchUser(id) {
    return (dispatch) => {
        dispatch(operatorViewError(false))
        dispatch(operatorViewSuccess(false, null))
        dispatch(operatorViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    id: id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + OPERATOR_URL + '/profile', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(operatorViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(operatorViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(operatorViewError(true))
            })
    }
}

export function handleStatus(data) {
    return (dispatch) => {
        dispatch(operatorAddError(false))
        dispatch(operatorAddSuccess(false, null))
        dispatch(operatorAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}

        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }

        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + 'register/' + data.status, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(operatorAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((operator) => {
                dispatch(operatorAddSuccess(true, operator))
            })
            .catch((err) => {
                dispatch(operatorAddError(true))
            })
    }
}

export function operatorDelete(id) {
    return (dispatch) => {
        dispatch(operatorViewSuccess(false, null))
        dispatch(operatorDeleteError(false))
        dispatch(operatorDeleteSuccess(false, null))
        dispatch(operatorDeleteLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'id': id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + OPERATOR_URL, config)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(operatorDeleteLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(operatorDeleteSuccess(true, data))
            })
            .catch((err) => {
                dispatch(operatorDeleteError(true))
            })
    }
}