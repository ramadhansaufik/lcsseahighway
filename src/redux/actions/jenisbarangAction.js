import {
    doLogout
} from './authAction'
import {
    BASE_URL,
    JENISBARANG_URL,

    JENISBARANG_GET_ERROR,
    JENISBARANG_GET_LOADING,
    JENISBARANG_GET_SUCCESS,

} from './constant'

const jenisbarangGetError = (bool) => {
    return {
        type: JENISBARANG_GET_ERROR,
        jenisbarangGetError: bool
    }
}

const jenisbarangGetLoading = (bool) => {
    return {
        type: JENISBARANG_GET_LOADING,
        jenisbarangGetLoading: bool
    }
}

const jenisbarangGetSuccess = (bool, jenisbarangs) => {
    return {
        type: JENISBARANG_GET_SUCCESS,
        jenisbarangGetSuccess: bool,
        jenisbarangs
    }
}

export function jenisbarangFetchAll() {
    return (dispatch) => {
        dispatch(jenisbarangGetSuccess(false, null))
        dispatch(jenisbarangGetError(false))
        dispatch(jenisbarangGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + JENISBARANG_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(jenisbarangGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(jenisbarangGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(jenisbarangGetError(true))
            })
    }
}