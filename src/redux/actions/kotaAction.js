import {
    doLogout
} from './authAction'
import {
    BASE_URL,
    KOTA_URL,

    KOTA_GET_ERROR,
    KOTA_GET_LOADING,
    KOTA_GET_SUCCESS,

    KOTA_VIEW_ERROR,
    KOTA_VIEW_LOADING,
    KOTA_VIEW_SUCCESS,

    KOTA_ADD_ERROR,
    KOTA_ADD_LOADING,
    KOTA_ADD_SUCCESS
} from './constant'

const kotaGetError = (bool) => {
    return {
        type: KOTA_GET_ERROR,
        kotaGetError: bool
    }
}

const kotaGetLoading = (bool) => {
    return {
        type: KOTA_GET_LOADING,
        kotaGetLoading: bool
    }
}

const kotaGetSuccess = (bool, kotas) => {
    return {
        type: KOTA_GET_SUCCESS,
        kotaGetSuccess: bool,
        kotas
    }
}

const kotaViewError = (bool) => {
    return {
        type: KOTA_VIEW_ERROR,
        kotaViewError: bool
    }
}

const kotaViewLoading = (bool) => {
    return {
        type: KOTA_VIEW_LOADING,
        kotaViewLoading: bool
    }
}

const kotaViewSuccess = (bool, kota) => {
    return {
        type: KOTA_VIEW_SUCCESS,
        kotaViewSuccess: bool,
        kota
    }
}

const kotaAddError = (bool) => {
    return {
        type: KOTA_ADD_ERROR,
        kotaAddError: bool
    }
}

const kotaAddLoading = (bool) => {
    return {
        type: KOTA_ADD_LOADING,
        kotaAddLoading: bool
    }
}

const kotaAddSuccess = (bool, kota) => {
    return {
        type: KOTA_ADD_SUCCESS,
        kotaAddSuccess: bool,
        kota
    }
}

export function kotaFetchAll() {
    return (dispatch) => {
        dispatch(kotaGetSuccess(false, null))
        dispatch(kotaGetError(false))
        dispatch(kotaGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + KOTA_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(kotaGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(kotaGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(kotaGetError(true))
            })
    }
}

export function kotaFetchSub(dataInput) {

    return (dispatch) => {
        dispatch(kotaGetSuccess(false, null))
        dispatch(kotaGetError(false))
        dispatch(kotaGetLoading(true))

        let config = {}
        config = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }
        fetch(BASE_URL + KOTA_URL + '/' + dataInput, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(kotaGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(kotaGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(kotaGetError(true))
            })
    }
}

export function kotaAdd(data) {
    return (dispatch) => {
        dispatch(kotaAddError(false))
        dispatch(kotaAddSuccess(false, null))
        dispatch(kotaAddLoading(true))
        let config = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }
        fetch(BASE_URL + KOTA_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(kotaAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((kota) => {
                dispatch(kotaAddSuccess(true, kota))
            })
            .catch((err) => {
                dispatch(kotaAddError(true))
            })
    }
}

export function kotaFetchOne(id) {
    return (dispatch) => {
        dispatch(kotaViewError(false))
        dispatch(kotaViewSuccess(false, null))
        dispatch(kotaViewLoading(true))
        let config = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: id
            })
        }
        fetch(BASE_URL + KOTA_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(kotaViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(kotaViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(kotaViewError(true))
            })
    }
}