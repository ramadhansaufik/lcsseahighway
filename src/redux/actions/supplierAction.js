import {
    doLogout
} from './authAction'
import {
    BASE_URL,
    SUPPLIER_URL,

    SUPPLIER_GET_ERROR,
    SUPPLIER_GET_LOADING,
    SUPPLIER_GET_SUCCESS,

    SUPPLIER_VIEW_ERROR,
    SUPPLIER_VIEW_LOADING,
    SUPPLIER_VIEW_SUCCESS,

    SUPPLIER_ADD_ERROR,
    SUPPLIER_ADD_LOADING,
    SUPPLIER_ADD_INVALID,
    SUPPLIER_ADD_SUCCESS,

    SUPPLIER_DELETE_ERROR,
    SUPPLIER_DELETE_LOADING,
    SUPPLIER_DELETE_SUCCESS
} from './constant'

const supplierGetError = (bool) => {
    return {
        type: SUPPLIER_GET_ERROR,
        supplierGetError: bool
    }
}

const supplierGetLoading = (bool) => {
    return {
        type: SUPPLIER_GET_LOADING,
        supplierGetLoading: bool
    }
}

const supplierGetSuccess = (bool, suppliers) => {
    return {
        type: SUPPLIER_GET_SUCCESS,
        supplierGetSuccess: bool,
        suppliers
    }
}

const supplierViewError = (bool) => {
    return {
        type: SUPPLIER_VIEW_ERROR,
        supplierViewError: bool
    }
}

const supplierViewLoading = (bool) => {
    return {
        type: SUPPLIER_VIEW_LOADING,
        supplierViewLoading: bool
    }
}

const supplierViewSuccess = (bool, supplier) => {
    return {
        type: SUPPLIER_VIEW_SUCCESS,
        supplierViewSuccess: bool,
        supplier
    }
}

const supplierAddError = (bool) => {
    return {
        type: SUPPLIER_ADD_ERROR,
        supplierAddError: bool
    }
}

const supplierAddLoading = (bool) => {
    return {
        type: SUPPLIER_ADD_LOADING,
        supplierAddLoading: bool
    }
}

const supplierAddInvalid = (bool, invalid) => {
    return {
        type: SUPPLIER_ADD_INVALID,
        supplierAddInvalid: bool,
        invalid
    }
}

const supplierAddSuccess = (bool, supplier) => {
    return {
        type: SUPPLIER_ADD_SUCCESS,
        supplierAddSuccess: bool,
        supplier
    }
}

const supplierDeleteError = (bool) => {
    return {
        type: SUPPLIER_DELETE_ERROR,
        supplierDeleteError: bool
    }
}

const supplierDeleteLoading = (bool) => {
    return {
        type: SUPPLIER_DELETE_LOADING,
        supplierDeleteLoading: bool
    }
}

const supplierDeleteSuccess = (bool, supplier) => {
    return {
        type: SUPPLIER_DELETE_SUCCESS,
        supplierDeleteSuccess: bool,
        supplier
    }
}

export function supplierFetchAll() {
    return (dispatch) => {
        dispatch(supplierGetSuccess(false, null))
        dispatch(supplierGetError(false))
        dispatch(supplierGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + SUPPLIER_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(supplierGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(supplierGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(supplierGetError(true))
            })
    }
}

export function supplierFetchSub(company_id) {
    return (dispatch) => {
        dispatch(supplierGetSuccess(false, null))
        dispatch(supplierGetError(false))
        dispatch(supplierGetLoading(true))
        let config = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                company: company_id
            })
        }
        fetch(BASE_URL + SUPPLIER_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(supplierGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(supplierGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(supplierGetError(true))
            })
    }
}

export function supplierFetchPage(item) {
    return (dispatch) => {
        dispatch(supplierGetSuccess(false, null))
        dispatch(supplierGetError(false))
        dispatch(supplierGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(item)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + SUPPLIER_URL + '/page', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(supplierGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(supplierGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(supplierGetError(true))
            })
    }
}

export function supplierAdd(data) {
    return (dispatch) => {
        dispatch(supplierAddError(false))
        dispatch(supplierAddInvalid(false, null))
        dispatch(supplierAddSuccess(false, null))
        dispatch(supplierAddLoading(true))

        let token = data.tokenAuth || null
        let config = {}
        let formData = new FormData();
        for (var key in data) {
            formData.append(key, data[key])
        }

        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                },
                body: formData
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + SUPPLIER_URL + '/new', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(supplierAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((supplier) => {
                if (supplier.success) {
                    dispatch(supplierAddSuccess(true, supplier))
                } else {
                    dispatch(supplierAddInvalid(true, supplier))
                }
            })
            .catch((err) => {
                dispatch(supplierAddError(true))
            })
    }
}

export function supplierUpdate(data) {
    return (dispatch) => {
        dispatch(supplierAddError(false))
        dispatch(supplierAddSuccess(false, null))
        dispatch(supplierAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        let formData = new FormData();
        for (var key in data) {
            formData.append(key, data[key])
        }

        if (token) {
            config = {
                method: 'PUT',
                headers: {
                    'Authorization': `Bearer ${token}`,
                },
                body: formData
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + SUPPLIER_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(supplierAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((supplier) => {
                dispatch(supplierAddSuccess(true, supplier))
            })
            .catch((err) => {
                dispatch(supplierAddError(true))
            })
    }
}

export function supplierFetchOne(id) {
    return (dispatch) => {
        dispatch(supplierViewError(false))
        dispatch(supplierViewSuccess(false, null))
        dispatch(supplierViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + SUPPLIER_URL + '/' + id, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(supplierViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(supplierViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(supplierViewError(true))
            })
    }
}

export function supplierFetchUser(id) {
    return (dispatch) => {
        dispatch(supplierViewError(false))
        dispatch(supplierViewSuccess(false, null))
        dispatch(supplierViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'id': id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + SUPPLIER_URL + '/profile', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(supplierViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(supplierViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(supplierViewError(true))
            })
    }
}

export function handleStatus(data) {
    return (dispatch) => {
        dispatch(supplierAddError(false))
        dispatch(supplierAddSuccess(false, null))
        dispatch(supplierAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}

        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }

        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + 'register/' + data.status, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(supplierAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((supplier) => {
                dispatch(supplierAddSuccess(true, supplier))
            })
            .catch((err) => {
                dispatch(supplierAddError(true))
            })
    }
}

export function supplierDelete(id) {
    return (dispatch) => {
        dispatch(supplierViewSuccess(false, null))
        dispatch(supplierDeleteError(false))
        dispatch(supplierDeleteSuccess(false, null))
        dispatch(supplierDeleteLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'id': id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + SUPPLIER_URL, config)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(supplierDeleteLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(supplierDeleteSuccess(true, data))
            })
            .catch((err) => {
                dispatch(supplierDeleteError(true))
            })
    }
}