import { AsyncStorage } from 'react-native'
import {
    doLogout
} from './authAction'
import {
    BASE_URL,
    CONSIGNEE_URL,

    CONSIGNEE_GET_ERROR,
    CONSIGNEE_GET_LOADING,
    CONSIGNEE_GET_SUCCESS,

    CONSIGNEE_VIEW_ERROR,
    CONSIGNEE_VIEW_LOADING,
    CONSIGNEE_VIEW_SUCCESS,

    CONSIGNEE_ADD_ERROR,
    CONSIGNEE_ADD_LOADING,
    CONSIGNEE_ADD_INVALID,
    CONSIGNEE_ADD_SUCCESS,

    CONSIGNEE_DELETE_ERROR,
    CONSIGNEE_DELETE_LOADING,
    CONSIGNEE_DELETE_SUCCESS
} from './constant'
import axios from 'axios'

const consigneeGetError = (bool) => {
    return {
        type: CONSIGNEE_GET_ERROR,
        consigneeGetError: bool
    }
}

const consigneeGetLoading = (bool) => {
    return {
        type: CONSIGNEE_GET_LOADING,
        consigneeGetLoading: bool
    }
}

const consigneeGetSuccess = (bool, consignees) => {
    return {
        type: CONSIGNEE_GET_SUCCESS,
        consigneeGetSuccess: bool,
        consignees
    }
}

const consigneeViewError = (bool) => {
    return {
        type: CONSIGNEE_VIEW_ERROR,
        consigneeViewError: bool
    }
}

const consigneeViewLoading = (bool) => {
    return {
        type: CONSIGNEE_VIEW_LOADING,
        consigneeViewLoading: bool
    }
}

const consigneeViewSuccess = (bool, consignee) => {
    return {
        type: CONSIGNEE_VIEW_SUCCESS,
        consigneeViewSuccess: bool,
        consignee
    }
}

const consigneeAddError = (bool) => {
    return {
        type: CONSIGNEE_ADD_ERROR,
        consigneeAddError: bool
    }
}

const consigneeAddLoading = (bool) => {
    return {
        type: CONSIGNEE_ADD_LOADING,
        consigneeAddLoading: bool
    }
}

const consigneeAddInvalid = (bool, invalid) => {
    return {
        type: CONSIGNEE_ADD_INVALID,
        consigneeAddInvalid: bool,
        invalid
    }
}

const consigneeAddSuccess = (bool, consignee) => {
    return {
        type: CONSIGNEE_ADD_SUCCESS,
        consigneeAddSuccess: bool,
        consignee
    }
}

const consigneeDeleteError = (bool) => {
    return {
        type: CONSIGNEE_DELETE_ERROR,
        consigneeDeleteError: bool
    }
}

const consigneeDeleteLoading = (bool) => {
    return {
        type: CONSIGNEE_DELETE_LOADING,
        consigneeDeleteLoading: bool
    }
}

const consigneeDeleteSuccess = (bool, consignee) => {
    return {
        type: CONSIGNEE_DELETE_SUCCESS,
        consigneeDeleteSuccess: bool,
        consignee
    }
}

export function consigneeFetchAll() {
    return (dispatch) => {
        dispatch(consigneeGetSuccess(false, null))
        dispatch(consigneeGetError(false))
        dispatch(consigneeGetLoading(true))

        let token = AsyncStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + CONSIGNEE_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(consigneeGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(consigneeGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(consigneeGetError(true))
            })
    }
}

export function consigneeFetchSub(itemId) {
    return (dispatch) => {
        dispatch(consigneeGetSuccess(false, null))
        dispatch(consigneeGetError(false))
        dispatch(consigneeGetLoading(true))
        let token = AsyncStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    company: itemId
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + CONSIGNEE_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(consigneeGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(consigneeGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(consigneeGetError(true))
            })
    }
}

export function consigneeFetchPage(item) {
    return (dispatch) => {
        dispatch(consigneeGetSuccess(false, null))
        dispatch(consigneeGetError(false))
        dispatch(consigneeGetLoading(true))
        let token = AsyncStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(item)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + CONSIGNEE_URL + '/page', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(consigneeGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(consigneeGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(consigneeGetError(true))
            })
    }
}

export function consigneeAdd(data) {
    return (dispatch) => {
        dispatch(consigneeAddError(false))
        dispatch(consigneeAddInvalid(false, null))
        dispatch(consigneeAddSuccess(false, null))
        dispatch(consigneeAddLoading(true))



        // let myHeaders = new Headers();
        // myHeaders.append("Authorization", `Bearer ${data.tokenAuth}`)

        // let formdata = new FormData();
        // for(let key in data) {
        //     formdata.append(key, data[key])
        // }

        // const requestOptions = {
        //     method:'POST',
        //     headers: myHeaders,
        //     url: "http://103.107.103.112/api/v1/consignee/new",
        //     data: formdata
        // }

        // axios(requestOptions)
        //     .then(response => {
        //         console.warn(response.data)
        //         dispatch(consigneeAddLoading(false))
        //         if(response.data.success) {
        //             dispatch(consigneeAddSuccess(true, response.data))
        //             dispatch(consigneeAddError(false))
        //             dispatch(consigneeAddInvalid(false))
        //         } else {
        //             dispatch(consigneeAddInvalid(true))
        //         }
        //     })
        //     .catch(error => {
        //         console.warn(`Gagal Euy: ${error}`)
        //         dispatch(consigneeAddLoading(false))
        //         dispatch(consigneeAddSuccess(false, null))
        //         dispatch(consigneeAddError(true))
        //     })
/*---------------------------------------
        var myHeaders = new Headers();
        myHeaders.append("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjpudWxsLCJpYXQiOjE1Nzg1NTE4MDEsImV4cCI6MTU3ODU1NTQwMX0.69o5gxpzqnBlH0HOQIYhw4beJ8iCwDAuS_i_CceiBnY");
        myHeaders.append("Content-Type", "multipart/form-data; boundary=--------------------------780545304838552399892260");

        var formdata = new FormData();
        formdata.append("nama_perusahaan", "Consignee Mail");
        formdata.append("email", "consignee01@mail.com");
        formdata.append("alamat", "Jakarta");
        formdata.append("provinsi_id", "31");
        formdata.append("kota_id", "3171");
        formdata.append("fax", "0212");
        formdata.append("telp", "321980");
        formdata.append("siup", "321321");
        formdata.append("siup_doc", fileInput.files[0], "dokumen_siup_c.jpg");
        formdata.append("npwp", "321732091");
        formdata.append("npwp_doc", fileInput.files[0], "npwp.png");
        formdata.append("nama_pic", "PIC Consignee");
        formdata.append("email_pic", "pic@consignee.com");
        formdata.append("hp_pic", "938210");
        formdata.append("telp_pic", "382190");
        formdata.append("fax_pic", "321809");
        formdata.append("tokenAuth", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjpudWxsLCJpYXQiOjE1Nzg1NTE4MDEsImV4cCI6MTU3ODU1NTQwMX0.69o5gxpzqnBlH0HOQIYhw4beJ8iCwDAuS_i_CceiBnY");
        formdata.append("users_id", "475");

        var requestOptions = {
          method: 'POST',
          headers: myHeaders,
          body: formdata,
          redirect: 'follow'
        };

        fetch("http://103.107.103.112/api/v1/consignee/new", requestOptions)
          .then(response => response.text())
          .then(result => console.log(result))
          .catch(error => console.log('error', error));
-----------------------------*/




        let token = data.tokenAuth || null
        let config = {}
        let formData = new FormData();
        for (var key in data) {
            formData.append(key, data[key])
        }

        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`
                },
                body: formData
            }

        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + CONSIGNEE_URL + '/new', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(consigneeAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((consignee) => {
                if (consignee.success === true) {
                    dispatch(consigneeAddSuccess(true, consignee))
                } else {
                    dispatch(consigneeAddInvalid(true, consignee))
                }
            })
            .catch((err) => {
                dispatch(consigneeAddError(true))
                console.warn(err)
            })
    }
}

export function consigneeUpdate(data) {
    return (dispatch) => {
        dispatch(consigneeAddError(false))
        dispatch(consigneeAddSuccess(false, null))
        dispatch(consigneeAddLoading(true))

        let token = AsyncStorage.getItem('tolaut@token') || null
        let config = {}
        let formData = new FormData();
        for (var key in data) {
            formData.append(key, data[key])
        }

        if (token) {
            config = {
                method: 'PUT',
                headers: {
                    'Authorization': `Bearer ${token}`
                },
                body: formData
            }

        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + CONSIGNEE_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(consigneeAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((consignee) => {
                dispatch(consigneeAddSuccess(true, consignee))
            })
            .catch((err) => {
                dispatch(consigneeAddError(true))
            })
    }
}

export function handleStatus(data) {
    return (dispatch) => {
        dispatch(consigneeAddError(false))
        dispatch(consigneeAddSuccess(false, null))
        dispatch(consigneeAddLoading(true))

        let token = AsyncStorage.getItem('tolaut@token') || null
        let config = {}

        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }

        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + 'register/' + data.status, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(consigneeAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((consignee) => {
                dispatch(consigneeAddSuccess(true, consignee))
            })
            .catch((err) => {
                dispatch(consigneeAddError(true))
            })
    }
}

export function consigneeFetchOne(id) {
    return (dispatch) => {
        dispatch(consigneeViewError(false))
        dispatch(consigneeViewSuccess(false, null))
        dispatch(consigneeViewLoading(true))

        let token = AsyncStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + CONSIGNEE_URL + '/' + id, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(consigneeViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(consigneeViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(consigneeViewError(true))
            })
    }
}

export function consigneeFetchUser(id) {
    return (dispatch) => {
        dispatch(consigneeViewError(false))
        dispatch(consigneeViewSuccess(false, null))
        dispatch(consigneeViewLoading(true))

        let token = AsyncStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'id': id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + CONSIGNEE_URL + '/profile', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(consigneeViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(consigneeViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(consigneeViewError(true))
            })
    }
}

export function consigneeDelete(id) {
    return (dispatch) => {
        dispatch(consigneeViewSuccess(false, null))
        dispatch(consigneeDeleteError(false))
        dispatch(consigneeDeleteSuccess(false, null))
        dispatch(consigneeDeleteLoading(true))

        let token = AsyncStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'id': id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + CONSIGNEE_URL, config)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(consigneeDeleteLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(consigneeDeleteSuccess(true, data))
            })
            .catch((err) => {
                dispatch(consigneeDeleteError(true))
            })
    }
}