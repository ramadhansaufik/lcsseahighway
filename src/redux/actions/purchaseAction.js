import {
    doLogout
} from './authAction'
import {
    BASE_URL,
    PURCHASE_URL,

    PURCHASE_GET_ERROR,
    PURCHASE_GET_LOADING,
    PURCHASE_GET_SUCCESS,

    PURCHASE_VIEW_ERROR,
    PURCHASE_VIEW_LOADING,
    PURCHASE_VIEW_SUCCESS,

    PURCHASE_ADD_ERROR,
    PURCHASE_ADD_LOADING,
    PURCHASE_ADD_SUCCESS,

    PURCHASE_DELETE_ERROR,
    PURCHASE_DELETE_LOADING,
    PURCHASE_DELETE_SUCCESS
} from './constant'

const purchaseGetError = (bool) => {
    return {
        type: PURCHASE_GET_ERROR,
        purchaseGetError: bool
    }
}

const purchaseGetLoading = (bool) => {
    return {
        type: PURCHASE_GET_LOADING,
        purchaseGetLoading: bool
    }
}

const purchaseGetSuccess = (bool, purchases) => {
    return {
        type: PURCHASE_GET_SUCCESS,
        purchaseGetSuccess: bool,
        purchases
    }
}

const purchaseViewError = (bool) => {
    return {
        type: PURCHASE_VIEW_ERROR,
        purchaseViewError: bool
    }
}

const purchaseViewLoading = (bool) => {
    return {
        type: PURCHASE_VIEW_LOADING,
        purchaseViewLoading: bool
    }
}

const purchaseViewSuccess = (bool, purchase) => {
    return {
        type: PURCHASE_VIEW_SUCCESS,
        purchaseViewSuccess: bool,
        purchase
    }
}

const purchaseAddError = (bool) => {
    return {
        type: PURCHASE_ADD_ERROR,
        purchaseAddError: bool
    }
}

const purchaseAddLoading = (bool) => {
    return {
        type: PURCHASE_ADD_LOADING,
        purchaseAddLoading: bool
    }
}

const purchaseAddSuccess = (bool, purchase) => {
    return {
        type: PURCHASE_ADD_SUCCESS,
        purchaseAddSuccess: bool,
        purchase
    }
}

const purchaseDeleteError = (bool) => {
    return {
        type: PURCHASE_DELETE_ERROR,
        purchaseDeleteError: bool
    }
}

const purchaseDeleteLoading = (bool) => {
    return {
        type: PURCHASE_DELETE_LOADING,
        purchaseDeleteLoading: bool
    }
}

const purchaseDeleteSuccess = (bool, purchase) => {
    return {
        type: PURCHASE_DELETE_SUCCESS,
        purchaseDeleteSuccess: bool,
        purchase
    }
}

export function purchaseFetchAll() {
    return (dispatch) => {
        dispatch(purchaseAddSuccess(false, null))
        dispatch(purchaseGetSuccess(false, null))
        dispatch(purchaseGetError(false))
        dispatch(purchaseGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + PURCHASE_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(purchaseGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(purchaseGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(purchaseGetError(true))
            })
    }
}

export function purchaseFetchSub(path) {
    return (dispatch) => {
        dispatch(purchaseAddSuccess(false, null))
        dispatch(purchaseGetSuccess(false, null))
        dispatch(purchaseGetError(false))
        dispatch(purchaseGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + PURCHASE_URL + '/' + path, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }

                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(purchaseGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(purchaseGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(purchaseGetError(true))
            })
    }
}

export function purchaseFetchPage(param) {
    return (dispatch) => {
        dispatch(purchaseGetSuccess(false, null))
        dispatch(purchaseAddSuccess(false, null))
        dispatch(purchaseGetError(false))
        dispatch(purchaseGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(param)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + PURCHASE_URL + '/page', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }

                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(purchaseGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {

                dispatch(purchaseGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(purchaseGetError(true))
            })
    }
}

export function purchaseAdd(data) {
    return (dispatch) => {
        dispatch(purchaseAddError(false))
        dispatch(purchaseAddSuccess(false, null))
        dispatch(purchaseAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + PURCHASE_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }

                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(purchaseAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((purchase) => {

                if (purchase.success) {
                    dispatch(purchaseAddSuccess(true, purchase))
                } else {
                    dispatch(purchaseAddError(true))
                }
            })
            .catch((err) => {
                dispatch(purchaseAddError(true))
            })
    }
}

export function purchaseUpdate(data) {
    return (dispatch) => {
        dispatch(purchaseAddError(false))
        dispatch(purchaseAddSuccess(false, null))
        dispatch(purchaseAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'PUT',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + PURCHASE_URL, config)
            .then((response) => {

                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(purchaseAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((purchase) => {

                if (purchase.success) {
                    dispatch(purchaseAddSuccess(true, purchase))
                } else {
                    dispatch(purchaseAddError(true))
                }
            })
            .catch((err) => {
                dispatch(purchaseAddError(true))
            })
    }
}

export function purchaseConfirm(data) {

    return (dispatch) => {
        dispatch(purchaseAddError(false))
        dispatch(purchaseViewSuccess(false, null))
        dispatch(purchaseAddSuccess(false, null))
        dispatch(purchaseAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + PURCHASE_URL + '/approve', config)
            .then((response) => {

                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(purchaseAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((purchase) => {
                if (purchase.status == 200) {
                    dispatch(purchaseAddSuccess(true, purchase))
                } else {
                    dispatch(purchaseAddError(true))
                }
            })
            .catch((err) => {
                dispatch(purchaseAddError(true))
            })
    }
}

export function purchaseFetchOne(id) {
    return (dispatch) => {
        dispatch(purchaseAddSuccess(false, null))
        dispatch(purchaseViewError(false))
        dispatch(purchaseViewSuccess(false, null))
        dispatch(purchaseViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + PURCHASE_URL + '/' + id, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(purchaseViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(purchaseViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(purchaseViewError(true))
            })
    }
}

export function purchaseDelete(id) {
    return (dispatch) => {
        dispatch(purchaseViewSuccess(false, null))
        dispatch(purchaseDeleteError(false))
        dispatch(purchaseDeleteSuccess(false, null))
        dispatch(purchaseDeleteLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'id': id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + PURCHASE_URL, config)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(purchaseDeleteLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(purchaseDeleteSuccess(true, data))
            })
            .catch((err) => {
                dispatch(purchaseDeleteError(true))
            })
    }
}