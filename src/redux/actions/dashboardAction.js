import {
    doLogout
} from './authAction'
import {
    BASE_URL,
    CONSIGNEE_STATISTIC_URL,
    SUPPLIER_STATISTIC_URL,
    SHIPPER_STATISTIC_URL,
    RESELLER_STATISTIC_URL,
    OPERATOR_STATISTIC_URL,
    REGULATOR_STATISTIC_URL,
    VESSEL_TRACKING_URL,

    CONSIGNEE_STTC_ALL_ERROR,
    CONSIGNEE_STTC_ALL_LOADING,
    CONSIGNEE_STTC_ALL_SUCCESS,

    CONSIGNEE_STTC_ACTIVE_ERROR,
    CONSIGNEE_STTC_ACTIVE_LOADING,
    CONSIGNEE_STTC_ACTIVE_SUCCESS,

    SUPPLIER_STTC_ALL_ERROR,
    SUPPLIER_STTC_ALL_LOADING,
    SUPPLIER_STTC_ALL_SUCCESS,

    SUPPLIER_STTC_ACTIVE_ERROR,
    SUPPLIER_STTC_ACTIVE_LOADING,
    SUPPLIER_STTC_ACTIVE_SUCCESS,

    SHIPPER_STTC_ALL_ERROR,
    SHIPPER_STTC_ALL_LOADING,
    SHIPPER_STTC_ALL_SUCCESS,

    SHIPPER_STTC_ACTIVE_ERROR,
    SHIPPER_STTC_ACTIVE_LOADING,
    SHIPPER_STTC_ACTIVE_SUCCESS,

    RESELLER_STTC_ALL_ERROR,
    RESELLER_STTC_ALL_LOADING,
    RESELLER_STTC_ALL_SUCCESS,

    RESELLER_STTC_ACTIVE_ERROR,
    RESELLER_STTC_ACTIVE_LOADING,
    RESELLER_STTC_ACTIVE_SUCCESS,

    OPERATOR_STTC_ALL_ERROR,
    OPERATOR_STTC_ALL_LOADING,
    OPERATOR_STTC_ALL_SUCCESS,

    OPERATOR_STTC_ACTIVE_ERROR,
    OPERATOR_STTC_ACTIVE_LOADING,
    OPERATOR_STTC_ACTIVE_SUCCESS,

    OPERATOR_NAME_ERROR,
    OPERATOR_NAME_LOADING,
    OPERATOR_NAME_SUCCESS,

    REGULATOR_STTC_ALL_ERROR,
    REGULATOR_STTC_ALL_LOADING,
    REGULATOR_STTC_ALL_SUCCESS,

    REGULATOR_STTC_ACTIVE_ERROR,
    REGULATOR_STTC_ACTIVE_LOADING,
    REGULATOR_STTC_ACTIVE_SUCCESS,

    VESSEL_TRACKING_GET_ERROR,
    VESSEL_TRACKING_GET_LOADING,
    VESSEL_TRACKING_GET_SUCCESS,
    VESSEL_TRACKING_VIEW_ERROR,
    VESSEL_TRACKING_VIEW_LOADING,
    VESSEL_TRACKING_VIEW_SUCCESS,

    ORDER_PER_JENIS_PRIORITAS_URL,
    ORDER_PER_JENIS_PRIORITAS_GET_LOADING,
    ORDER_PER_JENIS_PRIORITAS_GET_ERROR,
    ORDER_PER_JENIS_PRIORITAS_GET_SUCCESS,

    MUATAN_TERBANYAK_URL,
    MUATAN_TERBANYAK_GET_LOADING,
    MUATAN_TERBANYAK_GET_ERROR,
    MUATAN_TERBANYAK_GET_SUCCESS,

    CONTAINER_PER_TAHUN_URL,
    CONTAINER_PER_TAHUN_GET_LOADING,
    CONTAINER_PER_TAHUN_GET_ERROR,
    CONTAINER_PER_TAHUN_GET_SUCCESS,

    MUATAN_PER_WILAYAH_URL,
    MUATAN_PER_WILAYAH_GET_LOADING,
    MUATAN_PER_WILAYAH_GET_ERROR,
    MUATAN_PER_WILAYAH_GET_SUCCESS,

    ORDER_PAID_URL,
    ORDER_PAID_GET_LOADING,
    ORDER_PAID_GET_ERROR,
    ORDER_PAID_GET_SUCCESS,

    ORDER_UNPAID_URL,
    ORDER_UNPAID_GET_LOADING,
    ORDER_UNPAID_GET_ERROR,
    ORDER_UNPAID_GET_SUCCESS,

    ORDER_DENIED_URL,
    ORDER_DENIED_GET_LOADING,
    ORDER_DENIED_GET_ERROR,
    ORDER_DENIED_GET_SUCCESS,

    ALL_ORDER_URL,
    ALL_ORDER_GET_LOADING,
    ALL_ORDER_GET_ERROR,
    ALL_ORDER_GET_SUCCESS,

    ORDER_PER_BULAN_URL,
    ORDER_PER_BULAN_GET_LOADING,
    ORDER_PER_BULAN_GET_ERROR,
    ORDER_PER_BULAN_GET_SUCCESS,

    MUATAN_PER_OPERATOR_URL,
    MUATAN_PER_OPERATOR_GET_LOADING,
    MUATAN_PER_OPERATOR_GET_ERROR,
    MUATAN_PER_OPERATOR_GET_SUCCESS,

    PO_URL,
    PO_GET_LOADING,
    PO_GET_ERROR,
    PO_GET_SUCCESS,

    DO_URL,
    DO_GET_LOADING,
    DO_GET_ERROR,
    DO_GET_SUCCESS,

    SISA_QUOTA_TRAYEK_URL,
    SISA_QUOTA_TRAYEK_GET_LOADING,
    SISA_QUOTA_TRAYEK_GET_ERROR,
    SISA_QUOTA_TRAYEK_GET_SUCCESS,

    VOYAGE_PER_TRAYEK_URL,
    VOYAGE_PER_TRAYEK_GET_LOADING,
    VOYAGE_PER_TRAYEK_GET_ERROR,
    VOYAGE_PER_TRAYEK_GET_SUCCESS,

    DISPARITAS_HARGA_URL,
    DISPARITAS_HARGA_GET_LOADING,
    DISPARITAS_HARGA_GET_ERROR,
    DISPARITAS_HARGA_GET_SUCCESS,

    REALISASI_URL,
    REALISASI_GET_LOADING,
    REALISASI_GET_ERROR,
    REALISASI_GET_SUCCESS,

    WAKTU_TEMPUH_URL,
    WAKTU_TEMPUH_GET_LOADING,
    WAKTU_TEMPUH_GET_ERROR,
    WAKTU_TEMPUH_GET_SUCCESS,

    JADWAL_KAPAL_URL,
    JADWAL_KAPAL_GET_LOADING,
    JADWAL_KAPAL_GET_ERROR,
    JADWAL_KAPAL_GET_SUCCESS,

    BOOKING_VESSEL_URL,
    BOOKING_VESSEL_GET_LOADING,
    BOOKING_VESSEL_GET_ERROR,
    BOOKING_VESSEL_GET_SUCCESS,

    DISTRIBUSI_BARANG_URL,
    DISTRIBUSI_BARANG_GET_LOADING,
    DISTRIBUSI_BARANG_GET_ERROR,
    DISTRIBUSI_BARANG_GET_SUCCESS,
} from './constant'

const consigneeSttcAllError = (bool) => {
    return {
        type: CONSIGNEE_STTC_ALL_ERROR,
        consigneeSttcAllError: bool
    }
}

const consigneeSttcAllLoading = (bool) => {
    return {
        type: CONSIGNEE_STTC_ALL_LOADING,
        consigneeSttcAllLoading: bool
    }
}

const consigneeSttcAllSuccess = (bool, consigneeAll) => {
    return {
        type: CONSIGNEE_STTC_ALL_SUCCESS,
        consigneeSttcAllSuccess: bool,
        consigneeAll
    }
}

const consigneeSttcActiveError = (bool) => {
    return {
        type: CONSIGNEE_STTC_ACTIVE_ERROR,
        consigneeSttcActiveError: bool
    }
}

const consigneeSttcActiveLoading = (bool) => {
    return {
        type: CONSIGNEE_STTC_ACTIVE_LOADING,
        consigneeSttcActiveLoading: bool
    }
}

const consigneeSttcActiveSuccess = (bool, consigneeActive) => {
    return {
        type: CONSIGNEE_STTC_ACTIVE_SUCCESS,
        consigneeSttcActiveSuccess: bool,
        consigneeActive
    }
}

const supplierSttcAllError = (bool) => {
    return {
        type: SUPPLIER_STTC_ALL_ERROR,
        supplierSttcAllError: bool
    }
}

const supplierSttcAllLoading = (bool) => {
    return {
        type: SUPPLIER_STTC_ALL_LOADING,
        supplierSttcAllLoading: bool
    }
}

const supplierSttcAllSuccess = (bool, supplierAll) => {
    return {
        type: SUPPLIER_STTC_ALL_SUCCESS,
        supplierSttcAllSuccess: bool,
        supplierAll
    }
}

const supplierSttcActiveError = (bool) => {
    return {
        type: SUPPLIER_STTC_ACTIVE_ERROR,
        supplierSttcActiveError: bool
    }
}

const supplierSttcActiveLoading = (bool) => {
    return {
        type: SUPPLIER_STTC_ACTIVE_LOADING,
        supplierSttcActiveLoading: bool
    }
}

const supplierSttcActiveSuccess = (bool, supplierActive) => {
    return {
        type: SUPPLIER_STTC_ACTIVE_SUCCESS,
        supplierSttcActiveSuccess: bool,
        supplierActive
    }
}

const shipperSttcAllError = (bool) => {
    return {
        type: SHIPPER_STTC_ALL_ERROR,
        shipperSttcAllError: bool
    }
}

const shipperSttcAllLoading = (bool) => {
    return {
        type: SHIPPER_STTC_ALL_LOADING,
        shipperSttcAllLoading: bool
    }
}

const shipperSttcAllSuccess = (bool, shipperAll) => {
    return {
        type: SHIPPER_STTC_ALL_SUCCESS,
        shipperSttcAllSuccess: bool,
        shipperAll
    }
}

const shipperSttcActiveError = (bool) => {
    return {
        type: SHIPPER_STTC_ACTIVE_ERROR,
        shipperSttcActiveError: bool
    }
}

const shipperSttcActiveLoading = (bool) => {
    return {
        type: SHIPPER_STTC_ACTIVE_LOADING,
        shipperSttcActiveLoading: bool
    }
}

const shipperSttcActiveSuccess = (bool, shipperActive) => {
    return {
        type: SHIPPER_STTC_ACTIVE_SUCCESS,
        shipperSttcActiveSuccess: bool,
        shipperActive
    }
}

const resellerSttcAllError = (bool) => {
    return {
        type: RESELLER_STTC_ALL_ERROR,
        resellerSttcAllError: bool
    }
}

const resellerSttcAllLoading = (bool) => {
    return {
        type: RESELLER_STTC_ALL_LOADING,
        resellerSttcAllLoading: bool
    }
}

const resellerSttcAllSuccess = (bool, resellerAll) => {
    return {
        type: RESELLER_STTC_ALL_SUCCESS,
        resellerSttcAllSuccess: bool,
        resellerAll
    }
}

const resellerSttcActiveError = (bool) => {
    return {
        type: RESELLER_STTC_ACTIVE_ERROR,
        resellerSttcActiveError: bool
    }
}

const resellerSttcActiveLoading = (bool) => {
    return {
        type: RESELLER_STTC_ACTIVE_LOADING,
        resellerSttcActiveLoading: bool
    }
}

const resellerSttcActiveSuccess = (bool, resellerActive) => {
    return {
        type: RESELLER_STTC_ACTIVE_SUCCESS,
        resellerSttcActiveSuccess: bool,
        resellerActive
    }
}

const operatorSttcAllError = (bool) => {
    return {
        type: OPERATOR_STTC_ALL_ERROR,
        operatorSttcAllError: bool
    }
}

const operatorSttcAllLoading = (bool) => {
    return {
        type: OPERATOR_STTC_ALL_LOADING,
        operatorSttcAllLoading: bool
    }
}

const operatorSttcAllSuccess = (bool, operatorAll) => {
    return {
        type: OPERATOR_STTC_ALL_SUCCESS,
        operatorSttcAllSuccess: bool,
        operatorAll
    }
}

const operatorSttcActiveError = (bool) => {
    return {
        type: OPERATOR_STTC_ACTIVE_ERROR,
        operatorSttcActiveError: bool
    }
}

const operatorSttcActiveLoading = (bool) => {
    return {
        type: OPERATOR_STTC_ACTIVE_LOADING,
        operatorSttcActiveLoading: bool
    }
}

const operatorSttcActiveSuccess = (bool, operatorActive) => {
    return {
        type: OPERATOR_STTC_ACTIVE_SUCCESS,
        operatorSttcActiveSuccess: bool,
        operatorActive
    }
}

const operatorNameError = (bool) => {
    return {
        type: OPERATOR_NAME_ERROR,
        operatorNameError: bool
    }
}

const operatorNameLoading = (bool) => {
    return {
        type: OPERATOR_NAME_LOADING,
        operatorNameLoading: bool
    }
}

const operatorNameSuccess = (bool, operatorName) => {
    return {
        type: OPERATOR_NAME_SUCCESS,
        operatorNameSuccess: bool,
        operatorName
    }
}

const regulatorSttcAllError = (bool) => {
    return {
        type: REGULATOR_STTC_ALL_ERROR,
        regulatorSttcAllError: bool
    }
}

const regulatorSttcAllLoading = (bool) => {
    return {
        type: REGULATOR_STTC_ALL_LOADING,
        regulatorSttcAllLoading: bool
    }
}

const regulatorSttcAllSuccess = (bool, regulatorAll) => {
    return {
        type: REGULATOR_STTC_ALL_SUCCESS,
        regulatorSttcAllSuccess: bool,
        regulatorAll
    }
}

const regulatorSttcActiveError = (bool) => {
    return {
        type: REGULATOR_STTC_ACTIVE_ERROR,
        regulatorSttcActiveError: bool
    }
}

const regulatorSttcActiveLoading = (bool) => {
    return {
        type: REGULATOR_STTC_ACTIVE_LOADING,
        regulatorSttcActiveLoading: bool
    }
}

const regulatorSttcActiveSuccess = (bool, regulatorActive) => {
    return {
        type: REGULATOR_STTC_ACTIVE_SUCCESS,
        regulatorSttcActiveSuccess: bool,
        regulatorActive
    }
}

export function consigneeStatisticAll() {
    return (dispatch) => {
        dispatch(consigneeSttcAllSuccess(false, null))
        dispatch(consigneeSttcAllError(false))
        dispatch(consigneeSttcAllLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + CONSIGNEE_STATISTIC_URL + '/all', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(consigneeSttcAllLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(consigneeSttcAllSuccess(true, data))
            })
            .catch((err) => {
                dispatch(consigneeSttcAllError(true))
            })
    }
}

export function consigneeStatisticActive() {
    return (dispatch) => {
        dispatch(consigneeSttcActiveSuccess(false, null))
        dispatch(consigneeSttcActiveError(false))
        dispatch(consigneeSttcActiveLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + CONSIGNEE_STATISTIC_URL + '/active', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(consigneeSttcActiveLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(consigneeSttcActiveSuccess(true, data))
            })
            .catch((err) => {
                dispatch(consigneeSttcActiveError(true))
            })
    }
}

export function supplierStatisticAll() {
    return (dispatch) => {
        dispatch(supplierSttcAllSuccess(false, null))
        dispatch(supplierSttcAllError(false))
        dispatch(supplierSttcAllLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + SUPPLIER_STATISTIC_URL + '/all', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(supplierSttcAllLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(supplierSttcAllSuccess(true, data))
            })
            .catch((err) => {
                dispatch(supplierSttcAllError(true))
            })
    }
}

export function supplierStatisticActive() {
    return (dispatch) => {
        dispatch(supplierSttcActiveSuccess(false, null))
        dispatch(supplierSttcActiveError(false))
        dispatch(supplierSttcActiveLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + SUPPLIER_STATISTIC_URL + '/active', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(supplierSttcActiveLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(supplierSttcActiveSuccess(true, data))
            })
            .catch((err) => {
                dispatch(supplierSttcActiveError(true))
            })
    }
}

export function shipperStatisticAll() {
    return (dispatch) => {
        dispatch(shipperSttcAllSuccess(false, null))
        dispatch(shipperSttcAllError(false))
        dispatch(shipperSttcAllLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + SHIPPER_STATISTIC_URL + '/all', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(shipperSttcAllLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(shipperSttcAllSuccess(true, data))
            })
            .catch((err) => {
                dispatch(shipperSttcAllError(true))
            })
    }
}

export function shipperStatisticActive() {
    return (dispatch) => {
        dispatch(shipperSttcActiveSuccess(false, null))
        dispatch(shipperSttcActiveError(false))
        dispatch(shipperSttcActiveLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + SHIPPER_STATISTIC_URL + '/active', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(shipperSttcActiveLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(shipperSttcActiveSuccess(true, data))
            })
            .catch((err) => {
                dispatch(shipperSttcActiveError(true))
            })
    }
}

export function resellerStatisticAll() {
    return (dispatch) => {
        dispatch(resellerSttcAllSuccess(false, null))
        dispatch(resellerSttcAllError(false))
        dispatch(resellerSttcAllLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + RESELLER_STATISTIC_URL + '/all', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(resellerSttcAllLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(resellerSttcAllSuccess(true, data))
            })
            .catch((err) => {
                dispatch(resellerSttcAllError(true))
            })
    }
}

export function resellerStatisticActive() {
    return (dispatch) => {
        dispatch(resellerSttcActiveSuccess(false, null))
        dispatch(resellerSttcActiveError(false))
        dispatch(resellerSttcActiveLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + RESELLER_STATISTIC_URL + '/active', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(resellerSttcActiveLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(resellerSttcActiveSuccess(true, data))
            })
            .catch((err) => {
                dispatch(resellerSttcActiveError(true))
            })
    }
}

export function operatorStatisticAll() {
    return (dispatch) => {
        dispatch(operatorSttcAllSuccess(false, null))
        dispatch(operatorSttcAllError(false))
        dispatch(operatorSttcAllLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + OPERATOR_STATISTIC_URL + '/all', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(operatorSttcAllLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(operatorSttcAllSuccess(true, data))
            })
            .catch((err) => {
                dispatch(operatorSttcAllError(true))
            })
    }
}

export function operatorStatisticActive() {
    return (dispatch) => {
        dispatch(operatorSttcActiveSuccess(false, null))
        dispatch(operatorSttcActiveError(false))
        dispatch(operatorSttcActiveLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + OPERATOR_STATISTIC_URL + '/active', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(operatorSttcActiveLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(operatorSttcActiveSuccess(true, data))
            })
            .catch((err) => {
                dispatch(operatorSttcActiveError(true))
            })
    }
}

export function getOperatorName(data) {
    return (dispatch) => {
        dispatch(operatorNameSuccess(false, null))
        dispatch(operatorNameError(false))
        dispatch(operatorNameLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + OPERATOR_STATISTIC_URL + '/name', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(operatorNameLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(operatorNameSuccess(true, data))
            })
            .catch((err) => {
                dispatch(operatorNameError(true))
            })
    }
}

export function regulatorStatisticAll() {
    return (dispatch) => {
        dispatch(regulatorSttcAllSuccess(false, null))
        dispatch(regulatorSttcAllError(false))
        dispatch(regulatorSttcAllLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + REGULATOR_STATISTIC_URL + '/all', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(regulatorSttcAllLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(regulatorSttcAllSuccess(true, data))
            })
            .catch((err) => {
                dispatch(regulatorSttcAllError(true))
            })
    }
}

export function regulatorStatisticActive() {
    return (dispatch) => {
        dispatch(regulatorSttcActiveSuccess(false, null))
        dispatch(regulatorSttcActiveError(false))
        dispatch(regulatorSttcActiveLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + REGULATOR_STATISTIC_URL + '/active', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(regulatorSttcActiveLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(regulatorSttcActiveSuccess(true, data))
            })
            .catch((err) => {
                dispatch(regulatorSttcActiveError(true))
            })
    }
}


// Vessel Tracking
const vesselTrackingGetError = (bool) => {
    return {
        type: VESSEL_TRACKING_GET_ERROR,
        vesselTrackingGetError: bool
    }
}

const vesselTrackingGetLoading = (bool) => {
    return {
        type: VESSEL_TRACKING_GET_LOADING,
        vesselTrackingGetLoading: bool
    }
}

const vesselTrackingGetSuccess = (bool, vesselTrackingGet) => {
    return {
        type: VESSEL_TRACKING_GET_SUCCESS,
        vesselTrackingGetSuccess: bool,
        vesselTrackingGet
    }
}

export function vesselTrackingGet() {
    return (dispatch) => {
        dispatch(vesselTrackingGetSuccess(false, null))
        dispatch(vesselTrackingGetError(false))
        dispatch(vesselTrackingGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + VESSEL_TRACKING_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(vesselTrackingGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(vesselTrackingGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(vesselTrackingGetError(true))
            })
    }
}

// Vessel Tracking View
const vesselTrackingViewError = (bool) => {
    return {
        type: VESSEL_TRACKING_VIEW_ERROR,
        vesselTrackingViewError: bool
    }
}

const vesselTrackingViewLoading = (bool) => {
    return {
        type: VESSEL_TRACKING_VIEW_LOADING,
        vesselTrackingViewLoading: bool
    }
}

const vesselTrackingViewSuccess = (bool, vesselTrackingView) => {
    return {
        type: VESSEL_TRACKING_VIEW_SUCCESS,
        vesselTrackingViewSuccess: bool,
        vesselTrackingView
    }
}

export function vesselTrackingView(data) {
    return (dispatch) => {
        dispatch(vesselTrackingViewSuccess(false, null))
        dispatch(vesselTrackingViewError(false))
        dispatch(vesselTrackingViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + VESSEL_TRACKING_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(vesselTrackingViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(vesselTrackingViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(vesselTrackingViewError(true))
            })
    }
}

// order per jenis prioritas
const orderPerJenisPrioritasLoading = (bool) => {
    return {
        type: ORDER_PER_JENIS_PRIORITAS_GET_LOADING,
        orderPerJenisPrioritasLoading: bool
    }
}

const orderPerJenisPrioritasError = (bool) => {
    return {
        type: ORDER_PER_JENIS_PRIORITAS_GET_ERROR,
        orderPerJenisPrioritasError: bool
    }
}

const orderPerJenisPrioritasSuccess = (bool, orderPerJenisPrioritasData) => {
    return {
        type: ORDER_PER_JENIS_PRIORITAS_GET_SUCCESS,
        orderPerJenisPrioritasSuccess: bool,
        orderPerJenisPrioritasData
    }
}

// order per jenis prioritas
export function orderPerJenisPrioritas(data) {
    return (dispatch) => {
        dispatch(orderPerJenisPrioritasLoading(true))
        dispatch(orderPerJenisPrioritasError(false))
        dispatch(orderPerJenisPrioritasSuccess(false, null))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }

        fetch(BASE_URL + ORDER_PER_JENIS_PRIORITAS_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }

                if (!response.ok) {
                    throw Error(response.statusText)
                }

                dispatch(orderPerJenisPrioritasLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(orderPerJenisPrioritasSuccess(true, data))
            })
            .catch((err) => {
                dispatch(orderPerJenisPrioritasError(true))
            })
    }
}


// lima muatan terbanyak
const muatanTerbanyakLoading = (bool) => {
    return {
        type: MUATAN_TERBANYAK_GET_LOADING,
        muatanTerbanyakLoading: bool
    }
}

const muatanTerbanyakError = (bool) => {
    return {
        type: MUATAN_TERBANYAK_GET_ERROR,
        muatanTerbanyakError: bool
    }
}

const muatanTerbanyakSuccess = (bool, muatanTerbanyakData) => {
    return {
        type: MUATAN_TERBANYAK_GET_SUCCESS,
        muatanTerbanyakSuccess: bool,
        muatanTerbanyakData
    }
}

// lima muatan terbanyak
export function muatanTerbanyak(data) {
    return (dispatch) => {
        dispatch(muatanTerbanyakLoading(true))
        dispatch(muatanTerbanyakError(false))
        dispatch(muatanTerbanyakSuccess(false, null))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }

        fetch(BASE_URL + MUATAN_TERBANYAK_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }

                if (!response.ok) {
                    throw Error(response.statusText)
                }

                dispatch(muatanTerbanyakLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(muatanTerbanyakSuccess(true, data))
            })
            .catch((err) => {
                dispatch(muatanTerbanyakError(true))
            })
    }
}

// container pertahun
const containerPerTahunLoading = (bool) => {
    return {
        type: CONTAINER_PER_TAHUN_GET_LOADING,
        containerPerTahunLoading: bool
    }
}

const containerPerTahunError = (bool) => {
    return {
        type: CONTAINER_PER_TAHUN_GET_ERROR,
        containerPerTahunError: bool
    }
}

const containerPerTahunSuccess = (bool, containerPerTahunData) => {
    return {
        type: CONTAINER_PER_TAHUN_GET_SUCCESS,
        containerPerTahunSuccess: bool,
        containerPerTahunData
    }
}

// container pertahun
export function containerPerTahun(data) {
    return (dispatch) => {
        dispatch(containerPerTahunLoading(true))
        dispatch(containerPerTahunError(false))
        dispatch(containerPerTahunSuccess(false, null))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }

        fetch(BASE_URL + CONTAINER_PER_TAHUN_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }

                if (!response.ok) {
                    throw Error(response.statusText)
                }

                dispatch(containerPerTahunLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(containerPerTahunSuccess(true, data))
            })
            .catch((err) => {
                dispatch(containerPerTahunError(true))
            })
    }
}

// muatan perwilayah
const muatanPerWilayahLoading = (bool) => {
    return {
        type: MUATAN_PER_WILAYAH_GET_LOADING,
        muatanPerWilayahLoading: bool
    }
}

const muatanPerWilayahError = (bool) => {
    return {
        type: MUATAN_PER_WILAYAH_GET_ERROR,
        muatanPerWilayahError: bool
    }
}

const muatanPerWilayahSuccess = (bool, muatanPerWilayahData) => {
    return {
        type: MUATAN_PER_WILAYAH_GET_SUCCESS,
        muatanPerWilayahSuccess: bool,
        muatanPerWilayahData
    }
}

// muatan perwilayah
export function muatanPerWilayah(data) {
    return (dispatch) => {
        dispatch(muatanPerWilayahLoading(true))
        dispatch(muatanPerWilayahError(false))
        dispatch(muatanPerWilayahSuccess(false, null))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }

        fetch(BASE_URL + MUATAN_PER_WILAYAH_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }

                if (!response.ok) {
                    throw Error(response.statusText)
                }

                dispatch(muatanPerWilayahLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(muatanPerWilayahSuccess(true, data))
            })
            .catch((err) => {
                dispatch(muatanPerWilayahError(true))
            })
    }
}

// order paid
const orderPaidLoading = (bool) => {
    return {
        type: ORDER_PAID_GET_LOADING,
        orderPaidLoading: bool
    }
}

const orderPaidError = (bool) => {
    return {
        type: ORDER_PAID_GET_ERROR,
        orderPaidError: bool
    }
}

const orderPaidSuccess = (bool, orderPaidData) => {
    return {
        type: ORDER_PAID_GET_SUCCESS,
        orderPaidSuccess: bool,
        orderPaidData
    }
}

// order paid
export function orderPaid() {
    return (dispatch) => {
        dispatch(orderPaidLoading(true))
        dispatch(orderPaidError(false))
        dispatch(orderPaidSuccess(false, null))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }

        fetch(BASE_URL + ORDER_PAID_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }

                if (!response.ok) {
                    throw Error(response.statusText)
                }

                dispatch(orderPaidLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(orderPaidSuccess(true, data))
            })
            .catch((err) => {
                dispatch(orderPaidError(true))
            })
    }
}

// order unpaid
const orderUnpaidLoading = (bool) => {
    return {
        type: ORDER_UNPAID_GET_LOADING,
        orderUnpaidLoading: bool
    }
}

const orderUnpaidError = (bool) => {
    return {
        type: ORDER_UNPAID_GET_ERROR,
        orderUnpaidError: bool
    }
}

const orderUnpaidSuccess = (bool, orderUnpaidData) => {
    return {
        type: ORDER_UNPAID_GET_SUCCESS,
        orderUnpaidSuccess: bool,
        orderUnpaidData
    }
}

// order unpaid
export function orderUnpaid() {
    return (dispatch) => {
        dispatch(orderUnpaidLoading(true))
        dispatch(orderUnpaidError(false))
        dispatch(orderUnpaidSuccess(false, null))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }

        fetch(BASE_URL + ORDER_UNPAID_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }

                if (!response.ok) {
                    throw Error(response.statusText)
                }

                dispatch(orderUnpaidLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(orderUnpaidSuccess(true, data))
            })
            .catch((err) => {
                dispatch(orderUnpaidError(true))
            })
    }
}

// order paid
const orderDeniedLoading = (bool) => {
    return {
        type: ORDER_DENIED_GET_LOADING,
        orderDeniedLoading: bool
    }
}

const orderDeniedError = (bool) => {
    return {
        type: ORDER_DENIED_GET_ERROR,
        orderDeniedError: bool
    }
}

const orderDeniedSuccess = (bool, orderDeniedData) => {
    return {
        type: ORDER_DENIED_GET_SUCCESS,
        orderDeniedSuccess: bool,
        orderDeniedData
    }
}

// order paid
export function orderDenied() {
    return (dispatch) => {
        dispatch(orderDeniedLoading(true))
        dispatch(orderDeniedError(false))
        dispatch(orderDeniedSuccess(false, null))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }

        fetch(BASE_URL + ORDER_DENIED_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }

                if (!response.ok) {
                    throw Error(response.statusText)
                }

                dispatch(orderDeniedLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(orderDeniedSuccess(true, data))
            })
            .catch((err) => {
                dispatch(orderDeniedError(true))
            })
    }
}

// all orders
const allOrderLoading = (bool) => {
    return {
        type: ALL_ORDER_GET_LOADING,
        allOrderLoading: bool
    }
}

const allOrderError = (bool) => {
    return {
        type: ALL_ORDER_GET_ERROR,
        allOrderError: bool
    }
}

const allOrderSuccess = (bool, allOrderData) => {
    return {
        type: ALL_ORDER_GET_SUCCESS,
        allOrderSuccess: bool,
        allOrderData
    }
}

// all orders
export function allOrder() {
    return (dispatch) => {
        dispatch(allOrderLoading(true))
        dispatch(allOrderError(false))
        dispatch(allOrderSuccess(false, null))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }

        fetch(BASE_URL + ALL_ORDER_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }

                if (!response.ok) {
                    throw Error(response.statusText)
                }

                dispatch(allOrderLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(allOrderSuccess(true, data))
            })
            .catch((err) => {
                dispatch(allOrderError(true))
            })
    }
}

// order per bulan
const orderPerBulanLoading = (bool) => {
    return {
        type: ORDER_PER_BULAN_GET_LOADING,
        orderPerBulanLoading: bool
    }
}

const orderPerBulanError = (bool) => {
    return {
        type: ORDER_PER_BULAN_GET_ERROR,
        orderPerBulanError: bool
    }
}

const orderPerBulanSuccess = (bool, orderPerBulanData) => {
    return {
        type: ORDER_PER_BULAN_GET_SUCCESS,
        orderPerBulanSuccess: bool,
        orderPerBulanData
    }
}

// order per bulan
export function orderPerBulan() {
    return (dispatch) => {
        dispatch(orderPerBulanLoading(true))
        dispatch(orderPerBulanError(false))
        dispatch(orderPerBulanSuccess(false, null))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }

        fetch(BASE_URL + ORDER_PER_BULAN_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }

                if (!response.ok) {
                    throw Error(response.statusText)
                }

                dispatch(orderPerBulanLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(orderPerBulanSuccess(true, data))
            })
            .catch((err) => {
                dispatch(orderPerBulanError(true))
            })
    }
}

// muatan per operator
const muatanPerOperatorLoading = (bool) => {
    return {
        type: MUATAN_PER_OPERATOR_GET_LOADING,
        muatanPerOperatorLoading: bool
    }
}

const muatanPerOperatorError = (bool) => {
    return {
        type: MUATAN_PER_OPERATOR_GET_ERROR,
        muatanPerOperatorError: bool
    }
}

const muatanPerOperatorSuccess = (bool, muatanPerOperatorData) => {
    return {
        type: MUATAN_PER_OPERATOR_GET_SUCCESS,
        muatanPerOperatorSuccess: bool,
        muatanPerOperatorData
    }
}

// muatan per operator
export function muatanPerOperator(data) {
    return (dispatch) => {
        dispatch(muatanPerOperatorLoading(true))
        dispatch(muatanPerOperatorError(false))
        dispatch(muatanPerOperatorSuccess(false, null))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }

        fetch(BASE_URL + MUATAN_PER_OPERATOR_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }

                if (!response.ok) {
                    throw Error(response.statusText)
                }

                dispatch(muatanPerOperatorLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(muatanPerOperatorSuccess(true, data))
            })
            .catch((err) => {
                dispatch(muatanPerOperatorError(true))
            })
    }
}

// PO
const purchaseOrderLoading = (bool) => {
    return {
        type: PO_GET_LOADING,
        purchaseOrderLoading: bool
    }
}

const purchaseOrderError = (bool) => {
    return {
        type: PO_GET_ERROR,
        purchaseOrderError: bool
    }
}

const purchaseOrderSuccess = (bool, purchaseOrderData) => {
    return {
        type: PO_GET_SUCCESS,
        purchaseOrderSuccess: bool,
        purchaseOrderData
    }
}

// PO
export function purchaseOrder(data) {
    return (dispatch) => {
        dispatch(purchaseOrderLoading(true))
        dispatch(purchaseOrderError(false))
        dispatch(purchaseOrderSuccess(false, null))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }

        fetch(BASE_URL + PO_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }

                if (!response.ok) {
                    throw Error(response.statusText)
                }

                dispatch(purchaseOrderLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(purchaseOrderSuccess(true, data))
            })
            .catch((err) => {
                dispatch(purchaseOrderError(true))
            })
    }
}

// DO
const deliveryOrderLoading = (bool) => {
    return {
        type: DO_GET_LOADING,
        deliveryOrderLoading: bool
    }
}

const deliveryOrderError = (bool) => {
    return {
        type: DO_GET_ERROR,
        deliveryOrderError: bool
    }
}

const deliveryOrderSuccess = (bool, deliveryOrderData) => {
    return {
        type: DO_GET_SUCCESS,
        deliveryOrderSuccess: bool,
        deliveryOrderData
    }
}

// DO
export function deliveryOrder(data) {
    return (dispatch) => {
        dispatch(deliveryOrderLoading(true))
        dispatch(deliveryOrderError(false))
        dispatch(deliveryOrderSuccess(false, null))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }

        fetch(BASE_URL + DO_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }

                if (!response.ok) {
                    throw Error(response.statusText)
                }

                dispatch(deliveryOrderLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(deliveryOrderSuccess(true, data))
            })
            .catch((err) => {
                dispatch(deliveryOrderError(true))
            })
    }
}

//  sisa quota trayek
const sisaQuotaTrayekLoading = (bool) => {
    return {
        type: SISA_QUOTA_TRAYEK_GET_LOADING,
        sisaQuotaTrayekLoading: bool
    }
}

const sisaQuotaTrayekError = (bool) => {
    return {
        type: SISA_QUOTA_TRAYEK_GET_ERROR,
        sisaQuotaTrayekError: bool
    }
}

const sisaQuotaTrayekSuccess = (bool, sisaQuotaTrayekData) => {
    return {
        type: SISA_QUOTA_TRAYEK_GET_SUCCESS,
        sisaQuotaTrayekSuccess: bool,
        sisaQuotaTrayekData
    }
}

// sisa quota trayek
export function sisaQuotaTrayek(data) {
    return (dispatch) => {
        dispatch(sisaQuotaTrayekLoading(true))
        dispatch(sisaQuotaTrayekError(false))
        dispatch(sisaQuotaTrayekSuccess(false, null))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }

        fetch(BASE_URL + SISA_QUOTA_TRAYEK_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }

                if (!response.ok) {
                    throw Error(response.statusText)
                }

                dispatch(sisaQuotaTrayekLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(sisaQuotaTrayekSuccess(true, data))
            })
            .catch((err) => {
                dispatch(sisaQuotaTrayekError(true))
            })
    }
}

//  voyage per trayek
const voyagePerTrayekLoading = (bool) => {
    return {
        type: VOYAGE_PER_TRAYEK_GET_LOADING,
        voyagePerTrayekLoading: bool
    }
}

const voyagePerTrayekError = (bool) => {
    return {
        type: VOYAGE_PER_TRAYEK_GET_ERROR,
        voyagePerTrayekError: bool
    }
}

const voyagePerTrayekSuccess = (bool, voyagePerTrayekData) => {
    return {
        type: VOYAGE_PER_TRAYEK_GET_SUCCESS,
        voyagePerTrayekSuccess: bool,
        voyagePerTrayekData
    }
}

// voyage per trayek
export function voyagePerTrayek(data) {
    return (dispatch) => {
        dispatch(voyagePerTrayekLoading(true))
        dispatch(voyagePerTrayekError(false))
        dispatch(voyagePerTrayekSuccess(false, null))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }

        fetch(BASE_URL + VOYAGE_PER_TRAYEK_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }

                if (!response.ok) {
                    throw Error(response.statusText)
                }

                dispatch(voyagePerTrayekLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(voyagePerTrayekSuccess(true, data))
            })
            .catch((err) => {
                dispatch(voyagePerTrayekError(true))
            })
    }
}

//  disparitas harga
const disparitasHargaLoading = (bool) => {
    return {
        type: DISPARITAS_HARGA_GET_LOADING,
        disparitasHargaLoading: bool
    }
}

const disparitasHargaError = (bool) => {
    return {
        type: DISPARITAS_HARGA_GET_ERROR,
        disparitasHargaError: bool
    }
}

const disparitasHargaSuccess = (bool, disparitasHargaData) => {
    return {
        type: DISPARITAS_HARGA_GET_SUCCESS,
        disparitasHargaSuccess: bool,
        disparitasHargaData
    }
}

// disparitas harga
export function disparitasHarga(data) {
    return (dispatch) => {
        dispatch(disparitasHargaLoading(true))
        dispatch(disparitasHargaError(false))
        dispatch(disparitasHargaSuccess(false, null))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }

        fetch(BASE_URL + DISPARITAS_HARGA_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }

                if (!response.ok) {
                    throw Error(response.statusText)
                }

                dispatch(disparitasHargaLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(disparitasHargaSuccess(true, data))
            })
            .catch((err) => {
                dispatch(disparitasHargaError(true))
            })
    }
}

//  realisasi
const realisasiLoading = (bool) => {
    return {
        type: REALISASI_GET_LOADING,
        realisasiLoading: bool
    }
}

const realisasiError = (bool) => {
    return {
        type: REALISASI_GET_ERROR,
        realisasiError: bool
    }
}

const realisasiSuccess = (bool, realisasiData) => {
    return {
        type: REALISASI_GET_SUCCESS,
        realisasiSuccess: bool,
        realisasiData
    }
}

// realisasi
export function realisasi(data) {
    return (dispatch) => {
        dispatch(realisasiLoading(true))
        dispatch(realisasiError(false))
        dispatch(realisasiSuccess(false, null))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }

        fetch(BASE_URL + REALISASI_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }

                if (!response.ok) {
                    throw Error(response.statusText)
                }

                dispatch(realisasiLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(realisasiSuccess(true, data))
            })
            .catch((err) => {
                dispatch(realisasiError(true))
            })
    }
}

//  waktu tempuh
const waktuTempuhLoading = (bool) => {
    return {
        type: WAKTU_TEMPUH_GET_LOADING,
        waktuTempuhLoading: bool
    }
}

const waktuTempuhError = (bool) => {
    return {
        type: WAKTU_TEMPUH_GET_ERROR,
        waktuTempuhError: bool
    }
}

const waktuTempuhSuccess = (bool, waktuTempuhData) => {
    return {
        type: WAKTU_TEMPUH_GET_SUCCESS,
        waktuTempuhSuccess: bool,
        waktuTempuhData
    }
}

// waktu tempuh
export function waktuTempuh(data) {
    return (dispatch) => {
        dispatch(waktuTempuhLoading(true))
        dispatch(waktuTempuhError(false))
        dispatch(waktuTempuhSuccess(false, null))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }

        fetch(BASE_URL + WAKTU_TEMPUH_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }

                if (!response.ok) {
                    throw Error(response.statusText)
                }

                dispatch(waktuTempuhLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(waktuTempuhSuccess(true, data))
            })
            .catch((err) => {
                dispatch(waktuTempuhError(true))
            })
    }
}

//  jadwal kapal
const jadwalKapalLoading = (bool) => {
    return {
        type: JADWAL_KAPAL_GET_LOADING,
        jadwalKapalLoading: bool
    }
}

const jadwalKapalError = (bool) => {
    return {
        type: JADWAL_KAPAL_GET_ERROR,
        jadwalKapalError: bool
    }
}

const jadwalKapalSuccess = (bool, jadwalKapalData) => {
    return {
        type: JADWAL_KAPAL_GET_SUCCESS,
        jadwalKapalSuccess: bool,
        jadwalKapalData
    }
}

// jadwal kapal
export function jadwalKapal(data) {
    return (dispatch) => {
        dispatch(jadwalKapalLoading(true))
        dispatch(jadwalKapalError(false))
        dispatch(jadwalKapalSuccess(false, null))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }

        fetch(BASE_URL + JADWAL_KAPAL_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }

                if (!response.ok) {
                    throw Error(response.statusText)
                }

                dispatch(jadwalKapalLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(jadwalKapalSuccess(true, data))
            })
            .catch((err) => {
                dispatch(jadwalKapalError(true))
            })
    }
}

//  booking vessel
const bookingVesselLoading = (bool) => {
    return {
        type: BOOKING_VESSEL_GET_LOADING,
        bookingVesselLoading: bool
    }
}

const bookingVesselError = (bool) => {
    return {
        type: BOOKING_VESSEL_GET_ERROR,
        bookingVesselError: bool
    }
}

const bookingVesselSuccess = (bool, bookingVesselData) => {
    return {
        type: BOOKING_VESSEL_GET_SUCCESS,
        bookingVesselSuccess: bool,
        bookingVesselData
    }
}

// booking vessel
export function bookingVessel(data) {
    return (dispatch) => {
        dispatch(bookingVesselLoading(true))
        dispatch(bookingVesselError(false))
        dispatch(bookingVesselSuccess(false, null))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }

        fetch(BASE_URL + BOOKING_VESSEL_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }

                if (!response.ok) {
                    throw Error(response.statusText)
                }

                dispatch(bookingVesselLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(bookingVesselSuccess(true, data))
            })
            .catch((err) => {
                dispatch(bookingVesselError(true))
            })
    }
}

//  distribusi barang
const distribusiBarangLoading = (bool) => {
    return {
        type: DISTRIBUSI_BARANG_GET_LOADING,
        distribusiBarangLoading: bool
    }
}

const distribusiBarangError = (bool) => {
    return {
        type: DISTRIBUSI_BARANG_GET_ERROR,
        distribusiBarangError: bool
    }
}

const distribusiBarangSuccess = (bool, distribusiBarangData) => {
    return {
        type: DISTRIBUSI_BARANG_GET_SUCCESS,
        distribusiBarangSuccess: bool,
        distribusiBarangData
    }
}

// distribusi barang
export function distribusiBarang(data) {
    return (dispatch) => {
        dispatch(distribusiBarangLoading(true))
        dispatch(distribusiBarangError(false))
        dispatch(distribusiBarangSuccess(false, null))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }

        fetch(BASE_URL + DISTRIBUSI_BARANG_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }

                if (!response.ok) {
                    throw Error(response.statusText)
                }

                dispatch(distribusiBarangLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(distribusiBarangSuccess(true, data))
            })
            .catch((err) => {
                dispatch(distribusiBarangError(true))
            })
    }
}