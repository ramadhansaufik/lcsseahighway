import {
    doLogout
} from './authAction'
import {
    BASE_URL,
    RESELLER_URL,

    RESELLER_GET_ERROR,
    RESELLER_GET_LOADING,
    RESELLER_GET_SUCCESS,

    RESELLER_VIEW_ERROR,
    RESELLER_VIEW_LOADING,
    RESELLER_VIEW_SUCCESS,

    RESELLER_ADD_ERROR,
    RESELLER_ADD_LOADING,
    RESELLER_ADD_INVALID,
    RESELLER_ADD_SUCCESS,

    RESELLER_DELETE_ERROR,
    RESELLER_DELETE_LOADING,
    RESELLER_DELETE_SUCCESS
} from './constant'

const resellerGetError = (bool) => {
    return {
        type: RESELLER_GET_ERROR,
        resellerGetError: bool
    }
}

const resellerGetLoading = (bool) => {
    return {
        type: RESELLER_GET_LOADING,
        resellerGetLoading: bool
    }
}

const resellerGetSuccess = (bool, resellers) => {
    return {
        type: RESELLER_GET_SUCCESS,
        resellerGetSuccess: bool,
        resellers
    }
}

const resellerViewError = (bool) => {
    return {
        type: RESELLER_VIEW_ERROR,
        resellerViewError: bool
    }
}

const resellerViewLoading = (bool) => {
    return {
        type: RESELLER_VIEW_LOADING,
        resellerViewLoading: bool
    }
}

const resellerViewSuccess = (bool, reseller) => {
    return {
        type: RESELLER_VIEW_SUCCESS,
        resellerViewSuccess: bool,
        reseller
    }
}

const resellerAddError = (bool) => {
    return {
        type: RESELLER_ADD_ERROR,
        resellerAddError: bool
    }
}

const resellerAddLoading = (bool) => {
    return {
        type: RESELLER_ADD_LOADING,
        resellerAddLoading: bool
    }
}

const resellerAddInvalid = (bool, invalid) => {
    return {
        type: RESELLER_ADD_INVALID,
        resellerAddInvalid: bool,
        invalid
    }
}

const resellerAddSuccess = (bool, reseller) => {
    return {
        type: RESELLER_ADD_SUCCESS,
        resellerAddSuccess: bool,
        reseller
    }
}

const resellerDeleteError = (bool) => {
    return {
        type: RESELLER_DELETE_ERROR,
        resellerDeleteError: bool
    }
}

const resellerDeleteLoading = (bool) => {
    return {
        type: RESELLER_DELETE_LOADING,
        resellerDeleteLoading: bool
    }
}

const resellerDeleteSuccess = (bool, reseller) => {
    return {
        type: RESELLER_DELETE_SUCCESS,
        resellerDeleteSuccess: bool,
        reseller
    }
}

export function resellerFetchUser(id) {
    return (dispatch) => {
        dispatch(resellerViewError(false))
        dispatch(resellerViewSuccess(false, null))
        dispatch(resellerViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'id': id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + RESELLER_URL + '/profile', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(resellerViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(resellerViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(resellerViewError(true))
            })
    }
}

export function resellerFetchAll(tokenAuth) {
    return (dispatch) => {
        dispatch(resellerGetSuccess(false, null))
        dispatch(resellerGetError(false))
        dispatch(resellerGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + RESELLER_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(resellerGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {

                dispatch(resellerGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(resellerGetError(true))
            })
    }
}

export function resellerFetchPage(item) {
    return (dispatch) => {
        dispatch(resellerGetSuccess(false, null))
        dispatch(resellerGetError(false))
        dispatch(resellerGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(item)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + RESELLER_URL + '/page', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(resellerGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {

                dispatch(resellerGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(resellerGetError(true))
            })
    }
}

export function handleStatus(data) {
    return (dispatch) => {
        dispatch(resellerAddError(false))
        dispatch(resellerAddSuccess(false, null))
        dispatch(resellerAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}

        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }

        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + 'register/' + data.status, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(resellerAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((reseller) => {
                dispatch(resellerAddSuccess(true, reseller))
            })
            .catch((err) => {
                dispatch(resellerAddError(true))
            })
    }
}

export function resellerFetchOne(id) {
    return (dispatch) => {
        dispatch(resellerViewError(false))
        dispatch(resellerViewSuccess(false, null))
        dispatch(resellerViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + RESELLER_URL + '/' + id, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(resellerViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(resellerViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(resellerViewError(true))
            })
    }
}

export function resellerDelete(id) {
    return (dispatch) => {
        dispatch(resellerViewSuccess(false, null))
        dispatch(resellerDeleteError(false))
        dispatch(resellerDeleteSuccess(false, null))
        dispatch(resellerDeleteLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'id': id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + RESELLER_URL, config)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(resellerDeleteLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(resellerDeleteSuccess(true, data))
            })
            .catch((err) => {
                dispatch(resellerDeleteError(true))
            })
    }
}

export function resellerAdd(data) {
    return (dispatch) => {
        dispatch(resellerAddError(false))
        dispatch(resellerAddSuccess(false, null))
        dispatch(resellerAddInvalid(false, null))
        dispatch(resellerAddLoading(true))

        let token = data.tokenAuth || null
        let config = {}
        let formData = new FormData();
        for (var key in data) {
            formData.append(key, data[key])
        }

        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`
                },
                body: formData
            }

        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + RESELLER_URL + '/new', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(resellerAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((reseller) => {
                if (reseller.success) {
                    dispatch(resellerAddSuccess(true, reseller))
                } else {
                    dispatch(resellerAddInvalid(true, reseller))
                }
            })
            .catch((err) => {
                dispatch(resellerAddError(true))
            })
    }
}

export function resellerUpdate(data) {
    return (dispatch) => {
        dispatch(resellerAddError(false))
        dispatch(resellerAddSuccess(false, null))
        dispatch(resellerAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        let formData = new FormData();
        for (var key in data) {
            formData.append(key, data[key])
        }

        if (token) {
            config = {
                method: 'PUT',
                headers: {
                    'Authorization': `Bearer ${token}`
                },
                body: formData
            }

        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + RESELLER_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(resellerAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((reseller) => {
                dispatch(resellerAddSuccess(true, reseller))
            })
            .catch((err) => {
                dispatch(resellerAddError(true))
            })
    }
}