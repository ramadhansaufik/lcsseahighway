import {
    doLogout
} from './authAction'
import {
    BASE_URL,
    TRACKING_DATA_URL,

    TRACKING_DATA_GET_ERROR,
    TRACKING_DATA_GET_LOADING,
    TRACKING_DATA_GET_SUCCESS,
} from './constant'

const trackingDataGetError = (bool) => {
    return {
        type: TRACKING_DATA_GET_ERROR,
        trackingDataGetError: bool
    }
}

const trackingDataGetLoading = (bool) => {
    return {
        type: TRACKING_DATA_GET_LOADING,
        trackingDataGetLoading: bool
    }
}

const trackingDataGetSuccess = (bool, trackingData) => {
    return {
        type: TRACKING_DATA_GET_SUCCESS,
        trackingDataGetSuccess: bool,
        trackingData
    }
}

export function trackingDataGet(data) {
    return (dispatch) => {
        dispatch(trackingDataGetSuccess(false, null))
        dispatch(trackingDataGetError(false))
        dispatch(trackingDataGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + TRACKING_DATA_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(trackingDataGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(trackingDataGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(trackingDataGetError(true))
            })
    }
}