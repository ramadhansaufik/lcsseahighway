import {
    doLogout
} from './authAction'
import {
    BASE_URL,
    SHIPPER_URL,

    SHIPPER_GET_ERROR,
    SHIPPER_GET_LOADING,
    SHIPPER_GET_SUCCESS,

    SHIPPER_VIEW_ERROR,
    SHIPPER_VIEW_LOADING,
    SHIPPER_VIEW_SUCCESS,

    SHIPPER_ADD_ERROR,
    SHIPPER_ADD_LOADING,
    SHIPPER_ADD_INVALID,
    SHIPPER_ADD_SUCCESS,

    SHIPPER_DELETE_ERROR,
    SHIPPER_DELETE_LOADING,
    SHIPPER_DELETE_SUCCESS
} from './constant'

const shipperGetError = (bool) => {
    return {
        type: SHIPPER_GET_ERROR,
        shipperGetError: bool
    }
}

const shipperGetLoading = (bool) => {
    return {
        type: SHIPPER_GET_LOADING,
        shipperGetLoading: bool
    }
}

const shipperGetSuccess = (bool, shippers) => {
    return {
        type: SHIPPER_GET_SUCCESS,
        shipperGetSuccess: bool,
        shippers
    }
}

const shipperViewError = (bool) => {
    return {
        type: SHIPPER_VIEW_ERROR,
        shipperViewError: bool
    }
}

const shipperViewLoading = (bool) => {
    return {
        type: SHIPPER_VIEW_LOADING,
        shipperViewLoading: bool
    }
}

const shipperViewSuccess = (bool, shipper) => {
    return {
        type: SHIPPER_VIEW_SUCCESS,
        shipperViewSuccess: bool,
        shipper
    }
}

const shipperAddError = (bool) => {
    return {
        type: SHIPPER_ADD_ERROR,
        shipperAddError: bool
    }
}

const shipperAddLoading = (bool) => {
    return {
        type: SHIPPER_ADD_LOADING,
        shipperAddLoading: bool
    }
}

const shipperAddInvalid = (bool, invalid) => {
    return {
        type: SHIPPER_ADD_INVALID,
        shipperAddInvalid: bool,
        invalid
    }
}

const shipperAddSuccess = (bool, shipper) => {
    return {
        type: SHIPPER_ADD_SUCCESS,
        shipperAddSuccess: bool,
        shipper
    }
}

const shipperDeleteError = (bool) => {
    return {
        type: SHIPPER_DELETE_ERROR,
        shipperDeleteError: bool
    }
}

const shipperDeleteLoading = (bool) => {
    return {
        type: SHIPPER_DELETE_LOADING,
        shipperDeleteLoading: bool
    }
}

const shipperDeleteSuccess = (bool, shipper) => {
    return {
        type: SHIPPER_DELETE_SUCCESS,
        shipperDeleteSuccess: bool,
        shipper
    }
}

export function shipperFetchAll() {
    return (dispatch) => {
        dispatch(shipperGetSuccess(false, null))
        dispatch(shipperGetError(false))
        dispatch(shipperGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + SHIPPER_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(shipperGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(shipperGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(shipperGetError(true))
            })
    }
}

export function shipperFetchSub(shipper_id) {
    return (dispatch) => {
        dispatch(shipperGetSuccess(false, null))
        dispatch(shipperGetError(false))
        dispatch(shipperGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + SHIPPER_URL + '/' + shipper_id, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(shipperGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(shipperGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(shipperGetError(true))
            })
    }
}

export function shipperFetchPage(item) {
    return (dispatch) => {
        dispatch(shipperGetSuccess(false, null))
        dispatch(shipperGetError(false))
        dispatch(shipperGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(item)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + SHIPPER_URL + '/page', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(shipperGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(shipperGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(shipperGetError(true))
            })
    }
}

export function shipperAdd(data) {
    return (dispatch) => {
        dispatch(shipperAddError(false))
        dispatch(shipperAddSuccess(false, null))
        dispatch(shipperAddInvalid(false, null))
        dispatch(shipperAddLoading(true))

        let token = data.tokenAuth || null
        let config = {}
        let formData = new FormData();
        for (var key in data) {
            formData.append(key, data[key])
        }
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                },
                body: formData
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + SHIPPER_URL + '/new', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(shipperAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((shipper) => {
                if (shipper.success) {
                    dispatch(shipperAddSuccess(true, shipper))
                } else {
                    dispatch(shipperAddInvalid(true, shipper))
                }
            })
            .catch((err) => {
                dispatch(shipperAddError(true))
            })
    }
}

export function shipperUpdate(data) {
    return (dispatch) => {
        dispatch(shipperAddError(false))
        dispatch(shipperAddSuccess(false, null))
        dispatch(shipperAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        let formData = new FormData();
        for (var key in data) {
            formData.append(key, data[key])
        }
        if (token) {
            config = {
                method: 'PUT',
                headers: {
                    'Authorization': `Bearer ${token}`,
                },
                body: formData
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + SHIPPER_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(shipperAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((shipper) => {
                dispatch(shipperAddSuccess(true, shipper))
            })
            .catch((err) => {
                dispatch(shipperAddError(true))
            })
    }
}

export function shipperConfirm(data) {

    return (dispatch) => {
        dispatch(shipperAddError(false))
        dispatch(shipperAddSuccess(false, null))
        dispatch(shipperAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + SHIPPER_URL + '/approve', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(shipperAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((shipper) => {
                if (shipper.status === '200 OK') {
                    dispatch(shipperAddSuccess(true, shipper))
                } else {
                    dispatch(shipperAddError(true))
                }
            })
            .catch((err) => {
                dispatch(shipperAddError(true))
            })
    }
}

export function shipperFetchOne(id) {
    return (dispatch) => {
        dispatch(shipperViewError(false))
        dispatch(shipperViewSuccess(false, null))
        dispatch(shipperViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + SHIPPER_URL + '/' + id, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(shipperViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(shipperViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(shipperViewError(true))
            })
    }
}

export function shipperFetchUser(id) {
    return (dispatch) => {
        dispatch(shipperViewError(false))
        dispatch(shipperViewSuccess(false, null))
        dispatch(shipperViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'id': id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + SHIPPER_URL + '/profile', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(shipperViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(shipperViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(shipperViewError(true))
            })
    }
}

export function handleStatus(data) {
    return (dispatch) => {
        dispatch(shipperAddError(false))
        dispatch(shipperAddSuccess(false, null))
        dispatch(shipperAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}

        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }

        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + 'register/' + data.status, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(shipperAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((shipper) => {
                dispatch(shipperAddSuccess(true, shipper))
            })
            .catch((err) => {
                dispatch(shipperAddError(true))
            })
    }
}

export function shipperDelete(id) {
    return (dispatch) => {
        dispatch(shipperViewSuccess(false, null))
        dispatch(shipperDeleteError(false))
        dispatch(shipperDeleteSuccess(false, null))
        dispatch(shipperDeleteLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'id': id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + SHIPPER_URL, config)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(shipperDeleteLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(shipperDeleteSuccess(true, data))
            })
            .catch((err) => {
                dispatch(shipperDeleteError(true))
            })
    }
}