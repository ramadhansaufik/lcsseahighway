import {
    doLogout
} from './authAction'
import {
    BASE_URL,
    PACKAGING_URL,

    PACKAGING_GET_ERROR,
    PACKAGING_GET_LOADING,
    PACKAGING_GET_SUCCESS,

    PACKAGING_VIEW_ERROR,
    PACKAGING_VIEW_LOADING,
    PACKAGING_VIEW_SUCCESS,

    PACKAGING_ADD_ERROR,
    PACKAGING_ADD_LOADING,
    PACKAGING_ADD_SUCCESS,

    PACKAGING_DELETE_ERROR,
    PACKAGING_DELETE_LOADING,
    PACKAGING_DELETE_SUCCESS
} from './constant'

const packagingGetError = (bool) => {
    return {
        type: PACKAGING_GET_ERROR,
        packagingGetError: bool
    }
}

const packagingGetLoading = (bool) => {
    return {
        type: PACKAGING_GET_LOADING,
        packagingGetLoading: bool
    }
}

const packagingGetSuccess = (bool, packagings) => {
    return {
        type: PACKAGING_GET_SUCCESS,
        packagingGetSuccess: bool,
        packagings
    }
}

const packagingViewError = (bool) => {
    return {
        type: PACKAGING_VIEW_ERROR,
        packagingViewError: bool
    }
}

const packagingViewLoading = (bool) => {
    return {
        type: PACKAGING_VIEW_LOADING,
        packagingViewLoading: bool
    }
}

const packagingViewSuccess = (bool, packaging) => {
    return {
        type: PACKAGING_VIEW_SUCCESS,
        packagingViewSuccess: bool,
        packaging
    }
}

const packagingAddError = (bool) => {
    return {
        type: PACKAGING_ADD_ERROR,
        packagingAddError: bool
    }
}

const packagingAddLoading = (bool) => {
    return {
        type: PACKAGING_ADD_LOADING,
        packagingAddLoading: bool
    }
}

const packagingAddSuccess = (bool, packaging) => {
    return {
        type: PACKAGING_ADD_SUCCESS,
        packagingAddSuccess: bool,
        packaging
    }
}

const packagingDeleteError = (bool) => {
    return {
        type: PACKAGING_DELETE_ERROR,
        packagingDeleteError: bool
    }
}

const packagingDeleteLoading = (bool) => {
    return {
        type: PACKAGING_DELETE_LOADING,
        packagingDeleteLoading: bool
    }
}

const packagingDeleteSuccess = (bool, packaging) => {
    return {
        type: PACKAGING_DELETE_SUCCESS,
        packagingDeleteSuccess: bool,
        packaging
    }
}

export function packagingFetchAll(tokenAuth) {
    return (dispatch) => {
        dispatch(packagingAddSuccess(false, null))
        dispatch(packagingGetSuccess(false, null))
        dispatch(packagingGetError(false))
        dispatch(packagingGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + PACKAGING_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(packagingGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {

                dispatch(packagingGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(packagingGetError(true))
            })
    }
}

export function packagingFetchSub(path) {
    return (dispatch) => {
        dispatch(packagingAddSuccess(false, null))
        dispatch(packagingGetSuccess(false, null))
        dispatch(packagingGetError(false))
        dispatch(packagingGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + PACKAGING_URL + '/' + path, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(packagingGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(packagingGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(packagingGetError(true))
            })
    }
}

export function packagingFetchPage(params) {
    return (dispatch) => {
        dispatch(packagingAddSuccess(false, null))
        dispatch(packagingGetSuccess(false, null))
        dispatch(packagingGetError(false))
        dispatch(packagingGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + PACKAGING_URL + '/page', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(packagingGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(packagingGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(packagingGetError(true))
            })
    }
}

export function packagingAdd(data) {
    return (dispatch) => {
        dispatch(packagingAddError(false))
        dispatch(packagingAddSuccess(false, null))
        dispatch(packagingAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + PACKAGING_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(packagingAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((packaging) => {
                dispatch(packagingAddSuccess(true, packaging))
            })
            .catch((err) => {
                dispatch(packagingAddError(true))
            })
    }
}

export function packagingFetchOne(id) {
    return (dispatch) => {
        dispatch(packagingViewError(false))
        dispatch(packagingViewSuccess(false, null))
        dispatch(packagingViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + PACKAGING_URL + '/' + id, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(packagingViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(packagingViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(packagingViewError(true))
            })
    }
}

export function packagingUpdate(data) {
    return (dispatch) => {
        dispatch(packagingAddError(false))
        dispatch(packagingAddSuccess(false, null))
        dispatch(packagingAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'PUT',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + PACKAGING_URL, config)
            .then((response) => {

                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(packagingAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((packaging) => {

                if (packaging.success) {
                    dispatch(packagingAddSuccess(true, packaging))
                } else {
                    dispatch(packagingAddError(true))
                }
            })
            .catch((err) => {
                dispatch(packagingAddError(true))
            })
    }
}

export function packagingDelete(id) {
    return (dispatch) => {
        dispatch(packagingViewSuccess(false, null))
        dispatch(packagingDeleteError(false))
        dispatch(packagingDeleteSuccess(false, null))
        dispatch(packagingDeleteLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'id': id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + PACKAGING_URL, config)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(packagingDeleteLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(packagingDeleteSuccess(true, data))
            })
            .catch((err) => {
                dispatch(packagingDeleteError(true))
            })
    }
}