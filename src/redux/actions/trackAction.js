import {
    doLogout
} from './authAction'
import {
    BASE_URL,
    TRACK_URL,

    TRACK_GET_ERROR,
    TRACK_GET_LOADING,
    TRACK_GET_SUCCESS,

    TRACK_VIEW_ERROR,
    TRACK_VIEW_LOADING,
    TRACK_VIEW_SUCCESS,

    TRACK_ADD_ERROR,
    TRACK_ADD_LOADING,
    TRACK_ADD_SUCCESS
} from './constant'

const trackGetError = (bool) => {
    return {
        type: TRACK_GET_ERROR,
        trackGetError: bool
    }
}

const trackGetLoading = (bool) => {
    return {
        type: TRACK_GET_LOADING,
        trackGetLoading: bool
    }
}

const trackGetSuccess = (bool, tracks) => {
    return {
        type: TRACK_GET_SUCCESS,
        trackGetSuccess: bool,
        tracks
    }
}

const trackViewError = (bool) => {
    return {
        type: TRACK_VIEW_ERROR,
        trackViewError: bool
    }
}

const trackViewLoading = (bool) => {
    return {
        type: TRACK_VIEW_LOADING,
        trackViewLoading: bool
    }
}

const trackViewSuccess = (bool, track) => {
    return {
        type: TRACK_VIEW_SUCCESS,
        trackViewSuccess: bool,
        track
    }
}

const trackAddError = (bool) => {
    return {
        type: TRACK_ADD_ERROR,
        trackAddError: bool
    }
}

const trackAddLoading = (bool) => {
    return {
        type: TRACK_ADD_LOADING,
        trackAddLoading: bool
    }
}

const trackAddSuccess = (bool, track) => {
    return {
        type: TRACK_ADD_SUCCESS,
        trackAddSuccess: bool,
        track
    }
}

export function trackFetchAll() {
    return (dispatch) => {
        dispatch(trackGetSuccess(false, null))
        dispatch(trackGetError(false))
        dispatch(trackGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + TRACK_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(trackGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(trackGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(trackGetError(true))
            })
    }
}

export function trackFetchSub(path) {
    return (dispatch) => {
        dispatch(trackGetSuccess(false, null))
        dispatch(trackGetError(false))
        dispatch(trackGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + TRACK_URL + '/' + path, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(trackGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(trackGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(trackGetError(true))
            })
    }
}

export function trackFetchPage(params) {
    return (dispatch) => {
        dispatch(trackGetSuccess(false, null))
        dispatch(trackGetError(false))
        dispatch(trackGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + TRACK_URL + '/page', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(trackGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(trackGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(trackGetError(true))
            })
    }
}

export function trackAdd(data) {
    return (dispatch) => {
        dispatch(trackAddError(false))
        dispatch(trackAddSuccess(false, null))
        dispatch(trackAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + TRACK_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(trackAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((track) => {
                dispatch(trackAddSuccess(true, track))
            })
            .catch((err) => {
                dispatch(trackAddError(true))
            })
    }
}

export function trackUpdate(data) {
    return (dispatch) => {
        dispatch(trackAddError(false))
        dispatch(trackAddSuccess(false, null))
        dispatch(trackAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'PUT',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + TRACK_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(trackAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((track) => {
                dispatch(trackAddSuccess(true, track))
            })
            .catch((err) => {
                dispatch(trackAddError(true))
            })
    }
}

export function trackFetchOne(id) {
    return (dispatch) => {
        dispatch(trackViewError(false))
        dispatch(trackViewSuccess(false, null))
        dispatch(trackViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + TRACK_URL + '/' + id, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(trackViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(trackViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(trackViewError(true))
            })
    }
}