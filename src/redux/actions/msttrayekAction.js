import {
    doLogout
} from './authAction'
import {
    BASE_URL,
    MSTTRAYEK_URL,

    MSTTRAYEK_GET_ERROR,
    MSTTRAYEK_GET_LOADING,
    MSTTRAYEK_GET_SUCCESS,

    MSTTRAYEK_VIEW_ERROR,
    MSTTRAYEK_VIEW_LOADING,
    MSTTRAYEK_VIEW_SUCCESS,

    MSTTRAYEK_ADD_ERROR,
    MSTTRAYEK_ADD_LOADING,
    MSTTRAYEK_ADD_SUCCESS,

    MSTTRAYEK_DELETE_ERROR,
    MSTTRAYEK_DELETE_LOADING,
    MSTTRAYEK_DELETE_SUCCESS
} from './constant'

const msttrayekGetError = (bool) => {
    return {
        type: MSTTRAYEK_GET_ERROR,
        msttrayekGetError: bool
    }
}

const msttrayekGetLoading = (bool) => {
    return {
        type: MSTTRAYEK_GET_LOADING,
        msttrayekGetLoading: bool
    }
}

const msttrayekGetSuccess = (bool, msttrayeks) => {
    return {
        type: MSTTRAYEK_GET_SUCCESS,
        msttrayekGetSuccess: bool,
        msttrayeks
    }
}

const msttrayekViewError = (bool) => {
    return {
        type: MSTTRAYEK_VIEW_ERROR,
        msttrayekViewError: bool
    }
}

const msttrayekViewLoading = (bool) => {
    return {
        type: MSTTRAYEK_VIEW_LOADING,
        msttrayekViewLoading: bool
    }
}

const msttrayekViewSuccess = (bool, msttrayek) => {
    return {
        type: MSTTRAYEK_VIEW_SUCCESS,
        msttrayekViewSuccess: bool,
        msttrayek
    }
}

const msttrayekAddError = (bool) => {
    return {
        type: MSTTRAYEK_ADD_ERROR,
        msttrayekAddError: bool
    }
}

const msttrayekAddLoading = (bool) => {
    return {
        type: MSTTRAYEK_ADD_LOADING,
        msttrayekAddLoading: bool
    }
}

const msttrayekAddSuccess = (bool, msttrayek) => {
    return {
        type: MSTTRAYEK_ADD_SUCCESS,
        msttrayekAddSuccess: bool,
        msttrayek
    }
}

const msttrayekDeleteError = (bool) => {
    return {
        type: MSTTRAYEK_DELETE_ERROR,
        msttrayekDeleteError: bool
    }
}

const msttrayekDeleteLoading = (bool) => {
    return {
        type: MSTTRAYEK_DELETE_LOADING,
        msttrayekDeleteLoading: bool
    }
}

const msttrayekDeleteSuccess = (bool, msttrayek) => {
    return {
        type: MSTTRAYEK_DELETE_SUCCESS,
        msttrayekDeleteSuccess: bool,
        msttrayek
    }
}

export function msttrayekFetchAll() {
    return (dispatch) => {
        dispatch(msttrayekAddSuccess(false, null))
        dispatch(msttrayekGetSuccess(false, null))
        dispatch(msttrayekGetError(false))
        dispatch(msttrayekGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + MSTTRAYEK_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(msttrayekGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(msttrayekGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(msttrayekGetError(true))
            })
    }
}

export function msttrayekFetchSub(path) {
    return (dispatch) => {
        dispatch(msttrayekAddSuccess(false, null))
        dispatch(msttrayekGetSuccess(false, null))
        dispatch(msttrayekGetError(false))
        dispatch(msttrayekGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + MSTTRAYEK_URL + '/' + path, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(msttrayekGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(msttrayekGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(msttrayekGetError(true))
            })
    }
}

export function msttrayekFetchPage(params) {
    return (dispatch) => {
        dispatch(msttrayekAddSuccess(false, null))
        dispatch(msttrayekGetSuccess(false, null))
        dispatch(msttrayekGetError(false))
        dispatch(msttrayekGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + MSTTRAYEK_URL + '/page', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(msttrayekGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(msttrayekGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(msttrayekGetError(true))
            })
    }
}

export function msttrayekAdd(data) {

    return (dispatch) => {
        dispatch(msttrayekAddError(false))
        dispatch(msttrayekAddSuccess(false, null))
        dispatch(msttrayekAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}

        config = {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }
        fetch(BASE_URL + MSTTRAYEK_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(msttrayekAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((msttrayek) => {
                if (msttrayek.success) {
                    dispatch(msttrayekAddSuccess(true, msttrayek.data))
                } else {
                    dispatch(msttrayekAddError(true))
                }
            })
            .catch((err) => {
                dispatch(msttrayekAddError(true))
            })
    }
}

export function msttrayekFetchOne(id) {
    return (dispatch) => {
        dispatch(msttrayekAddSuccess(false, null))
        dispatch(msttrayekViewError(false))
        dispatch(msttrayekViewSuccess(false, null))
        dispatch(msttrayekViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + MSTTRAYEK_URL + '/' + id, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(msttrayekViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(msttrayekViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(msttrayekViewError(true))
            })
    }
}

export function msttrayekUpdate(data) {
    return (dispatch) => {
        dispatch(msttrayekAddError(false))
        dispatch(msttrayekAddSuccess(false, null))
        dispatch(msttrayekAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'PUT',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + MSTTRAYEK_URL, config)
            .then((response) => {

                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(msttrayekAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((msttrayek) => {

                if (msttrayek.success) {
                    dispatch(msttrayekAddSuccess(true, msttrayek))
                } else {
                    dispatch(msttrayekAddError(true))
                }
            })
            .catch((err) => {
                dispatch(msttrayekAddError(true))
            })
    }
}

export function msttrayekDelete(id) {
    return (dispatch) => {
        dispatch(msttrayekGetSuccess(false, null))
        dispatch(msttrayekDeleteError(false))
        dispatch(msttrayekDeleteSuccess(false, null))
        dispatch(msttrayekDeleteLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'id': id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + MSTTRAYEK_URL, config)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(msttrayekDeleteLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(msttrayekDeleteSuccess(true, data))
            })
            .catch((err) => {
                dispatch(msttrayekDeleteError(true))
            })
    }
}