import {
    doLogout
} from './authAction'
import {
    BASE_URL,
    DELIVERY_URL,

    DELIVERY_GET_ERROR,
    DELIVERY_GET_LOADING,
    DELIVERY_GET_SUCCESS,

    DELIVERY_VIEW_ERROR,
    DELIVERY_VIEW_LOADING,
    DELIVERY_VIEW_SUCCESS,

    DELIVERY_ADD_ERROR,
    DELIVERY_ADD_LOADING,
    DELIVERY_ADD_SUCCESS,

    DELIVERY_DELETE_ERROR,
    DELIVERY_DELETE_LOADING,
    DELIVERY_DELETE_SUCCESS
} from './constant'

const deliveryGetError = (bool) => {
    return {
        type: DELIVERY_GET_ERROR,
        deliveryGetError: bool
    }
}

const deliveryGetLoading = (bool) => {
    return {
        type: DELIVERY_GET_LOADING,
        deliveryGetLoading: bool
    }
}

const deliveryGetSuccess = (bool, deliveries) => {
    return {
        type: DELIVERY_GET_SUCCESS,
        deliveryGetSuccess: bool,
        deliveries
    }
}

const deliveryViewError = (bool) => {
    return {
        type: DELIVERY_VIEW_ERROR,
        deliveryViewError: bool
    }
}

const deliveryViewLoading = (bool) => {
    return {
        type: DELIVERY_VIEW_LOADING,
        deliveryViewLoading: bool
    }
}

const deliveryViewSuccess = (bool, delivery) => {
    return {
        type: DELIVERY_VIEW_SUCCESS,
        deliveryViewSuccess: bool,
        delivery
    }
}

const deliveryAddError = (bool) => {
    return {
        type: DELIVERY_ADD_ERROR,
        deliveryAddError: bool
    }
}

const deliveryAddLoading = (bool) => {
    return {
        type: DELIVERY_ADD_LOADING,
        deliveryAddLoading: bool
    }
}

const deliveryAddSuccess = (bool, delivery) => {
    return {
        type: DELIVERY_ADD_SUCCESS,
        deliveryAddSuccess: bool,
        delivery
    }
}

const deliveryDeleteError = (bool) => {
    return {
        type: DELIVERY_DELETE_ERROR,
        deliveryDeleteError: bool
    }
}

const deliveryDeleteLoading = (bool) => {
    return {
        type: DELIVERY_DELETE_LOADING,
        deliveryDeleteLoading: bool
    }
}

const deliveryDeleteSuccess = (bool, delivery) => {
    return {
        type: DELIVERY_DELETE_SUCCESS,
        deliveryDeleteSuccess: bool,
        delivery
    }
}

export function deliveryFetchAll() {
    return (dispatch) => {
        dispatch(deliveryGetSuccess(false, null))
        dispatch(deliveryGetError(false))
        dispatch(deliveryGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'PATCH',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + DELIVERY_URL + '/delivery_order_all', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(deliveryGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(deliveryGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(deliveryGetError(true))
            })
    }
}

export function deliveryFetchSub(path) {
    return (dispatch) => {
        dispatch(deliveryAddSuccess(false, null))
        dispatch(deliveryGetSuccess(false, null))
        dispatch(deliveryGetError(false))
        dispatch(deliveryGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + DELIVERY_URL + '/' + path, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(deliveryGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(deliveryGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(deliveryGetError(true))
            })
    }
}

export function deliveryFetchPage(params) {
    return (dispatch) => {
        dispatch(deliveryAddSuccess(false, null))
        dispatch(deliveryGetSuccess(false, null))
        dispatch(deliveryGetError(false))
        dispatch(deliveryGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + DELIVERY_URL + '/page', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(deliveryGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(deliveryGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(deliveryGetError(true))
            })
    }
}

export function deliveryAdd(data) {
    return (dispatch) => {
        dispatch(deliveryAddError(false))
        dispatch(deliveryAddSuccess(false, null))
        dispatch(deliveryAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + DELIVERY_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(deliveryAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((delivery) => {
                dispatch(deliveryAddSuccess(true, delivery))
            })
            .catch((err) => {
                dispatch(deliveryAddError(true))
            })
    }
}

export function deliveryConfirm(data) {
    return (dispatch) => {
        dispatch(deliveryViewSuccess(false, null))
        dispatch(deliveryAddError(false))
        dispatch(deliveryAddSuccess(false, null))
        dispatch(deliveryAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + DELIVERY_URL + '/approve', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(deliveryAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((delivery) => {
                if (delivery.status == '200 OK') {
                    dispatch(deliveryAddSuccess(true, delivery))
                } else {
                    dispatch(deliveryAddError(true))
                }
            })
            .catch((err) => {
                dispatch(deliveryAddError(true))
            })
    }
}

export function deliveryFetchOne(id) {
    return (dispatch) => {
        dispatch(deliveryAddSuccess(false, null))
        dispatch(deliveryViewError(false))
        dispatch(deliveryViewSuccess(false, null))
        dispatch(deliveryViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + DELIVERY_URL + '/' + id, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(deliveryViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((delivery) => {
                if (delivery.success) {
                    dispatch(deliveryViewSuccess(true, delivery))
                } else {
                    dispatch(deliveryViewError(true))
                }
            })
            .catch((err) => {
                dispatch(deliveryViewError(true))
            })
    }
}

export function deliveryUpdate(data) {
    return (dispatch) => {
        dispatch(deliveryAddError(false))
        dispatch(deliveryAddSuccess(false, null))
        dispatch(deliveryAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'PUT',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + DELIVERY_URL, config)
            .then((response) => {

                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(deliveryAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((delivery) => {

                if (delivery.success) {
                    dispatch(deliveryViewSuccess(false, null))
                    dispatch(deliveryAddSuccess(true, delivery))
                } else {
                    dispatch(deliveryAddError(true))
                }
            })
            .catch((err) => {
                dispatch(deliveryAddError(true))
            })
    }
}

export function deliveryDelete(id) {
    return (dispatch) => {
        dispatch(deliveryViewSuccess(false, null))
        dispatch(deliveryDeleteError(false))
        dispatch(deliveryDeleteSuccess(false, null))
        dispatch(deliveryDeleteLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'id': id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + DELIVERY_URL, config)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(deliveryDeleteLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(deliveryDeleteSuccess(true, data))
            })
            .catch((err) => {
                dispatch(deliveryDeleteError(true))
            })
    }
}