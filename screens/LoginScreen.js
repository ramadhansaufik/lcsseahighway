import React, { useState, useRef, useEffect } from 'react';
import { useNavigation } from 'react-navigation-hooks';
import { ScrollView, StyleSheet, KeyboardAvoidingView, Image, Text, View, AsyncStorage, TouchableWithoutFeedback } from 'react-native';
import { Container, Content, Form, Item, Input, Button, Icon, Picker } from 'native-base';
import { useDispatch, useSelector } from 'react-redux'
import { doLogin } from '../src/redux/actions/authAction';

import window from '../constants/Layout.js'

export default function LoginScreen() {
  const LOGO = '../assets/images/logo.png';
  const BACKGROUND = '../assets/images/tollaut.png';
  
  const isAuthenticated = useSelector(state => state.auth.isAuthenticated)
  const auth = useSelector(state => state.auth)
  const [data, setData] = useState({
    email:'',
    password:'',
    usery_type_id:''
  });
  const { navigate } = useNavigation();
  const navigation = useRef(useNavigation());
  const dispatch = useDispatch();
  useEffect(() => {
    navigation.current.navigate(isAuthenticated ? 'Main' : 'Auth')
  }, [isAuthenticated])

  function handleLogin() {
    dispatch(doLogin(data));
  }

  return (
    <KeyboardAvoidingView behavior='height'>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{ marginHorizontal:30, marginTop: 80 }}>
          <Image source={require(LOGO)} style={{ width: 40, height: 45  }}/>
          <Text style={{ color:"darkblue", fontSize:25 }}>Login to LCS</Text>
        </View>
        <Form style={{ marginTop: 10, marginHorizontal:30 }}>
          <Picker
            mode="dropdown"
            placeholder="Select your SIM"
            iosIcon={<Icon name="arrow-down" />}
            placeholder="Select your SIM"
            textStyle={{ color: "#5cb85c" }}
            itemStyle={{
              backgroundColor: "#d3d3d3",
              marginLeft: 0,
              paddingLeft: 10,
            }}
            itemTextStyle={{ color: '#788ad2' }}
            style={{ width: undefined, borderWidth: 1 }}
            selectedValue={data.usery_type_id}
            onValueChange={value => setData({...data, usery_type_id:value})}
          >
            <Picker.Item label="Jenis User" value='0' />
            <Picker.Item label="Consigne" value='1' />
            <Picker.Item label="Supplier" value='2' />
            <Picker.Item label="Shipper" value='3' />
            <Picker.Item label="Reseller" value='4' />
            <Picker.Item label="Vessel Operator" value='5' />
            <Picker.Item label="Regulator" value='6' />
          </Picker>
          <Item rounded style={ styles.item }>
            <Input placeholder="Email" style={ styles.itemInput } value={data.email} onChangeText={val => setData({...data, email:val})}/>
            <Icon name='mail' style={{ width:40 }}/>
          </Item>
          <Item rounded style={ styles.item }>
            <Input secureTextEntry={true} placeholder="Password" style={ styles.itemInput } value={data.password} onChangeText={val => setData({...data, password:val})}/>
            <Text>Show</Text>
            <Icon name='key' style={{ width:40 }}/>
          </Item>
        </Form>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, marginHorizontal: 30}}>
          <Text style={{ alignSelf: 'center', color:'darkblue' }}> Forgot Password ? </Text>
          <Button block iconRight rounded onPress={() => handleLogin()}>
            <Text style={{ color: "#fff", paddingHorizontal: 40 }}>LOGIN</Text>
            <Icon name='log-in' style={{ marginRight: 0, padding: 10, borderTopRightRadius: 20, borderBottomRightRadius: 20, backgroundColor: 'darkblue' }}/>
          </Button>
        </View>
        <View style={{marginHorizontal:30, marginTop: 30}}>
          <TouchableWithoutFeedback onPress={() => navigate('Register')}>
            <Text style={{color:'midnightblue', textAlign: 'center', fontWeight:'bold'}}>Don't have an account Register Here</Text>
          </TouchableWithoutFeedback>
        </View>
        {/*
        <Button block success onPress={() => navigate('Register')} style={{ paddingHorizontal: 10, marginTop:20, marginHorizontal: 30, justifyContent:'center', borderBottomLeftRadius: 30, borderBottomRightRadius: 30 }}>
          <Text style={{ color: "#fff" }}>REGISTER</Text>
        </Button>
      */}
        <Image source={require(BACKGROUND)} style={{ width:window.width, height:200 }}/>
      </ScrollView>
    </KeyboardAvoidingView>
  );
}

LoginScreen.navigationOptions = {
  headerShown: false,
  title: 'Login',
};

const styles = StyleSheet.create({
  item: {
    marginTop: 20,
  },
  itemInput: {
    marginLeft: 10,
  }
});
