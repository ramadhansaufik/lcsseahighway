import React from 'react';
import { StyleSheet } from 'react-native';
import { Container, Content, Card, CardItem, Body } from 'native-base';

function PurchaseOrder() {
  return (
    <Container>
      <Content>
        
      </Content>
    </Container>
  );
}

PurchaseOrder.navigationOptions = {
  title: 'Purchase Order',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});

export { PurchaseOrder };