import React, {useState} from 'react';
import { SafeAreaView, StyleSheet, View, Text, Dimensions } from 'react-native';
import MapView, { Marker } from 'react-native-maps';

function VesselTracking() {
  const [marker, setMarker] = useState({
    markerData: {
      latitude: -3.470564,
      longitude: 118.982802,
    },
    markerData1: {
      latitude: -7.470564,
      longitude: 112.982802,
    },
    mapData: {
      latitude: -3.470564,
      longitude: 118.982802,
      latitudeDelta: 0.015,
      longitudeDelta: 0.0121,
    },
  })
  const [initialRegion, setInitialRegion] = useState({
    latitude: -3.470564,
    longitude: 118.982802,
    latitudeDelta: 20.015,
    longitudeDelta: 20.0121,
  })
  const DEVICE_WIDTH = Dimensions.get('window').width;
  return (
    <SafeAreaView style={styles.container}>
      {/* Vessel Tracking */}
      <View>
        
        <View>
          <MapView style={styles.mapStyle} initialRegion={initialRegion}>
            <Marker
              coordinate={marker.markerData}
              title="Home"
              description="This is our home"
            />
            <Marker
              coordinate={marker.markerData1}
              title="Home"
              description="This is our home"
            />
          </MapView>
        </View>
      </View>
    </SafeAreaView>
  );
}

VesselTracking.navigationOptions = {
  title: 'VesselTracking',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  mapStyle: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
});

export { VesselTracking };