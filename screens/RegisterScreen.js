import React, { 
  useState, 
  useEffect } from 'react';
import { 
  useNavigation } from 'react-navigation-hooks';
import { 
  TouchableWithoutFeedback, 
  ScrollView, 
  StyleSheet, 
  KeyboardAvoidingView, 
  Image, 
  Text, 
  View, 
  Alert, 
  Modal } from 'react-native';
import { 
  Container, 
  Content, 
  Form, 
  Item, 
  Input, 
  Button, 
  Icon, 
  Picker } from 'native-base';
import { 
  useDispatch, 
  useSelector } from 'react-redux';
import { 
  doRegister, 
  doRegisterCheck } from '../src/redux/actions/authAction';
import * as DocumentPicker from 'expo-document-picker';
import { consigneeAdd } from '../src/redux/actions/consigneeAction';
import { supplierAdd } from '../src/redux/actions/supplierAction';
import { shipperAdd } from '../src/redux/actions/shipperAction';
import { resellerAdd } from '../src/redux/actions/resellerAction';
import { operatorAdd } from '../src/redux/actions/operatorAction';

export default function RegisterScreen() {
  const LOGO = '../assets/images/logo.png';
  const BACKGROUND = '../assets/images/tollaut.png';

  // States
  //--- Main data
  const [data, setData] = useState({
    email:'',
    password:'',
    usery_type_id:'',
    siup_doc: {
      name:''
    },
    npwp_doc: {
      name:''
    }
  });
  //--- Profile Submit
  const [profileSubmit, setProfileSubmit] = useState({})

  const [siup_doc, setSiupDoc] = useState({})
  //--- Profile in Modal
  const [profile, setProfile] = useState(''); 
  //--- Submitted Profile
  const [detailProfile, setDetailProfile] = useState({});
  //--- Modal : Submit Profile
  const [modalVisible, setModalVisible] = useState(false);

  // Navigasi
  const { navigate } = useNavigation();

  // Register Checking State
  const authRegisterCheckSuccess = useSelector(state => state.auth.authRegisterCheckSuccess);
  const authRegisterCheckError = useSelector(state => state.auth.authRegisterCheckError);

  // Register State
  const authRegisterSuccess = useSelector(state => state.auth.authRegisterSuccess);
  const authRegisterError = useSelector(state => state.auth.authRegisterError);
  const account  = useSelector(state => state.auth.account);

  // Memanggil dispatch ke redux action
  const dispatch = useDispatch();

  // Reset Data dan Hide Modal ketika re-render
  useEffect(() => {
    setModalVisible(false)
    setData({
      ...data,
      email:null,
      password:null,
      usery_type_id:null,
      siup_doc:{
        name:'',
        uri:'empty',
        preview:'none'
      },
      npwp_doc: {
        name:'',
        uri:'empty',
        preview:'none'
      },
      
      foto_ttd: {
        name:'',
        uri:'empty',
        preview:'none'
      },
      surat_rekomendasi: {
        name:'',
        uri:'empty',
        preview:'none'
      },
      logo_perusahaan: {
        name:'',
        uri:'empty',
        preview:'none'
      },
      identify_officerdoc1: {
        name:'',
        uri:'empty',
        preview:'none'
      },
      identify_officerdoc2: {
        name:'',
        uri:'empty',
        preview:'none'
      },
      identify_officerdoc3: {
        name:'',
        uri:'empty',
        preview:'none'
      },

      tgl_pmku:'dd-mm-yy',
    })
  }, [])
  // Perubahan Input
  function handleChange(name, value) {
    setData({...data, [name]: value})
  }
  // Handle File Input (call DocumentPicker)
  function handleFile(name) {
    const options = {
      type: 'image/*',
      copyToCacheDirectory: false,
      multiple: false
    }
    DocumentPicker.getDocumentAsync(options)
      .then(response => {
        if(response.type == 'success') {
          setData({
            ...data,
            [name]:{preview:'block'},
            [name]:response
          })
        } else {
          setData({
            ...data,
            [name]:{
              ...[name],
              name:'',
              uri:'empty',
              preview:'none'
            }
          })
        }
      })
      .catch(error => console.warn(error))
  }
  // Register Check
  function handleRegisterCheck() {
    dispatch(doRegisterCheck(data))
  }
  // If doRegisterCheck() called
  useEffect(() => {
    if(data.email && data.password && authRegisterCheckSuccess && !authRegisterCheckError) {
      switch(data.usery_type_id) {
        case '1': setProfile('Consigne')
                  break;
        case '2': setProfile('Supplier')
                  break;
        case '3': setProfile('Shipper')
                  break;
        case '4': setProfile('Reseller')
                  break;
        case '5': setProfile('Vessel Operator')
                  break;
      }
      setModalVisible(true)
    }
  }, [authRegisterCheckSuccess, authRegisterCheckError])
  // Register
  function handleProfileSubmit(name, value) {
    dispatch(doRegister({
      email:data.email,
      password:data.password,
      usery_type_id:data.usery_type_id
    }))
  }
  // If doRegister() Called
  useEffect(() => {
    if(authRegisterSuccess && !authRegisterError && account) {
      setProfileSubmit({
        ...data,
        tokenAuth:account.token,
        users_id:account.data[0].id,
        siup_doc:{
          uri: data.siup_doc.uri,
          type: 'image',
          name: data.siup_doc.name
        },
        npwp_doc:{
          uri: data.npwp_doc.uri,
          type: 'image',
          name: data.npwp_doc.name
        },
        foto_ttd:{
          uri: data.foto_ttd.uri,
          type: 'image',
          name: data.foto_ttd.name
        },
        surat_rekomendasi:{
          uri: data.surat_rekomendasi.uri,
          type: 'image',
          name: data.surat_rekomendasi.name
        },
        logo_perusahaan:{
          uri: data.logo_perusahaan.uri,
          type: 'image',
          name: data.logo_perusahaan.name
        },
        identify_officerdoc1:{
          uri: data.identify_officerdoc1.uri,
          type: 'image',
          name: data.identify_officerdoc1.name
        },
        identify_officerdoc2:{
          uri: data.identify_officerdoc2.uri,
          type: 'image',
          name: data.identify_officerdoc2.name
        },
        identify_officerdoc3:{
          uri: data.identify_officerdoc3.uri,
          type: 'image',
          name: data.identify_officerdoc3.name
        }
      })
      // console.warn(`Register Sukses: dengan token (${account.token})`)
      // console.warn(`UserID: ${account.data[0].id}`)
      // console.warn(`UserType: ${account.data[0].usery_type_id}`)

      // switch(data.usery_type_id) {
      //   case '1': dispatch(consigneeAdd(data))
      //             break;
      //   case '2': dispatch(supplierAdd(data))
      //             break;
      //   case '3': dispatch(shipperAdd(data))
      //             break;
      //   case '4': dispatch(resellerAdd(data))
      //             break;
      //   case '5': dispatch(operatorAdd(data))
      //             break;
      // }
    }
  }, [authRegisterSuccess, authRegisterError, account])
  useEffect(() => {
    if(authRegisterSuccess && !authRegisterError && profileSubmit.tokenAuth && profileSubmit.users_id) {
      console.warn(profileSubmit)
      switch(profileSubmit.usery_type_id) {
        case '1': dispatch(consigneeAdd(profileSubmit))
                  break;
        case '2': dispatch(supplierAdd(profileSubmit))
                  break;
        case '3': dispatch(shipperAdd(profileSubmit))
                  break;
        case '4': dispatch(resellerAdd(profileSubmit))
                  break;
        case '5': dispatch(operatorAdd(profileSubmit))
                  break;
      }
    }
  }, [profileSubmit, authRegisterSuccess, authRegisterError])

  return (
    <KeyboardAvoidingView behavior='height'>
      <ScrollView>
        {/*-------------------------------- Lengkapi Profile --------------*/}
        <Modal
          transparent={false}
          visible={modalVisible}
        >
          <View style={{ position:'absolute', bottom:20, left:10, zIndex:999 }}>
            <TouchableWithoutFeedback onPress={() => setModalVisible(!modalVisible)}>
              <Icon name="arrow-round-back" style={{ paddingVertical: 10, paddingHorizontal:20 ,borderRadius: 10, backgroundColor:'#eee' }}/>
            </TouchableWithoutFeedback>
          </View>
          <ScrollView>
          <View style={{ marginTop: 22, marginBottom: 22 }}>
            <View>
              <View>
                <Text style={{fontSize:24, marginLeft: 30}}> 
                  <Text style={{color:'darkblue'}}>
                  {profile} 
                  </Text> 
                  &nbsp;Profile
                </Text>
              </View>
              <View style={{ marginHorizontal:30 }}>
                <Text>Lengkapi profile anda untuk menggunakan aplikasi ini</Text>
              </View>
              <Form style={{ marginHorizontal:30 }}>
                <Item rounded style={ styles.item }>
                  <Input placeholder="Nama Perusahaan" style={ styles.itemInput } onChangeText={(value) => handleChange('nama_perusahaan', value)}/>
                  <Icon name='contact' style={{ width:40 }}/>
                </Item>
                <Item rounded disabled style={ [styles.item, {backgroundColor:'#ccc'}] }>
                  <Input disabled placeholder="Email" value={data.email} style={ styles.itemInput } onChangeText={(value) => handleChange('email', value)}/>
                  <Icon name='mail' style={{ width:40 }}/>
                </Item>
                <Item rounded style={ styles.item }>
                  <Input placeholder="Alamat" style={ [styles.itemInput, {maxHeight:100}] } multiline={true} numberOfLines={5} onChangeText={(value) => handleChange('alamat', value)}/>
                  <Icon name='locate' style={{ width:40 }}/>
                </Item>
                <Item rounded style={ styles.item }>
                  <Input placeholder="Provinsi" style={ styles.itemInput } onChangeText={(value) => handleChange('provinsi_id', value)}/>
                  <Icon name='locate' style={{ width:40 }}/>
                </Item>
                <Item rounded style={ styles.item }>
                  <Input placeholder="Kota" style={ styles.itemInput } onChangeText={(value) => handleChange('kota_id', value)}/>
                  <Icon name='locate' style={{ width:40 }}/>
                </Item>
                <Item rounded style={ styles.item }>
                  <Input placeholder="Fax" style={ styles.itemInput } onChangeText={(value) => handleChange('fax', value)}/>
                  <Icon name='print' style={{ width:40 }}/>
                </Item>
                <Item rounded style={ styles.item }>
                  <Input placeholder="Phone" style={ styles.itemInput } onChangeText={(value) => handleChange('telp', value)}/>
                  <Icon name='call' style={{ width:40 }}/>
                </Item>
                <Item rounded style={ styles.item }>
                  <Input placeholder="SIUP" style={ styles.itemInput } onChangeText={(value) => handleChange('siup', value)}/>
                  <Icon name='document' style={{ width:40 }}/>
                </Item>
                {/* SIUP doc */}
                <Item rounded style={ styles.item } onPress={() => handleFile('siup_doc')}>
                  <Input placeholder="Upload SIUP" disabled style={ styles.itemInput } value={ data.siup_doc.name }/>
                  <Icon name='document' style={{ width:40 }}/>
                </Item>
                {/* SIUP Doc Preview */}
                <View style={{ flexDirection:'row', justifyContent: 'space-around', marginTop: 10, display:data.siup_doc.preview }}>
                  <Image
                    style={{width: 66, height: 80}}
                    source={{uri:data.siup_doc.uri}}
                  />
                  <Text style={{ alignSelf:'center' }}>Dokumen SIUP</Text>
                  <Button style={{ alignSelf:'center' }}
                    rounded 
                    onPress={
                      () => setData({
                        ...data, 
                        siup_doc:{
                          name:'',
                          uri:'empty',
                          preview:'none'
                        }
                      })
                    }><Icon name="trash"/></Button>
                </View>
                <Item rounded style={ styles.item }>
                  <Input placeholder="NPWP" style={ styles.itemInput } onChangeText={(value) => handleChange('npwp', value)}/>
                  <Icon name='document' style={{ width:40 }}/>
                </Item>
                {/* NPWP doc */}
                <Item rounded style={ styles.item } onPress={() => handleFile('npwp_doc')}>
                  <Input placeholder="Upload NPWP" disabled style={ styles.itemInput } value={ data.npwp_doc.name }/>
                  <Icon name='document' style={{ width:40 }}/>
                </Item>
                {/* NPWP Doc Preview */}
                <View style={{ flexDirection:'row', justifyContent: 'space-around', marginTop: 10, display:data.npwp_doc.preview }}>
                  <Image
                    style={{width: 66, height: 80}}
                    source={{uri:data.npwp_doc.uri}}
                  />
                  <Text style={{ alignSelf:'center' }}>Dokumen NPWP</Text>
                  <Button style={{ alignSelf:'center' }}
                    onPress={
                      () => setData({
                        ...data, 
                        npwp_doc:{
                          name:'',
                          uri:'empty',
                          preview:'none'
                        }
                      })
                    }><Icon name="trash"/></Button>
                </View>
              </Form>

              {/* Form Jika User Type adalah Shipper */}
              {data.usery_type_id == '3' &&
                <Form style={{marginHorizontal:30}}>
                  <Item rounded style={ styles.item }>
                    <Input placeholder="Nama Penanda Tangan" style={ styles.itemInput } onChangeText={(value) => handleChange('nama_ttd', value)}/>
                    <Icon name='people' style={{ width:40 }}/>
                  </Item>
                  {/* Foto TTD */}
                  <Item rounded style={ styles.item } onPress={() => handleFile('foto_ttd')}>
                    <Input placeholder="Foto Tanda Tangan" disabled style={ styles.itemInput } value={ (data.foto_ttd.name === undefined) ? "" : data.foto_ttd.name }/>
                    <Icon name='document' style={{ width:40 }}/>
                  </Item>
                  {/* -------- */}
                  <View style={{ flexDirection:'row', justifyContent: 'space-around', marginTop: 10, display:data.foto_ttd.preview }}>
                    <Image
                      style={{width: 66, height: 80}}
                      source={{uri:data.foto_ttd.uri}}
                    />
                    <Text style={{ alignSelf:'center' }}>Tanda Tangan</Text>
                    <Button style={{ alignSelf:'center' }}
                      onPress={
                        () => setData({
                          ...data, 
                          foto_ttd:{
                            name:'',
                            uri:'empty',
                            preview:'none'
                          }
                        })
                      }><Icon name="trash"/></Button>
                  </View>
                  {/*-- / --*/}
                  {/* Jenis Perusahaan */}
                  <Picker
                    mode="dropdown"
                    placeholder="Jenis Perusahaan"
                    iosIcon={<Icon name="arrow-down"/>}
                    textStyle={{ color: "#5cb85c" }}
                    style={{
                      backgroundColor: "#d3d3d3",
                      marginLeft: 0,
                      paddingLeft: 10,
                      marginTop: 30,
                    }}
                    itemTextStyle={{ color: '#788ad2' }}
                    style={{ width: undefined, borderWidth: 1 }}
                    selectedValue={data.jenis_user}
                    onValueChange={value => handleChange('jenis_user', value)}
                  >
                    <Picker.Item label="Jenis Perusahaan" value="0" />
                    <Picker.Item label="BUMN" value="BUMN" />
                    <Picker.Item label="Swasta" value="Swasta" />
                  </Picker>
                  {/*-- / --*/}
                  {/* Status Perusahaan */}
                  <Picker
                    mode="dropdown"
                    placeholder="Status Perusahaan"
                    iosIcon={<Icon name="arrow-down"/>}
                    textStyle={{ color: "#5cb85c" }}
                    itemStyle={{
                      backgroundColor: "#d3d3d3",
                      marginLeft: 0,
                      paddingLeft: 10,
                    }}
                    itemTextStyle={{ color: '#788ad2' }}
                    style={{ width: undefined, borderWidth: 1 }}
                    selectedValue={data.status_perusahaan}
                    onValueChange={value => handleChange('status_perusahaan', value)}
                  >
                    <Picker.Item label="Status Perusahaan" value="0" />
                    <Picker.Item label="Pusat" value="Pusat" />
                    <Picker.Item label="Cabang" value="Cabang" />
                  </Picker>
                  {/*-- / --*/}
                  {/* Surat Rekomendasi */}
                  <Item rounded style={ styles.item } onPress={() => handleFile('surat_rekomendasi')}>
                    <Input placeholder="Surat Rekomendasi" disabled style={ styles.itemInput } value={ data.surat_rekomendasi.name }/>
                    <Icon name='document' style={{ width:40 }}/>
                  </Item>
                  {/* -------- */}
                  <View style={{ flexDirection:'row', justifyContent: 'space-around', marginTop: 10, display:data.surat_rekomendasi.preview }}>
                    <Image
                      style={{width: 66, height: 80}}
                      source={{uri:data.surat_rekomendasi.uri}}
                    />
                    <Text style={{ alignSelf:'center' }}>Surat Rekomendasi</Text>
                    <Button style={{ alignSelf:'center' }}
                      onPress={
                        () => setData({
                          ...data, 
                          surat_rekomendasi:{
                            name:'',
                            uri:'empty',
                            preview:'none'
                          }
                        })
                      }><Icon name="trash"/></Button>
                  </View>
                  {/*-- / --*/}
                  {/* Logo Perusahaan */}
                  <Item rounded style={ styles.item } onPress={() => handleFile('logo_perusahaan')}>
                    <Input placeholder="Logo Perusahaan" disabled style={ styles.itemInput } value={ data.logo_perusahaan.name }/>
                    <Icon name='document' style={{ width:40 }}/>
                  </Item>
                  {/* -------- */}
                  <View style={{ flexDirection:'row', justifyContent: 'space-around', marginTop: 10, display:data.logo_perusahaan.preview }}>
                    <Image
                      style={{width: 66, height: 80}}
                      source={{uri:data.logo_perusahaan.uri}}
                    />
                    <Text style={{ alignSelf:'center' }}>Logo Perusahaan</Text>
                    <Button style={{ alignSelf:'center' }}
                      onPress={
                        () => setData({
                          ...data, 
                          logo_perusahaan:{
                            name:'',
                            uri:'empty',
                            preview:'none'
                          }
                        })
                      }><Icon name="trash"/></Button>
                  </View>
                  {/*-- / --*/}

                  {/* Officer 1 */}
                  <Item rounded style={ styles.item }>
                    <Input placeholder="Nama Officer 1" style={ styles.itemInput } onChangeText={(value) => handleChange('nama_officer1', value)}/>
                    <Icon name='people' style={{ width:40 }}/>
                  </Item>
                  <Item rounded style={ styles.item }>
                    <Input placeholder="Telp Officer 1" style={ styles.itemInput } onChangeText={(value) => handleChange('telp_officer1', value)}/>
                    <Icon name='people' style={{ width:40 }}/>
                  </Item>
                  {/* Kartu Identitas */}
                  <Picker
                    mode="dropdown"
                    placeholder="Kartu Identitas"
                    iosIcon={<Icon name="arrow-down"/>}
                    textStyle={{ color: "#5cb85c" }}
                    itemStyle={{
                      backgroundColor: "#d3d3d3",
                      marginLeft: 0,
                      paddingLeft: 10,
                    }}
                    itemTextStyle={{ color: '#788ad2' }}
                    style={{ width: undefined, borderWidth: 1 }}
                    selectedValue={data.identify_officer1}
                    onValueChange={value => handleChange('identify_officer1', value)}
                  >
                    <Picker.Item label="Pilih Kartu Identitas" value="" />
                    <Picker.Item label="KTP" value="KTP" />
                    <Picker.Item label="SIM" value="SIM" />
                    <Picker.Item label="Kartu Identitas Lainnya" value="Kartu Identitas Lainnya" />
                  </Picker>
                  {/* File Identity */}
                  <Item rounded style={ styles.item } onPress={() => handleFile('identify_officerdoc1')}>
                    <Input placeholder="File Identitas" disabled style={ styles.itemInput } value={ data.identify_officerdoc1.name }/>
                    <Icon name='document' style={{ width:40 }}/>
                  </Item>
                  {/* -------- */}
                  <View style={{ flexDirection:'row', justifyContent: 'space-around', marginTop: 10, display:data.identify_officerdoc1.preview }}>
                    <Image
                      style={{width: 66, height: 80}}
                      source={{uri:data.identify_officerdoc1.uri}}
                    />
                    <Text style={{ alignSelf:'center' }}>File Identitas</Text>
                    <Button style={{ alignSelf:'center' }}
                      onPress={
                        () => setData({
                          ...data, 
                          identify_officerdoc1:{
                            name:'',
                            uri:'empty',
                            preview:'none'
                          }
                        })
                      }><Icon name="trash"/></Button>
                  </View>
                  {/*-- / --*/}

                  {/* Officer 2 */}
                  <Item rounded style={ styles.item }>
                    <Input placeholder="Nama Officer 2" style={ styles.itemInput } onChangeText={(value) => handleChange('nama_officer2', value)}/>
                    <Icon name='people' style={{ width:40 }}/>
                  </Item>
                  <Item rounded style={ styles.item }>
                    <Input placeholder="Telp Officer 2" style={ styles.itemInput } onChangeText={(value) => handleChange('telp_officer2', value)}/>
                    <Icon name='people' style={{ width:40 }}/>
                  </Item>
                  {/* Kartu Identitas */}
                  <Picker
                    mode="dropdown"
                    placeholder="Kartu Identitas"
                    iosIcon={<Icon name="arrow-down"/>}
                    textStyle={{ color: "#5cb85c" }}
                    itemStyle={{
                      backgroundColor: "#d3d3d3",
                      marginLeft: 0,
                      paddingLeft: 10,
                    }}
                    itemTextStyle={{ color: '#788ad2' }}
                    style={{ width: undefined, borderWidth: 1 }}
                    selectedValue={data.identify_officer2}
                    onValueChange={value => handleChange('identify_officer2', value)}
                  >
                    <Picker.Item label="Pilih Kartu Identitas" value="" />
                    <Picker.Item label="KTP" value="KTP" />
                    <Picker.Item label="SIM" value="SIM" />
                    <Picker.Item label="Kartu Identitas Lainnya" value="Kartu Identitas Lainnya" />
                  </Picker>
                  {/* File Identity */}
                  <Item rounded style={ styles.item } onPress={() => handleFile('identify_officerdoc2')}>
                    <Input placeholder="File Identitas" disabled style={ styles.itemInput } value={ data.identify_officerdoc2.name }/>
                    <Icon name='document' style={{ width:40 }}/>
                  </Item>
                  {/* -------- */}
                  <View style={{ flexDirection:'row', justifyContent: 'space-around', marginTop: 10, display:data.identify_officerdoc2.preview }}>
                    <Image
                      style={{width: 66, height: 80}}
                      source={{uri:data.identify_officerdoc2.uri}}
                    />
                    <Text style={{ alignSelf:'center' }}>File Identitas</Text>
                    <Button style={{ alignSelf:'center' }}
                      onPress={
                        () => setData({
                          ...data, 
                          identify_officerdoc2:{
                            name:'',
                            uri:'empty',
                            preview:'none'
                          }
                        })
                      }><Icon name="trash"/></Button>
                  </View>
                  {/*-- / --*/}

                  {/* Officer 3 */}
                  <Item rounded style={ styles.item }>
                    <Input placeholder="Nama Officer 3" style={ styles.itemInput } onChangeText={(value) => handleChange('nama_officer3', value)}/>
                    <Icon name='people' style={{ width:40 }}/>
                  </Item>
                  <Item rounded style={ styles.item }>
                    <Input placeholder="Telp Officer 3" style={ styles.itemInput } onChangeText={(value) => handleChange('telp_officer3', value)}/>
                    <Icon name='people' style={{ width:40 }}/>
                  </Item>
                  {/* Kartu Identitas */}
                  <Picker
                    mode="dropdown"
                    placeholder="Kartu Identitas"
                    iosIcon={<Icon name="arrow-down"/>}
                    textStyle={{ color: "#5cb85c" }}
                    itemStyle={{
                      backgroundColor: "#d3d3d3",
                      marginLeft: 0,
                      paddingLeft: 10,
                    }}
                    itemTextStyle={{ color: '#788ad2' }}
                    style={{ width: undefined, borderWidth: 1 }}
                    selectedValue={data.identify_officer3}
                    onValueChange={value => handleChange('identify_officer3', value)}
                  >
                    <Picker.Item label="Pilih Kartu Identitas" value="" />
                    <Picker.Item label="KTP" value="KTP" />
                    <Picker.Item label="SIM" value="SIM" />
                    <Picker.Item label="Kartu Identitas Lainnya" value="Kartu Identitas Lainnya" />
                  </Picker>
                  {/* File Identity */}
                  <Item rounded style={ styles.item } onPress={() => handleFile('identify_officerdoc3')}>
                    <Input placeholder="File Identitas" disabled style={ styles.itemInput } value={ data.identify_officerdoc3.name }/>
                    <Icon name='document' style={{ width:40 }}/>
                  </Item>
                  {/* -------- */}
                  <View style={{ flexDirection:'row', justifyContent: 'space-around', marginTop: 10, display:data.identify_officerdoc3.preview }}>
                    <Image
                      style={{width: 66, height: 80}}
                      source={{uri:data.identify_officerdoc3.uri}}
                    />
                    <Text style={{ alignSelf:'center' }}>File Identitas</Text>
                    <Button style={{ alignSelf:'center' }}
                      onPress={
                        () => setData({
                          ...data, 
                          identify_officerdoc3:{
                            name:'',
                            uri:'empty',
                            preview:'none'
                          }
                        })
                      }><Icon name="trash"/></Button>
                  </View>
                  {/*-- / --*/}
                </Form>
              }

              {/* Form Jika User Type adalah Vessel Operator */}
              {data.usery_type_id === '5' &&
                <Form style={{ marginHorizontal:30 }}>
                  <Text style={{fontSize:24, marginTop: 15}}> Penanda Tangan </Text>
                  <Item rounded style={ styles.item }>
                    <Input placeholder="Nama Penanda Tangan" style={ styles.itemInput } onChangeText={(value) => handleChange('nama_ttd', value)}/>
                    <Icon name='people' style={{ width:40 }}/>
                  </Item>
                  {/* Foto TTD */}
                  <Item rounded style={ styles.item } onPress={() => handleFile('foto_ttd')}>
                    <Input placeholder="Foto Tanda Tangan" disabled style={ styles.itemInput } value={ (data.foto_ttd.name === undefined) ? "" : data.foto_ttd.name }/>
                    <Icon name='document' style={{ width:40 }}/>
                  </Item>
                  <View style={{ flexDirection:'row', justifyContent: 'space-around', marginTop: 10, display:data.foto_ttd.preview }}>
                    <Image
                      style={{width: 66, height: 80}}
                      source={{uri:data.foto_ttd.uri}}
                    />
                    <Text style={{ alignSelf:'center' }}>Foto Tanda Tangan</Text>
                    <Button style={{ alignSelf:'center' }}
                      onPress={
                        () => setData({
                          ...data, 
                          foto_ttd:{
                            name:'',
                            uri:'empty',
                            preview:'none'
                          }
                        })
                      }><Icon name="trash"/></Button>
                  </View>
                  {/* -------- */}
                  {/* Logo Perusahaan */}
                  <Item rounded style={ styles.item } onPress={() => handleFile('logo_perusahaan')}>
                    <Input placeholder="Logo Perusahaan" disabled style={ styles.itemInput } value={ data.logo_perusahaan.name }/>
                    <Icon name='document' style={{ width:40 }}/>
                  </Item>
                  {/* -------- */}
                  <View style={{ flexDirection:'row', justifyContent: 'space-around', marginTop: 10, display:data.logo_perusahaan.preview }}>
                    <Image
                      style={{width: 66, height: 80}}
                      source={{uri:data.logo_perusahaan.uri}}
                    />
                    <Text style={{ alignSelf:'center' }}>Logo Perusahaan</Text>
                    <Button style={{ alignSelf:'center' }}
                      onPress={
                        () => setData({
                          ...data, 
                          logo_perusahaan:{
                            name:'',
                            uri:'empty',
                            preview:'none'
                          }
                        })
                      }><Icon name="trash"/></Button>
                  </View>
                  {/*-- / --*/}
                  <Text style={{fontSize:24, marginTop: 15}}> PMKU </Text>
                  {/* No PMKU */}
                  <Item rounded style={ styles.item }>
                    <Input placeholder="Nomor PMKU" style={ styles.itemInput } onChangeText={(value) => handleChange('no_pmku', value)}/>
                    <Icon name='people' style={{ width:40 }}/>
                  </Item>
                  {/* Tanggal PMKU */}
                  <Item rounded style={ styles.item }>
                    <Input placeholder="Tanggal PMKU" style={ styles.itemInput } onChangeText={(value) => handleChange('tgl_pmku', value)}/>
                    <Icon name='people' style={{ width:40 }}/>
                  </Item>
                  {/* Nama Bank */}
                  <Item rounded style={ styles.item }>
                    <Input placeholder="Nama Bank" style={ styles.itemInput } onChangeText={(value) => handleChange('nama_bank', value)}/>
                    <Icon name='people' style={{ width:40 }}/>
                  </Item>
                  {/* No. Rekening */}
                  <Item rounded style={ styles.item }>
                    <Input placeholder="No. Rekening" style={ styles.itemInput } onChangeText={(value) => handleChange('nomor_rekening', value)}/>
                    <Icon name='people' style={{ width:40 }}/>
                  </Item>
                  {/* Atas Nama */}
                  <Item rounded style={ styles.item }>
                    <Input placeholder="Atas Nama" style={ styles.itemInput } onChangeText={(value) => handleChange('atas_nama', value)}/>
                    <Icon name='people' style={{ width:40 }}/>
                  </Item>
                </Form>
              }

              <Text style={{fontSize:24, marginLeft: 30, marginTop: 15}}> Data PIC </Text>
              <Form style={{ marginHorizontal:30 }}>
                <Item rounded style={ styles.item }>
                  <Input placeholder="Nama PIC" style={ styles.itemInput } onChangeText={(value) => handleChange('nama_pic', value)}/>
                  <Icon name='contact' style={{ width:40 }}/>
                </Item>
                <Item rounded style={ styles.item }>
                  <Input placeholder="Email" style={ styles.itemInput } onChangeText={(value) => handleChange('email_pic', value)}/>
                  <Icon name='mail' style={{ width:40 }}/>
                </Item>
                <Item rounded style={ styles.item }>
                  <Input placeholder="No. HP" style={ styles.itemInput } onChangeText={(value) => handleChange('hp_pic', value)}/>
                  <Icon name='phone-portrait' style={{ width:40 }}/>
                </Item>
                <Item rounded style={ styles.item }>
                  <Input placeholder="No. Telp" style={ styles.itemInput } onChangeText={(value) => handleChange('telp_pic', value)}/>
                  <Icon name='call' style={{ width:40 }}/>
                </Item>
                <Item rounded style={ styles.item }>
                  <Input placeholder="Fax" style={ styles.itemInput } onChangeText={(value) => handleChange('fax_pic', value)}/>
                  <Icon name='print' style={{ width:40 }}/>
                </Item>
              </Form>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, marginLeft:80 }}>
                <Button block iconRight success rounded onPress={() => handleProfileSubmit()}>
                  <Text style={{ color: "#fff", paddingHorizontal: 45 }}>SUBMIT</Text>
                  <Icon name='add-circle' style={{ marginRight: 0, padding: 10, borderTopRightRadius: 20, borderBottomRightRadius: 20, backgroundColor: 'darkgreen' }}/>
                </Button>
              </View>
            </View>
          </View>
          </ScrollView>
        </Modal>
        {/*-------------------------------- Lengkapi Profile --------------*/}
        
        {/*<Header />*/}
        <View style={{marginHorizontal:30, marginTop:80}}>
          <Image source={require(LOGO)} style={{ width: 40, height: 45  }}/>
          <Text style={{ fontSize: 25, color:'darkblue', marginTop:10 }}> Register New Account </Text>
        </View>
        <Form style={{ marginHorizontal:30, marginTop:10 }}>
          <Picker
            mode="dropdown"
            placeholder="Select your SIM"
            iosIcon={<Icon name="arrow-down"/>}
            placeholder="Select your SIM"
            textStyle={{ color: "#5cb85c" }}
            itemStyle={{
              backgroundColor: "#d3d3d3",
              marginLeft: 0,
              paddingLeft: 10,
            }}
            itemTextStyle={{ color: '#788ad2' }}
            style={{ width: undefined, borderWidth: 1 }}
            selectedValue={data.usery_type_id}
            onValueChange={value => handleChange('usery_type_id', value)}
          >
            <Picker.Item label="Jenis User" value="0" />
            <Picker.Item label="Consigne" value="1" />
            <Picker.Item label="Supplier" value="2" />
            <Picker.Item label="Shipper" value="3" />
            <Picker.Item label="Reseller" value="4" />
            <Picker.Item label="Vessel Operator" value="5" />
          </Picker>
          <Item rounded style={ styles.item }>
            <Input placeholder="Email" style={ styles.itemInput } value={data.email} onChangeText={value => handleChange('email', value)}/>
            <Icon name='mail' style={{ width:40 }}/>
          </Item>
          <Item rounded style={ styles.item }>
            <Input secureTextEntry={true} placeholder="Password" style={ styles.itemInput } value={data.password} onChangeText={value => handleChange('password', value)}/>
            <Text>Show</Text>
            <Icon name='key' style={{ width:40 }}/>
          </Item>
        </Form>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, marginHorizontal:30, marginBottom:30}}>
          <Button block iconRight rounded onPress={() => navigate('Login')}>
            <Icon name='arrow-round-back' style={{ marginRight: 0, paddingVertical: 10, paddingHorizontal: 20, borderRadius:20, backgroundColor: 'darkblue' }}/>
          </Button>
          <Button block iconRight success rounded onPress={() => handleRegisterCheck(data)}>
            <Text style={{ color: "#fff", paddingHorizontal: 45 }}>REGISTER</Text>
            <Icon name='add-circle' style={{ marginRight: 0, padding: 10, borderTopRightRadius: 20, borderBottomRightRadius: 20, backgroundColor: 'darkgreen' }}/>
          </Button>
        </View>
        <Image source={require(BACKGROUND)} style={{ width:"100%", height:200 }}/>
      </ScrollView>
    </KeyboardAvoidingView>
  );
}

RegisterScreen.navigationOptions = {
  title: 'Register',
  headerShown: false
};

const styles = StyleSheet.create({
  item: {
    marginTop: 20,
  },
  itemInput: {
    marginLeft: 10,
  }
});
