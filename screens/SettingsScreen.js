import React, { useRef, useEffect } from 'react';
import { TouchableWithoutFeedback, Text } from 'react-native';
import { useNavigation } from 'react-navigation-hooks'; 
import { doLogout } from '../src/redux/actions/authAction'
import { useDispatch, useSelector } from 'react-redux';

export default function SettingsScreen() {
	const dispatch = useDispatch();
 	const navigation = useRef(useNavigation());
 	const isAuthenticated = useSelector(state => state.auth.isAuthenticated);
 	useEffect(() => {
 		if(isAuthenticated === false || isAuthenticated === '' || isAuthenticated === null)
 		{
 			navigation.current.navigate('Auth');
 		}
 	}, [isAuthenticated])
  return (
  	<TouchableWithoutFeedback onPress={() => dispatch(doLogout())}>
  		<Text>Logout</Text>
  	</TouchableWithoutFeedback>
  )
}

SettingsScreen.navigationOptions = {
  title: 'app.json',
};
