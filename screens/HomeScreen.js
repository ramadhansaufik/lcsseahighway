import React, { useReducer, useEffect, useState } from 'react';
import { StyleSheet, View, Image, ScrollView, Dimensions, ImageBackground, TouchableWithoutFeedback } from 'react-native';
import { Container, Content, Text, Icon, Thumbnail, Button } from 'native-base';
import { useNavigation } from 'react-navigation-hooks';
import { useSelector, useDispatch } from 'react-redux'
import { createSelector } from 'reselect'
import { userFetchUser } from '../src/redux/actions/userAction'


export default function HomeScreen() {
  const { navigate } = useNavigation();
  const counter = useSelector(state => JSON.stringify(state));
  const isAuthenticated = useSelector(state => JSON.stringify(state.auth.isAuthenticated));
  const dispatch = useDispatch();
  const user = useSelector(state => state.users.user);
  const [profile, setProfile] = useState({
    data:[],
    status:200,
    success:true,
  })
  useEffect(() => {
    if(isAuthenticated === false || isAuthenticated === '' || isAuthenticated === null)
    {
      navigation.current.navigate('Auth');
    }
  }, [isAuthenticated])

  return (
    <Container style={{ flex: 1 }}>
      <Content showsVerticalScrollIndicator={false}>
        <View style={{ position:'absolute', zIndex: -1, height: 170, width: "100%" }}>
          <ImageBackground source={require('../assets/images/tollaut.png')} style={{width: '100%', height: '100%'}}></ImageBackground>
        </View>
        {/* Logo LCS */}
        <Image source={require('../assets/images/logo2.png')} style={{ width: 100, height: 30, alignSelf:'center', marginTop:50 }}/>
        {/* Brief Profile */}
        <View style={{ flexDirection: 'row', paddingVertical:10, elevation:40, marginTop: 20, marginHorizontal: 10, backgroundColor:'rgba(57,49,133,1)', borderRadius: 10 }}>
          <Thumbnail small style={{ marginLeft: 10, flex:1, alignSelf:'center' }} source={require('../assets/images/thumbnail.png')}/>
          <View style={{ flex: 6, alignSelf:'center', marginLeft: 10 }}>
            <Text style={{ fontSize: 18, color:'white' }}>{user == null ? '' : user.data[0].nama_regulator}</Text>
            <Text style={{ fontSize: 12, color:'white' }}>{user == null ? '' : user.data[0].email}</Text>
          </View>
          <Icon name='qr-scanner' style={{ flex: 1, alignSelf: 'center', marginLeft: 20, color:'white' }}/>
        </View>
        {/* Dashboard */}
        <TouchableWithoutFeedback onPress={() => navigate('VesselTracking')}>
          <View style={{ backgroundColor:'rgba(255,255,255,1)', marginHorizontal:10, marginTop: 10, padding: 15,borderRadius: 10, elevation: 40 }}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flex: 3 }}>
                <Text style={{ fontSize: 24 }}>Lihat posisi terakhir pengiriman barang!</Text>
              </View>
              <View style={{ flex: 1, alignSelf:'center' }}>
                <Image source={require('../assets/images/map.png')} style={{ height: 70, width: 70, alignSelf:'center' }}/>
              </View>
            </View>
            <Text style={{ color:'darkblue', marginTop: 10  }}>Lacak Sekarang</Text>
          </View>
        </TouchableWithoutFeedback>
        {/* Grid Menu */}
        <View style={{ marginTop: 5 }}>
          <View style={styles.menuRow}>
            <TouchableWithoutFeedback onPress={() => navigate('PurchaseOrder')}><View style={styles.menuItem}><Image source={require('../assets/images/menu/shop.png')} style={styles.menuItemImage}/><Text>P.O.</Text></View></TouchableWithoutFeedback>
            <TouchableWithoutFeedback onPress={() => navigate('Containers')}><View style={styles.menuItem}><Image source={require('../assets/images/menu/container.png')} style={styles.menuItemImage}/><Text>Container</Text></View></TouchableWithoutFeedback>
            <TouchableWithoutFeedback onPress={() => navigate('Commodities')}><View style={styles.menuItem}><Image source={require('../assets/images/menu/boxes.png')} style={styles.menuItemImage}/><Text>Commodity</Text></View></TouchableWithoutFeedback>
            <TouchableWithoutFeedback onPress={() => navigate('Penting')}><View style={styles.menuItem}><Image source={require('../assets/images/menu/packaging2.png')} style={styles.menuItemImage}/><Text>Penting</Text></View></TouchableWithoutFeedback>
          </View>
          <View style={styles.menuRow}>
            <TouchableWithoutFeedback onPress={() => navigate('Deliveries')}><View style={styles.menuItem}><Image source={require('../assets/images/menu/truck.png')} style={styles.menuItemImage}/><Text>Delivery</Text></View></TouchableWithoutFeedback>
            <TouchableWithoutFeedback onPress={() => navigate('Kapal')}><View style={styles.menuItem}><Image source={require('../assets/images/menu/cargoship.png')} style={styles.menuItemImage}/><Text>Kapal</Text></View></TouchableWithoutFeedback>
            <TouchableWithoutFeedback onPress={() => navigate('Kemasan')}><View style={styles.menuItem}><Image source={require('../assets/images/menu/packaging4.png')} style={styles.menuItemImage}/><Text>Kemasan</Text></View></TouchableWithoutFeedback>
            <TouchableWithoutFeedback onPress={() => console.log("More Menu")}><View style={styles.menuItem}><Image source={require('../assets/images/menu/menu.png')} style={styles.menuItemImage}/><Text>More</Text></View></TouchableWithoutFeedback>
          </View>
        </View>
        
        <View style={{ marginTop: 15 }}>
          {/* Status Order */}
          <ScrollView horizontal pagingEnabled showsHorizontalScrollIndicator={false} disableIntervalMomentum>
            <View style={styles.carouselItem}>
              <Text>Status Order</Text>

            </View>
            <View style={styles.carouselItem}>
              <Text>Order Bulan: Desember</Text>
            </View>
          </ScrollView>
          {/* 
            Order Per Jenis Prioritas - 
            5 Muatan Terbanyak per Jenis Prioritas 
            Voyage Per Trayek 2019
            Total Container per Port 2019
          */}
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            <View style={{ marginVertical: 10, backgroundColor:'rgba(255,255,255,1)', width: DEVICE_WIDTH-160, marginHorizontal:10, padding: 15,borderRadius: 10, elevation: 10 }}>
              <Text style={{ fontSize: 20 }}>Order Per Jenis Prioritas</Text>
              <Text style={{ color:'darkblue', marginTop: 10 }}>Lihat Sekarang</Text>
              <Image source={require('../assets/images/diagram.png')} style={{ height: 40, width: 40, position:'absolute', right:15, bottom: 20 }}/> 
            </View>
            <View style={{ marginVertical: 10, backgroundColor:'rgba(255,255,255,1)', width: DEVICE_WIDTH-160, marginHorizontal:10, padding: 15,borderRadius: 10, elevation: 10 }}>
              <Text style={{ fontSize: 20 }}>5 Muatan Terbanyak per Jenis Prioritas</Text>
              <Text style={{ color:'darkblue', marginTop: 10 }}>Lihat Sekarang</Text>
              <Image source={require('../assets/images/line-chart.png')} style={{ height: 40, width: 40, position:'absolute', right:15, bottom: 20 }}/> 
            </View>
            <View style={{ marginVertical: 10, backgroundColor:'rgba(255,255,255,1)', width: DEVICE_WIDTH-160, marginHorizontal:10, padding: 15,borderRadius: 10, elevation: 10 }}>
              <Text style={{ fontSize: 20 }}>Voyage Per Trayek 2019</Text>
              <Text style={{ color:'darkblue', marginTop: 10 }}>Lihat Sekarang</Text>
              <Image source={require('../assets/images/line-chart.png')} style={{ height: 40, width: 40, position:'absolute', right:15, bottom: 20 }}/> 
            </View>
            <View style={{ marginVertical: 10, backgroundColor:'rgba(255,255,255,1)', width: DEVICE_WIDTH-160, marginHorizontal:10, padding: 15,borderRadius: 10, elevation: 10 }}>
              <Text style={{ fontSize: 20 }}>Total Container per Port 2019</Text>
              <Text style={{ color:'darkblue', marginTop: 10 }}>Lihat Sekarang</Text>
              <Image source={require('../assets/images/line-chart.png')} style={{ height: 40, width: 40, position:'absolute', right:15, bottom: 20 }}/> 
            </View>
          </ScrollView>
          {/* Order Masuk Bulan Ini */}
          <ScrollView 
            snapToInterval={360 - (10 + 10)}
            snapToAlignment={"center"} horizontal showsHorizontalScrollIndicator={false} disableIntervalMomentum>
            <View style={styles.carouselItem}>
              <Text>Report User</Text>
            </View>
            <View style={styles.carouselItem}>
              <Text>Daftar Operator</Text>
            </View>
            <View style={styles.carouselItem}>
              <Text>Realisasi Muatan Per Operator</Text>
            </View>
          </ScrollView>
        </View>

        {/* Disparitas Harga */}
        <View style={{ backgroundColor:'rgba(255,255,255,1)', marginHorizontal:10, marginTop: 10, padding: 15,borderRadius: 10, elevation: 10 }}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 3 }}>
              <Text style={{ fontSize: 24 }}>Lihat Disparitas Harga Barang!</Text>
            </View>
            <View style={{ flex: 1, alignSelf:'center' }}>
              <Image source={require('../assets/images/menu/list.png')} style={{ height: 70, width: 70, alignSelf:'center' }}/>
            </View>
          </View>
          <Text style={{ color:'darkblue', marginTop: 10 }}>Lihat Sekarang</Text>
        </View>
        {/* Sisa Kuota Trayek */}
        <View style={{ backgroundColor:'#FECC00', marginHorizontal:10, marginTop: 10, padding: 15,borderRadius: 10, elevation: 10 }}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 3 }}>
              <Text style={{ fontSize: 24 }}>Lihat Kuota Trayek Bulan Ini!</Text>
            </View>
            <View style={{ flex: 1, alignSelf:'center' }}>
              <Image source={require('../assets/images/menu/data.png')} style={{ height: 70, width: 70, alignSelf:'center' }}/>
            </View>
          </View>
          <Text style={{ color:'darkblue', marginTop: 10 }}>Lihat Sekarang</Text>
        </View>
        {/* Muatan Per Wilayah */}
        <View style={{ backgroundColor:'rgba(255,255,255,1)', marginHorizontal:10, marginTop: 10, padding: 15,borderRadius: 10, elevation: 10 }}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 3 }}>
              <Text style={{ fontSize: 24 }}>Lihat Muatan Per Wilayah!</Text>
            </View>
            <View style={{ flex: 1, alignSelf:'center' }}>
              <Image source={require('../assets/images/menu/packaging1.png')} style={{ height: 70, width: 70, alignSelf:'center' }}/>
            </View>
          </View>
          <Text style={{ color:'darkblue', marginTop: 10 }}>Lihat Sekarang</Text>
        </View>
        {/* Realisasi */}
        <View style={{ backgroundColor:'rgba(255,255,255,1)', marginHorizontal:10, marginTop: 10, padding: 15,borderRadius: 10, elevation: 10 }}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 3 }}>
              <Text style={{ fontSize: 24 }}>Realisasi!</Text>
            </View>
            <View style={{ flex: 1, alignSelf:'center' }}>
              <Image source={require('../assets/images/menu/data.png')} style={{ height: 70, width: 70, alignSelf:'center' }}/>
            </View>
          </View>
          <Text style={{ color:'darkblue', marginTop: 10 }}>Lihat Sekarang</Text>
        </View>
        {/* Waktu Tempuh */}
        <View style={{ backgroundColor:'rgba(255,255,255,1)', marginHorizontal:10, marginTop: 10, padding: 15,borderRadius: 10, elevation: 10 }}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 3 }}>
              <Text style={{ fontSize: 24 }}>Waktu Tempuh!</Text>
            </View>
            <View style={{ flex: 1, alignSelf:'center' }}>
              <Image source={require('../assets/images/schedule.png')} style={{ height: 70, width: 70, alignSelf:'center' }}/>
            </View>
          </View>
          <Text style={{ color:'darkblue', marginTop: 10 }}>Lihat Sekarang</Text>
        </View>
        {/* Report User 
        <View>
          <Text style={{ marginLeft: 5 }}> Report User </Text>
          <ScrollView horizontal showsHorizontalScrollIndicator={false} style={{  }} decelerationRate={0.01}>
            <View style={ styles.reportUser }>
              <Text style={{ fontSize: 16 }}>Consignee</Text>
              <Text style={{ fontSize: 12 }}>Terdaftar: 40</Text>
              <Text style={{ fontSize: 12 }}>Aktif: 12</Text>
            </View>
            <View style={ styles.reportUser }>
              <Text style={{ fontSize: 16 }}>Supplier</Text>
              <Text style={{ fontSize: 12 }}>Terdaftar: 30</Text>
              <Text style={{ fontSize: 12 }}>Aktif: 4</Text>
            </View>
            <View style={ styles.reportUser }>
              <Text style={{ fontSize: 16 }}>Shipper</Text>
              <Text style={{ fontSize: 12 }}>Terdaftar: 80</Text>
              <Text style={{ fontSize: 12 }}>Aktif: 22</Text>
            </View>
            <View style={ styles.reportUser }>
              <Text style={{ fontSize: 16 }}>Reseller</Text>
              <Text style={{ fontSize: 12 }}>Terdaftar: 20</Text>
              <Text style={{ fontSize: 12 }}>Aktif: 10</Text>
            </View>
            <View style={ styles.reportUser }>
              <Text style={{ fontSize: 16 }}>Vessel Operator</Text>
              <Text style={{ fontSize: 12 }}>Terdaftar: 20</Text>
              <Text style={{ fontSize: 12 }}>Aktif: 10</Text>
            </View>
            <View style={ styles.reportUser }>
              <Text style={{ fontSize: 16 }}>Regulator</Text>
              <Text style={{ fontSize: 12 }}>Terdaftar: 20</Text>
              <Text style={{ fontSize: 12 }}>Aktif: 10</Text>
            </View>
          </ScrollView>
        </View>
        */}
        {/* Menu
        <View style={{ flexDirection:'row' }}>
          <Card style={{ flex:1 }}>
            <CardItem>
              <Body style={{ flex:1, alignItems: 'center', justifyContent:'center', flexDirection:'row' }}>
                <Icon name="document" />
                <Text style={{ textAlign: 'center' }}>
                  Purchase Order
                </Text>
              </Body>
            </CardItem>
          </Card>
          <Card style={{ flex:1 }}>
            <CardItem>
              <Body style={{ flex:1, alignItems: 'center', justifyContent:'center', flexDirection:'row' }}>
                <Icon name="cube" />
                <Text style={{ textAlign: 'center' }}>
                  Komoditas
                </Text>
              </Body>
            </CardItem>
          </Card>
          <Card style={{ flex:1 }}>
            <CardItem>
             <Body style={{ flex:1, alignItems: 'center', justifyContent:'center', flexDirection:'row' }}>
                <Icon name="cube" />
                <Text style={{ textAlign: 'center' }}>
                  Barang{"\n"}Penting
                </Text>
              </Body>
            </CardItem>
          </Card>
        </View>
        <View style={{ flexDirection:'row' }}>
          <Card style={{ flex:1 }}>
            <CardItem>
              <Body style={{ flex:1, alignItems: 'center', justifyContent:'center', flexDirection:'row' }}>
                <Icon name="ios-send" />
                <Text style={{ textAlign: 'center' }}>
                  Delivery Order
                </Text>
              </Body>
            </CardItem>
          </Card>
          <Card style={{ flex:1 }}>
            <CardItem>
              <Body style={{ flex:1, alignItems: 'center', justifyContent:'center', flexDirection:'row' }}>
                <Icon name="cube" />
                <Text style={{ textAlign: 'center' }}>
                  Kemasan
                </Text>
              </Body>
            </CardItem>
          </Card>
          <Card style={{ flex:1 }}>
            <CardItem>
              <Body style={{ flex:1, alignItems: 'center', justifyContent:'center', flexDirection:'row' }}>
                <Icon name="cube" />
                <Text style={{ textAlign: 'center' }}>
                  Satuan
                </Text>
              </Body>
            </CardItem>
          </Card>
        </View>
        <View style={{ flexDirection:'row' }}>
          <Card style={{ flex:1 }}>
            <CardItem>
              <Body style={{ flex:1, alignItems: 'center', justifyContent:'center', flexDirection:'row' }}>
                <Icon name="pricetags" />
                <Text style={{ textAlign: 'center' }}>
                  Tipe Container
                </Text>
              </Body>
            </CardItem>
          </Card>
          <Card style={{ flex:2 }}>
            <CardItem>
              <Body>
                <Text>
                   //Your text here
                </Text>
              </Body>
            </CardItem>
          </Card>
        </View>
        <View style={{ flexDirection:'row' }}>
          <Card style={{ flex:1 }}>
            <CardItem>
              <Body style={{ flex:1, alignItems: 'center', justifyContent:'center', flexDirection:'row' }}>
                <Icon name="ios-boat" />
                <Text style={{ textAlign: 'center' }}>
                  Kapal
                </Text>
              </Body>
            </CardItem>
          </Card>
          <Card style={{ flex:1 }}>
            <CardItem>
              <Body style={{ flex:1, alignItems: 'center', justifyContent:'center', flexDirection:'row' }}>
                <Icon name="compass" />
                <Text style={{ textAlign: 'center' }}>
                  Tracking
                </Text>
              </Body>
            </CardItem>
          </Card>
          <Card style={{ flex:1 }}>
            <CardItem>
              <Body style={{ flex:1, alignItems: 'center', justifyContent:'center', flexDirection:'row' }}>
                <Icon name="chatbubbles" />
                <Text style={{ textAlign: 'center' }}>
                  Pengaduan
                </Text>
              </Body>
            </CardItem>
          </Card>
        </View>
        <View style={{ flexDirection:'row' }}>
          <Card style={{ flex:1 }}>
            <CardItem>
              <Body style={{ flex:1, alignItems: 'center', justifyContent:'center', flexDirection:'row' }}>
                <Icon name="ios-boat" />
                <Text style={{ textAlign: 'center' }}>
                  Aktivasi User
                </Text>
              </Body>
            </CardItem>
          </Card>
          <Card style={{ flex:1 }}>
            <CardItem>
              <Body style={{ flex:1, alignItems: 'center', justifyContent:'center', flexDirection:'row' }}>
                <Icon name="compass" />
                <Text style={{ textAlign: 'center' }}>
                  User LCS
                </Text>
              </Body>
            </CardItem>
          </Card>
          <Card style={{ flex:1 }}>
            <CardItem>
              <Body style={{ flex:1, alignItems: 'center', justifyContent:'center', flexDirection:'row' }}>
                <Icon name="chatbubbles" />
                <Text style={{ textAlign: 'center' }}>
                  Regulator
                </Text>
              </Body>
            </CardItem>
          </Card>
        </View>
        */}
        <View style={{ height: 20 }}></View>
      </Content>
    </Container>
  );
}

HomeScreen.navigationOptions = {
  header: null,
};
const DEVICE_WIDTH = Dimensions.get('window').width;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  reportUser: {
    padding: 10,
    marginHorizontal: 7,
    backgroundColor: "mediumturquoise",
    borderRadius: 10,
    elevation: 2
  },
  menuRow: {
    marginTop: 20,
    flexDirection: 'row',
  },
  menuItem: {
    flex:1,
    justifyContent:'center',
    alignItems:'center',
  },
  menuItemImage: {
    width:36,
    height:36,
  },
  carouselItem: {
    backgroundColor:'rgba(255,255,255,1)', 
    marginHorizontal:10, 
    padding: 15,
    borderRadius: 10,
    borderWidth: 0.4,  
    width: DEVICE_WIDTH-40, 
    height: 100, 
    flexDirection:'row',
    marginVertical:10,
  }
});
