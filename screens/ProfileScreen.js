import React, { useRef, useEffect, useState } from 'react';
import { Container, Header, Content, List, ListItem, Text, Right, Left } from 'native-base'
import { useNavigation } from 'react-navigation-hooks'; 
import { doLogout } from '../src/redux/actions/authAction'
import { useDispatch, useSelector } from 'react-redux';
import { AsyncStorage } from 'react-native';
import { userFetchUser } from '../src/redux/actions/userAction'


export default function ProfileScreen() {

  const dispatch = useDispatch();
 	const navigation = useRef(useNavigation());
 	const isAuthenticated = useSelector(state => state.auth.isAuthenticated);
  const user = useSelector(state => state.users.user);
  const account = useSelector(state => state.auth.account);

  const [profile, setProfile] = useState({
    nama:'',
    email:'',
    telp:'',
    alamat:'',
    birthdate:'',
    birthplace:''
  });
  const [idUser, setIdUser] = useState(false);

  useEffect(() => {
 		if(isAuthenticated === false || isAuthenticated === '' || isAuthenticated === null)
 		{
 			navigation.current.navigate('Auth');
 		}
 	}, [isAuthenticated])

  return (
  	<Container>
      <Content>
        <List>
          <ListItem itemDivider>
            <Left><Text>User</Text></Left>
          </ListItem>                    
          <ListItem>
          	<Left><Text>{user.data[0].nama_regulator}</Text></Left>
          	<Right><Text style={{fontSize:13}}>Nama</Text></Right>
          </ListItem>
          <ListItem>
          	<Left><Text>{user.data[0].email}</Text></Left>
            <Right><Text style={{fontSize:13}}>Email</Text></Right>
          </ListItem>
          <ListItem>
          	<Left><Text>{user.data[0].telp}</Text></Left>
            <Right><Text style={{fontSize:13}}>Telp</Text></Right>
          </ListItem>
          <ListItem>
          	<Left><Text>{user.data[0].alamat}</Text></Left>
            <Right><Text style={{fontSize:13}}>Alamat</Text></Right>
          </ListItem>
          <ListItem>
            <Left><Text>{user.data[0].tgl_lahir}</Text></Left>
            <Right><Text style={{fontSize:13}}>Birth Date</Text></Right>
          </ListItem>
          <ListItem>
            <Left><Text>{user.data[0].tmpt_lahir}</Text></Left>
            <Right><Text style={{fontSize:13}}>Birth Place</Text></Right>
          </ListItem>
          <ListItem itemDivider>
            <Text>Configuration</Text>
          </ListItem>  
          <ListItem>
            <Text>Ubah Password</Text>
          </ListItem>
          <ListItem onPress = {() => navigation.current.navigate('EditProfileScreen')}>
            <Text>Edit Profile</Text>
          </ListItem>
          <ListItem onPress={() => dispatch(doLogout())}>
            <Text>Logout</Text>
          </ListItem>
        </List>
      </Content>
    </Container>
  )

}

ProfileScreen.navigationOptions = {
  title: 'Profile',
};
