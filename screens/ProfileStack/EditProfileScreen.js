/* Edit Profile Screen */

import React from 'react'
import { Container, Header, Content, List, ListItem, Text, Right, Left, Input } from 'native-base'
import { useSelector, useDispatch } from 'react-redux'
import { StyleSheet, TouchableWithoutFeedback, View } from 'react-native'

function EditProfileScreen() {
	const user = useSelector(state => state.users.user);
	return (
		<Container>
      <Content>
        <List style={{ marginBottom:20 }}>
          <ListItem itemDivider>
            <Left><Text>User</Text></Left>
          </ListItem>                    
          <ListItem>
          	<Left><Input placeholder={user.data[0].nama_regulator} style={styles.itemInput}/></Left>
          	<Right><Text style={{fontSize:13}}>Nama</Text></Right>
          </ListItem>
          <ListItem>
          	<Left><Input placeholder={user.data[0].email} style={styles.itemInput}/></Left>
            <Right><Text style={{fontSize:13}}>Email</Text></Right>
          </ListItem>
          <ListItem>
          	<Left><Input placeholder={user.data[0].telp} style={styles.itemInput}/></Left>
            <Right><Text style={{fontSize:13}}>Telp</Text></Right>
          </ListItem>
          <ListItem>
          	<Left><Input placeholder={user.data[0].alamat} style={styles.itemInput}/></Left>
            <Right><Text style={{fontSize:13}}>Alamat</Text></Right>
          </ListItem>
          <ListItem>
          	<Left><Input disabled value={user.data[0].kota} style={styles.itemInput}/></Left>
            <Right><Text style={{fontSize:13}}>Kota</Text></Right>
          </ListItem>
          <ListItem>
          	<Left><Input disabled value={user.data[0].provinsi} style={styles.itemInput}/></Left>
            <Right><Text style={{fontSize:13}}>Provinsi</Text></Right>
          </ListItem>
          <ListItem>
            <Left><Input disabled value={user.data[0].tgl_lahir} style={styles.itemInput}/></Left>
            <Right><Text style={{fontSize:13}}>Birth Date</Text></Right>
          </ListItem>
          <ListItem>
            <Left><Input disabled value={user.data[0].tmpt_lahir} style={styles.itemInput}/></Left>
            <Right><Text style={{fontSize:13}}>Birth Place</Text></Right>
          </ListItem>
        </List>
        <TouchableWithoutFeedback>
        	<View style={{ alignItems:'center', justifyContent: 'center', backgroundColor:'darkgreen', marginHorizontal:70, marginBottom:20, paddingVertical:10 }}>
        		<Text style={{ color:"#fff" }}>SUBMIT</Text>
        	</View>
        </TouchableWithoutFeedback>
      </Content>
    </Container>
	)
}


EditProfileScreen.navigationOptions = {
  title: 'Edit Profile',
};

const styles = StyleSheet.create({
	itemInput: {
		marginVertical:-10
	},
})

export { EditProfileScreen }