import React, {useEffect, useState, useRef} from 'react';
import { AsyncStorage, Text, View, Image } from 'react-native';
import { useNavigation } from 'react-navigation-hooks';
import { useSelector } from 'react-redux';

function LoadingScreen() {
	const navigation = useRef(useNavigation());
	const LOGO = '../assets/images/logo.png';
	const isAuthenticated = useSelector(state => state.auth.isAuthenticated);
	const _bootstrapAsync = async () => {
		const userToken = await AsyncStorage.getItem('tolaut@token');
		navigation.current.navigate(userToken && isAuthenticated ? 'Main': 'Auth');
	}
	
	useEffect(() => {
		_bootstrapAsync()
	}, [isAuthenticated])

	return (
		<View style={{ flex:1, alignItems:'center', justifyContent:'center' }}>
      <Image source={require(LOGO)} style={{ width: 80, height: 90, alignSelf:'center'  }}/>
    </View>
	)
}

export default LoadingScreen